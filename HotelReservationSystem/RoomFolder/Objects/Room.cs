﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelReservationSystem.RoomFolder
{
    public class Room
    {
        public int roomId;
        public int roomNumber;
        public string roomType;
        public string bedType;
        public float price;

        public Room(int roomNumber, string roomType, string bedType, float price)
        {
            this.roomNumber = roomNumber;
            this.roomType = roomType;
            this.bedType = bedType;
            this.price = price;
        }

        public Room(int roomId, int roomNumber, string roomType, string bedType, float price)
        {
            this.roomId = roomId;
            this.roomNumber = roomNumber;
            this.roomType = roomType;
            this.bedType = bedType;
            this.price = price;
        }
    }
}
