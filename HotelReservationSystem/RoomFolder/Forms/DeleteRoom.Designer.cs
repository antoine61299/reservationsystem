﻿namespace HotelReservationSystem.RoomFolder
{
    partial class DeleteRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeleteRoom));
            this.pnlDeleteRoom = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblDeleteRoom = new System.Windows.Forms.Label();
            this.pnlRoomDetails = new System.Windows.Forms.Panel();
            this.lblRoomDetails = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.dgvRooms = new System.Windows.Forms.DataGridView();
            this.RoomID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RoomNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RoomType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BedType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlDeleteRoom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlRoomDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRooms)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlDeleteRoom
            // 
            this.pnlDeleteRoom.BackColor = System.Drawing.Color.DarkTurquoise;
            this.pnlDeleteRoom.Controls.Add(this.pictureBox1);
            this.pnlDeleteRoom.Controls.Add(this.lblDeleteRoom);
            this.pnlDeleteRoom.Location = new System.Drawing.Point(0, 0);
            this.pnlDeleteRoom.Name = "pnlDeleteRoom";
            this.pnlDeleteRoom.Size = new System.Drawing.Size(500, 100);
            this.pnlDeleteRoom.TabIndex = 33;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(221, 79);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 45;
            this.pictureBox1.TabStop = false;
            // 
            // lblDeleteRoom
            // 
            this.lblDeleteRoom.AutoSize = true;
            this.lblDeleteRoom.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeleteRoom.ForeColor = System.Drawing.Color.White;
            this.lblDeleteRoom.Location = new System.Drawing.Point(309, 36);
            this.lblDeleteRoom.Name = "lblDeleteRoom";
            this.lblDeleteRoom.Size = new System.Drawing.Size(186, 36);
            this.lblDeleteRoom.TabIndex = 3;
            this.lblDeleteRoom.Text = "Delete Room";
            // 
            // pnlRoomDetails
            // 
            this.pnlRoomDetails.BackColor = System.Drawing.Color.FloralWhite;
            this.pnlRoomDetails.Controls.Add(this.lblRoomDetails);
            this.pnlRoomDetails.Controls.Add(this.btnDelete);
            this.pnlRoomDetails.Controls.Add(this.dgvRooms);
            this.pnlRoomDetails.Location = new System.Drawing.Point(0, 100);
            this.pnlRoomDetails.Name = "pnlRoomDetails";
            this.pnlRoomDetails.Size = new System.Drawing.Size(503, 384);
            this.pnlRoomDetails.TabIndex = 34;
            // 
            // lblRoomDetails
            // 
            this.lblRoomDetails.AutoSize = true;
            this.lblRoomDetails.BackColor = System.Drawing.Color.Transparent;
            this.lblRoomDetails.Font = new System.Drawing.Font("Candara", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRoomDetails.Location = new System.Drawing.Point(132, 17);
            this.lblRoomDetails.Name = "lblRoomDetails";
            this.lblRoomDetails.Size = new System.Drawing.Size(260, 29);
            this.lblRoomDetails.TabIndex = 58;
            this.lblRoomDetails.Text = "Choose a room to delete";
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnDelete.Font = new System.Drawing.Font("Candara", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Location = new System.Drawing.Point(360, 335);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(116, 36);
            this.btnDelete.TabIndex = 51;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            this.btnDelete.MouseLeave += new System.EventHandler(this.btnDelete_MouseLeave);
            this.btnDelete.MouseHover += new System.EventHandler(this.btnDelete_MouseHover);
            // 
            // dgvRooms
            // 
            this.dgvRooms.AllowUserToAddRows = false;
            this.dgvRooms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRooms.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RoomID,
            this.RoomNumber,
            this.RoomType,
            this.BedType,
            this.Price});
            this.dgvRooms.Location = new System.Drawing.Point(26, 63);
            this.dgvRooms.MultiSelect = false;
            this.dgvRooms.Name = "dgvRooms";
            this.dgvRooms.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRooms.Size = new System.Drawing.Size(443, 254);
            this.dgvRooms.TabIndex = 0;
            // 
            // RoomID
            // 
            this.RoomID.HeaderText = "Room ID";
            this.RoomID.Name = "RoomID";
            // 
            // RoomNumber
            // 
            this.RoomNumber.HeaderText = "Room Number";
            this.RoomNumber.Name = "RoomNumber";
            // 
            // RoomType
            // 
            this.RoomType.HeaderText = "Room Type";
            this.RoomType.Name = "RoomType";
            // 
            // BedType
            // 
            this.BedType.HeaderText = "Bed Type";
            this.BedType.Name = "BedType";
            // 
            // Price
            // 
            this.Price.HeaderText = "Price";
            this.Price.Name = "Price";
            // 
            // DeleteRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 479);
            this.Controls.Add(this.pnlDeleteRoom);
            this.Controls.Add(this.pnlRoomDetails);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DeleteRoom";
            this.Text = "Delete Rooms";
            this.pnlDeleteRoom.ResumeLayout(false);
            this.pnlDeleteRoom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlRoomDetails.ResumeLayout(false);
            this.pnlRoomDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRooms)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlDeleteRoom;
        private System.Windows.Forms.Label lblDeleteRoom;
        private System.Windows.Forms.Panel pnlRoomDetails;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.DataGridView dgvRooms;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoomID;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoomNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoomType;
        private System.Windows.Forms.DataGridViewTextBoxColumn BedType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.Label lblRoomDetails;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}