﻿namespace HotelReservationSystem.RoomFolder
{
    partial class UpdateRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateRoom));
            this.pnlUpdateRoom = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblUpdateRoom = new System.Windows.Forms.Label();
            this.gbxRoomDetails = new System.Windows.Forms.GroupBox();
            this.cmbBedType = new System.Windows.Forms.ComboBox();
            this.cmbRoomType = new System.Windows.Forms.ComboBox();
            this.cmbRoomID = new System.Windows.Forms.ComboBox();
            this.lblRoomID = new System.Windows.Forms.Label();
            this.lblBedType = new System.Windows.Forms.Label();
            this.lblRoomType = new System.Windows.Forms.Label();
            this.lblRoomNumber = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.txtRoomNumber = new System.Windows.Forms.TextBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.pnlRoomDetails = new System.Windows.Forms.Panel();
            this.lblRoomDetails = new System.Windows.Forms.Label();
            this.erpRoomNumber = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpRoomType = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpBedType = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpPrice = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlUpdateRoom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gbxRoomDetails.SuspendLayout();
            this.pnlRoomDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.erpRoomNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpRoomType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpBedType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpPrice)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlUpdateRoom
            // 
            this.pnlUpdateRoom.BackColor = System.Drawing.Color.DarkTurquoise;
            this.pnlUpdateRoom.Controls.Add(this.pictureBox1);
            this.pnlUpdateRoom.Controls.Add(this.lblUpdateRoom);
            this.pnlUpdateRoom.Location = new System.Drawing.Point(0, 0);
            this.pnlUpdateRoom.Name = "pnlUpdateRoom";
            this.pnlUpdateRoom.Size = new System.Drawing.Size(509, 100);
            this.pnlUpdateRoom.TabIndex = 33;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(221, 79);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 45;
            this.pictureBox1.TabStop = false;
            // 
            // lblUpdateRoom
            // 
            this.lblUpdateRoom.AutoSize = true;
            this.lblUpdateRoom.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpdateRoom.ForeColor = System.Drawing.Color.White;
            this.lblUpdateRoom.Location = new System.Drawing.Point(310, 37);
            this.lblUpdateRoom.Name = "lblUpdateRoom";
            this.lblUpdateRoom.Size = new System.Drawing.Size(196, 36);
            this.lblUpdateRoom.TabIndex = 3;
            this.lblUpdateRoom.Text = "Update Room";
            // 
            // gbxRoomDetails
            // 
            this.gbxRoomDetails.BackColor = System.Drawing.Color.Transparent;
            this.gbxRoomDetails.Controls.Add(this.cmbBedType);
            this.gbxRoomDetails.Controls.Add(this.cmbRoomType);
            this.gbxRoomDetails.Controls.Add(this.cmbRoomID);
            this.gbxRoomDetails.Controls.Add(this.lblRoomID);
            this.gbxRoomDetails.Controls.Add(this.lblBedType);
            this.gbxRoomDetails.Controls.Add(this.lblRoomType);
            this.gbxRoomDetails.Controls.Add(this.lblRoomNumber);
            this.gbxRoomDetails.Controls.Add(this.lblPrice);
            this.gbxRoomDetails.Controls.Add(this.txtPrice);
            this.gbxRoomDetails.Controls.Add(this.txtRoomNumber);
            this.gbxRoomDetails.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxRoomDetails.Location = new System.Drawing.Point(96, 63);
            this.gbxRoomDetails.Name = "gbxRoomDetails";
            this.gbxRoomDetails.Size = new System.Drawing.Size(315, 237);
            this.gbxRoomDetails.TabIndex = 34;
            this.gbxRoomDetails.TabStop = false;
            this.gbxRoomDetails.Text = "Room Details";
            // 
            // cmbBedType
            // 
            this.cmbBedType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBedType.FormattingEnabled = true;
            this.cmbBedType.Items.AddRange(new object[] {
            "Single",
            "Double",
            "King",
            "Queen"});
            this.cmbBedType.Location = new System.Drawing.Point(145, 154);
            this.cmbBedType.Name = "cmbBedType";
            this.cmbBedType.Size = new System.Drawing.Size(125, 21);
            this.cmbBedType.TabIndex = 51;
            // 
            // cmbRoomType
            // 
            this.cmbRoomType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbRoomType.FormattingEnabled = true;
            this.cmbRoomType.Items.AddRange(new object[] {
            "Standard",
            "Deluxe",
            "Executive",
            "Presidential",
            "Family Suite"});
            this.cmbRoomType.Location = new System.Drawing.Point(145, 113);
            this.cmbRoomType.Name = "cmbRoomType";
            this.cmbRoomType.Size = new System.Drawing.Size(125, 21);
            this.cmbRoomType.TabIndex = 50;
            // 
            // cmbRoomID
            // 
            this.cmbRoomID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbRoomID.FormattingEnabled = true;
            this.cmbRoomID.Location = new System.Drawing.Point(145, 34);
            this.cmbRoomID.Name = "cmbRoomID";
            this.cmbRoomID.Size = new System.Drawing.Size(125, 21);
            this.cmbRoomID.TabIndex = 55;
            this.cmbRoomID.DropDownClosed += new System.EventHandler(this.cmbRoomID_DropDownClosed);
            // 
            // lblRoomID
            // 
            this.lblRoomID.AutoSize = true;
            this.lblRoomID.BackColor = System.Drawing.Color.Transparent;
            this.lblRoomID.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRoomID.Location = new System.Drawing.Point(18, 34);
            this.lblRoomID.Name = "lblRoomID";
            this.lblRoomID.Size = new System.Drawing.Size(70, 19);
            this.lblRoomID.TabIndex = 54;
            this.lblRoomID.Text = "Room Id:";
            // 
            // lblBedType
            // 
            this.lblBedType.AutoSize = true;
            this.lblBedType.BackColor = System.Drawing.Color.Transparent;
            this.lblBedType.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBedType.Location = new System.Drawing.Point(15, 154);
            this.lblBedType.Name = "lblBedType";
            this.lblBedType.Size = new System.Drawing.Size(73, 19);
            this.lblBedType.TabIndex = 52;
            this.lblBedType.Text = "Bed Type:";
            this.lblBedType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblRoomType
            // 
            this.lblRoomType.AutoSize = true;
            this.lblRoomType.BackColor = System.Drawing.Color.Transparent;
            this.lblRoomType.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRoomType.Location = new System.Drawing.Point(15, 113);
            this.lblRoomType.Name = "lblRoomType";
            this.lblRoomType.Size = new System.Drawing.Size(88, 19);
            this.lblRoomType.TabIndex = 26;
            this.lblRoomType.Text = "Room Type:";
            this.lblRoomType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblRoomNumber
            // 
            this.lblRoomNumber.AutoSize = true;
            this.lblRoomNumber.BackColor = System.Drawing.Color.Transparent;
            this.lblRoomNumber.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRoomNumber.Location = new System.Drawing.Point(15, 74);
            this.lblRoomNumber.Name = "lblRoomNumber";
            this.lblRoomNumber.Size = new System.Drawing.Size(113, 19);
            this.lblRoomNumber.TabIndex = 27;
            this.lblRoomNumber.Text = "Room Number:";
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.BackColor = System.Drawing.Color.Transparent;
            this.lblPrice.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrice.Location = new System.Drawing.Point(15, 194);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(46, 19);
            this.lblPrice.TabIndex = 30;
            this.lblPrice.Text = "Price:";
            // 
            // txtPrice
            // 
            this.txtPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrice.Location = new System.Drawing.Point(145, 194);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(125, 20);
            this.txtPrice.TabIndex = 36;
            // 
            // txtRoomNumber
            // 
            this.txtRoomNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRoomNumber.Location = new System.Drawing.Point(145, 74);
            this.txtRoomNumber.Name = "txtRoomNumber";
            this.txtRoomNumber.Size = new System.Drawing.Size(125, 20);
            this.txtRoomNumber.TabIndex = 38;
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnUpdate.Font = new System.Drawing.Font("Candara", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(258, 324);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(116, 36);
            this.btnUpdate.TabIndex = 48;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            this.btnUpdate.MouseLeave += new System.EventHandler(this.btnUpdate_MouseLeave);
            this.btnUpdate.MouseHover += new System.EventHandler(this.btnUpdate_MouseHover);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnClear.Font = new System.Drawing.Font("Candara", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(384, 324);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(116, 36);
            this.btnClear.TabIndex = 49;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            this.btnClear.MouseLeave += new System.EventHandler(this.btnClear_MouseLeave);
            this.btnClear.MouseHover += new System.EventHandler(this.btnClear_MouseHover);
            // 
            // pnlRoomDetails
            // 
            this.pnlRoomDetails.BackColor = System.Drawing.Color.FloralWhite;
            this.pnlRoomDetails.Controls.Add(this.lblRoomDetails);
            this.pnlRoomDetails.Controls.Add(this.btnClear);
            this.pnlRoomDetails.Controls.Add(this.btnUpdate);
            this.pnlRoomDetails.Controls.Add(this.gbxRoomDetails);
            this.pnlRoomDetails.Location = new System.Drawing.Point(0, 100);
            this.pnlRoomDetails.Name = "pnlRoomDetails";
            this.pnlRoomDetails.Size = new System.Drawing.Size(510, 372);
            this.pnlRoomDetails.TabIndex = 34;
            // 
            // lblRoomDetails
            // 
            this.lblRoomDetails.AutoSize = true;
            this.lblRoomDetails.BackColor = System.Drawing.Color.Transparent;
            this.lblRoomDetails.Font = new System.Drawing.Font("Candara", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRoomDetails.Location = new System.Drawing.Point(178, 15);
            this.lblRoomDetails.Name = "lblRoomDetails";
            this.lblRoomDetails.Size = new System.Drawing.Size(165, 29);
            this.lblRoomDetails.TabIndex = 59;
            this.lblRoomDetails.Text = "Update a room";
            // 
            // erpRoomNumber
            // 
            this.erpRoomNumber.ContainerControl = this;
            // 
            // erpRoomType
            // 
            this.erpRoomType.ContainerControl = this;
            // 
            // erpBedType
            // 
            this.erpBedType.ContainerControl = this;
            // 
            // erpPrice
            // 
            this.erpPrice.ContainerControl = this;
            // 
            // UpdateRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 467);
            this.Controls.Add(this.pnlUpdateRoom);
            this.Controls.Add(this.pnlRoomDetails);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UpdateRoom";
            this.Text = "Update Rooms";
            this.pnlUpdateRoom.ResumeLayout(false);
            this.pnlUpdateRoom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gbxRoomDetails.ResumeLayout(false);
            this.gbxRoomDetails.PerformLayout();
            this.pnlRoomDetails.ResumeLayout(false);
            this.pnlRoomDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.erpRoomNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpRoomType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpBedType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpPrice)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlUpdateRoom;
        private System.Windows.Forms.Label lblUpdateRoom;
        private System.Windows.Forms.GroupBox gbxRoomDetails;
        private System.Windows.Forms.Label lblBedType;
        private System.Windows.Forms.Label lblRoomType;
        private System.Windows.Forms.Label lblRoomNumber;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Panel pnlRoomDetails;
        private System.Windows.Forms.ComboBox cmbRoomID;
        private System.Windows.Forms.Label lblRoomID;
        private System.Windows.Forms.TextBox txtRoomNumber;
        private System.Windows.Forms.ComboBox cmbBedType;
        private System.Windows.Forms.ComboBox cmbRoomType;
        private System.Windows.Forms.ErrorProvider erpRoomNumber;
        private System.Windows.Forms.ErrorProvider erpRoomType;
        private System.Windows.Forms.ErrorProvider erpBedType;
        private System.Windows.Forms.ErrorProvider erpPrice;
        private System.Windows.Forms.Label lblRoomDetails;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}