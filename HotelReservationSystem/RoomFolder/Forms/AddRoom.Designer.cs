﻿namespace HotelReservationSystem.RoomFolder
{
    partial class AddRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddRoom));
            this.pnlRoomDetails = new System.Windows.Forms.Panel();
            this.btnClear = new System.Windows.Forms.Button();
            this.lblRoomDetails = new System.Windows.Forms.Label();
            this.grpRoomDetails = new System.Windows.Forms.GroupBox();
            this.lblRoomNumber = new System.Windows.Forms.Label();
            this.cmbBedType = new System.Windows.Forms.ComboBox();
            this.txtRoomNumber = new System.Windows.Forms.TextBox();
            this.lblBedType = new System.Windows.Forms.Label();
            this.lblRoomType = new System.Windows.Forms.Label();
            this.cmbRoomType = new System.Windows.Forms.ComboBox();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.pnlAddRooms = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblAddRooms = new System.Windows.Forms.Label();
            this.erpRoomNumber = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpRoomType = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpBedType = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpPrice = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlRoomDetails.SuspendLayout();
            this.grpRoomDetails.SuspendLayout();
            this.pnlAddRooms.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpRoomNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpRoomType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpBedType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpPrice)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlRoomDetails
            // 
            this.pnlRoomDetails.BackColor = System.Drawing.Color.FloralWhite;
            this.pnlRoomDetails.Controls.Add(this.btnClear);
            this.pnlRoomDetails.Controls.Add(this.lblRoomDetails);
            this.pnlRoomDetails.Controls.Add(this.grpRoomDetails);
            this.pnlRoomDetails.Controls.Add(this.btnAdd);
            this.pnlRoomDetails.Location = new System.Drawing.Point(0, 100);
            this.pnlRoomDetails.Name = "pnlRoomDetails";
            this.pnlRoomDetails.Size = new System.Drawing.Size(507, 330);
            this.pnlRoomDetails.TabIndex = 7;
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnClear.Font = new System.Drawing.Font("Candara", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(381, 281);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(116, 36);
            this.btnClear.TabIndex = 50;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            this.btnClear.MouseLeave += new System.EventHandler(this.btnClear_MouseLeave);
            this.btnClear.MouseHover += new System.EventHandler(this.btnClear_MouseHover);
            // 
            // lblRoomDetails
            // 
            this.lblRoomDetails.AutoSize = true;
            this.lblRoomDetails.BackColor = System.Drawing.Color.Transparent;
            this.lblRoomDetails.Font = new System.Drawing.Font("Candara", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRoomDetails.Location = new System.Drawing.Point(92, 18);
            this.lblRoomDetails.Name = "lblRoomDetails";
            this.lblRoomDetails.Size = new System.Drawing.Size(328, 29);
            this.lblRoomDetails.TabIndex = 58;
            this.lblRoomDetails.Text = "Enter the details to add a room";
            // 
            // grpRoomDetails
            // 
            this.grpRoomDetails.Controls.Add(this.lblRoomNumber);
            this.grpRoomDetails.Controls.Add(this.cmbBedType);
            this.grpRoomDetails.Controls.Add(this.txtRoomNumber);
            this.grpRoomDetails.Controls.Add(this.lblBedType);
            this.grpRoomDetails.Controls.Add(this.lblRoomType);
            this.grpRoomDetails.Controls.Add(this.cmbRoomType);
            this.grpRoomDetails.Controls.Add(this.txtPrice);
            this.grpRoomDetails.Controls.Add(this.lblPrice);
            this.grpRoomDetails.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpRoomDetails.Location = new System.Drawing.Point(86, 67);
            this.grpRoomDetails.Name = "grpRoomDetails";
            this.grpRoomDetails.Size = new System.Drawing.Size(339, 195);
            this.grpRoomDetails.TabIndex = 29;
            this.grpRoomDetails.TabStop = false;
            this.grpRoomDetails.Text = "Room Details";
            // 
            // lblRoomNumber
            // 
            this.lblRoomNumber.AutoSize = true;
            this.lblRoomNumber.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRoomNumber.Location = new System.Drawing.Point(19, 30);
            this.lblRoomNumber.Name = "lblRoomNumber";
            this.lblRoomNumber.Size = new System.Drawing.Size(113, 19);
            this.lblRoomNumber.TabIndex = 19;
            this.lblRoomNumber.Text = "Room Number:";
            // 
            // cmbBedType
            // 
            this.cmbBedType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBedType.FormattingEnabled = true;
            this.cmbBedType.Items.AddRange(new object[] {
            "Single",
            "Double",
            "King",
            "Queen"});
            this.cmbBedType.Location = new System.Drawing.Point(149, 116);
            this.cmbBedType.Name = "cmbBedType";
            this.cmbBedType.Size = new System.Drawing.Size(125, 21);
            this.cmbBedType.TabIndex = 28;
            // 
            // txtRoomNumber
            // 
            this.txtRoomNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRoomNumber.Location = new System.Drawing.Point(149, 30);
            this.txtRoomNumber.Name = "txtRoomNumber";
            this.txtRoomNumber.Size = new System.Drawing.Size(125, 20);
            this.txtRoomNumber.TabIndex = 20;
            // 
            // lblBedType
            // 
            this.lblBedType.AutoSize = true;
            this.lblBedType.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBedType.Location = new System.Drawing.Point(20, 116);
            this.lblBedType.Name = "lblBedType";
            this.lblBedType.Size = new System.Drawing.Size(73, 19);
            this.lblBedType.TabIndex = 27;
            this.lblBedType.Text = "Bed Type:";
            // 
            // lblRoomType
            // 
            this.lblRoomType.AutoSize = true;
            this.lblRoomType.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRoomType.Location = new System.Drawing.Point(20, 73);
            this.lblRoomType.Name = "lblRoomType";
            this.lblRoomType.Size = new System.Drawing.Size(88, 19);
            this.lblRoomType.TabIndex = 21;
            this.lblRoomType.Text = "Room Type:";
            // 
            // cmbRoomType
            // 
            this.cmbRoomType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbRoomType.FormattingEnabled = true;
            this.cmbRoomType.Items.AddRange(new object[] {
            "Standard",
            "Deluxe",
            "Executive",
            "Presidential",
            "Family Suite"});
            this.cmbRoomType.Location = new System.Drawing.Point(149, 73);
            this.cmbRoomType.Name = "cmbRoomType";
            this.cmbRoomType.Size = new System.Drawing.Size(125, 21);
            this.cmbRoomType.TabIndex = 26;
            // 
            // txtPrice
            // 
            this.txtPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrice.Location = new System.Drawing.Point(149, 159);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(125, 20);
            this.txtPrice.TabIndex = 24;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrice.Location = new System.Drawing.Point(21, 159);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(46, 19);
            this.lblPrice.TabIndex = 23;
            this.lblPrice.Text = "Price:";
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnAdd.Font = new System.Drawing.Font("Candara", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(256, 281);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(116, 36);
            this.btnAdd.TabIndex = 25;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            this.btnAdd.MouseLeave += new System.EventHandler(this.btnAdd_MouseLeave);
            this.btnAdd.MouseHover += new System.EventHandler(this.btnAdd_MouseHover);
            // 
            // pnlAddRooms
            // 
            this.pnlAddRooms.BackColor = System.Drawing.Color.DarkTurquoise;
            this.pnlAddRooms.Controls.Add(this.pictureBox1);
            this.pnlAddRooms.Controls.Add(this.lblAddRooms);
            this.pnlAddRooms.Location = new System.Drawing.Point(0, 0);
            this.pnlAddRooms.Name = "pnlAddRooms";
            this.pnlAddRooms.Size = new System.Drawing.Size(507, 103);
            this.pnlAddRooms.TabIndex = 6;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(221, 79);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 45;
            this.pictureBox1.TabStop = false;
            // 
            // lblAddRooms
            // 
            this.lblAddRooms.AutoSize = true;
            this.lblAddRooms.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddRooms.ForeColor = System.Drawing.Color.White;
            this.lblAddRooms.Location = new System.Drawing.Point(353, 35);
            this.lblAddRooms.Name = "lblAddRooms";
            this.lblAddRooms.Size = new System.Drawing.Size(153, 36);
            this.lblAddRooms.TabIndex = 3;
            this.lblAddRooms.Text = "Add Room";
            // 
            // erpRoomNumber
            // 
            this.erpRoomNumber.ContainerControl = this;
            // 
            // erpRoomType
            // 
            this.erpRoomType.ContainerControl = this;
            // 
            // erpBedType
            // 
            this.erpBedType.ContainerControl = this;
            // 
            // erpPrice
            // 
            this.erpPrice.ContainerControl = this;
            // 
            // AddRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 431);
            this.Controls.Add(this.pnlRoomDetails);
            this.Controls.Add(this.pnlAddRooms);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddRoom";
            this.Text = "Add Rooms";
            this.pnlRoomDetails.ResumeLayout(false);
            this.pnlRoomDetails.PerformLayout();
            this.grpRoomDetails.ResumeLayout(false);
            this.grpRoomDetails.PerformLayout();
            this.pnlAddRooms.ResumeLayout(false);
            this.pnlAddRooms.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpRoomNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpRoomType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpBedType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpPrice)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlRoomDetails;
        private System.Windows.Forms.Panel pnlAddRooms;
        private System.Windows.Forms.Label lblAddRooms;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label lblRoomType;
        private System.Windows.Forms.Label lblRoomNumber;
        private System.Windows.Forms.TextBox txtRoomNumber;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ComboBox cmbBedType;
        private System.Windows.Forms.Label lblBedType;
        private System.Windows.Forms.ComboBox cmbRoomType;
        private System.Windows.Forms.GroupBox grpRoomDetails;
        private System.Windows.Forms.ErrorProvider erpRoomNumber;
        private System.Windows.Forms.ErrorProvider erpRoomType;
        private System.Windows.Forms.ErrorProvider erpBedType;
        private System.Windows.Forms.ErrorProvider erpPrice;
        private System.Windows.Forms.Label lblRoomDetails;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}