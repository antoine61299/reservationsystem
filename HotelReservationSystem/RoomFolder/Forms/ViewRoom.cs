﻿using HotelReservationSystem.DataLayer;
using HotelReservationSystem.Other;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelReservationSystem.RoomFolder
{
    public partial class ViewRoom : Form
    {
        public ViewRoom()
        {
            InitializeComponent();
            addDataToTable();
        }

        // Populate table in form with data from database.
        public void addDataToTable()
        {
            RoomManager rm = new RoomManager();
            List<Room> roomList = rm.GetAll();
            
            foreach(Room r in roomList)
            {
                dgvRooms.Rows.Add(r.roomNumber, r.roomType, r.bedType, r.price);
            }
        }

        private void dgvRooms_SelectionChanged(object sender, EventArgs e)
        {
            dgvRooms.ClearSelection();
        }
    }
}
