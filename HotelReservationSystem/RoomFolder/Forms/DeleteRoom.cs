﻿using HotelReservationSystem.DataLayer;
using HotelReservationSystem.Other;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelReservationSystem.RoomFolder
{
    public partial class DeleteRoom : Form
    {
        public DeleteRoom()
        {
            InitializeComponent();
            addDataToTable();
            foreach (DataGridViewRow selRow in dgvRooms.SelectedRows.OfType<DataGridViewRow>().ToArray())
            {
                dgvRooms.Rows.Remove(selRow);
            }
        }

        //Add data from database to the table in form. 
        public void addDataToTable()
        {
            RoomManager rm = new RoomManager();
            List<Room> roomList = rm.GetAll();

            foreach(Room r in roomList)
            {
                dgvRooms.Rows.Add(r.roomId, r.roomNumber, r.roomType, r.bedType, r.price);
            }
        }

        //Delete the selected record in the table form.
        public bool deleteDataInDatabase(int roomId)
        {
            bool roomIsBooked = false;

            RoomManager rm = new RoomManager();
            BookingRoomManager br = new BookingRoomManager();
            List<int> roomIdList = br.GetAllRoomId();

            if(roomIdList.Contains(roomId))
            {
                roomIsBooked = true;
            }

            if(!roomIsBooked)
            {
                rm.Delete(roomId);
            }
            return roomIsBooked;
        }

        // Event handler for when delete button is clicked
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvRooms.SelectedCells.Count > 0)
            {
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result = MessageBox.Show("Are you sure you want to delete this record?", "Delete Record", buttons, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    int roomId = int.Parse(dgvRooms.SelectedCells[0].Value.ToString());
                    bool isBooked = deleteDataInDatabase(roomId); 
                    
                    if(!isBooked)
                    {
                        dgvRooms.Rows.RemoveAt(dgvRooms.SelectedCells[0].RowIndex);
                        MessageBox.Show("Room is deleted", "Guest", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Room Cannot be deleted because it was previosuly booked", "Room Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }                
        }

        private void btnDelete_MouseHover(object sender, EventArgs e)
        {
            btnDelete.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnDelete.ForeColor = Color.Black;
        }

        private void btnDelete_MouseLeave(object sender, EventArgs e)
        {
            btnDelete.BackColor = Color.Black;
            btnDelete.ForeColor = Color.White;
        }
    }
}
