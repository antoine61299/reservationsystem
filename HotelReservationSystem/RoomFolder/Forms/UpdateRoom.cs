﻿using HotelReservationSystem.DataLayer;
using HotelReservationSystem.Other;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelReservationSystem.RoomFolder
{
    public partial class UpdateRoom : Form
    {
        public UpdateRoom()
        {
            InitializeComponent();
            disableElements();
            populateRoomIdComboBox();
            cmbRoomID.SelectedIndex = 0;
        }

        //This method clears the textboxes once a room is updated.
        public void clearData()
        {
            txtRoomNumber.Text = "";
            txtPrice.Text = "";
            cmbRoomID.SelectedIndex = 0;
            cmbRoomType.SelectedIndex = 0;
            cmbBedType.SelectedIndex = 0;
        }

        //This method enable buttons and other components
        public void enableElements()
        {
            txtRoomNumber.Enabled = true;
            cmbRoomType.Enabled = true;
            cmbBedType.Enabled = true;
            txtPrice.Enabled = true;
            btnUpdate.Visible = true;
            btnClear.Visible = true;
        }


        //This method disable buttons and other components
        public void disableElements()
        {
            txtRoomNumber.Enabled = false;
            cmbRoomType.Enabled = false;
            cmbBedType.Enabled = false;
            txtPrice.Enabled = false;
            btnUpdate.Visible = false;
            btnClear.Visible = false;
        }

        //Populate combobox with all room id's so that the one to be updated is selected.
        public void populateRoomIdComboBox()
        {
            RoomManager rm = new RoomManager();
            List<int> roomIdList = rm.GetAllRoomId();

            foreach(int roomId in roomIdList)
            {
                cmbRoomID.Items.Add(roomId);
            }
        }

        //Elements in form are populated with the respective data according to the chosen room id.
        public void setElementsInForm()
        {
            enableElements();
            RoomManager rm = new RoomManager();
            Room room = rm.GetSingle(int.Parse(cmbRoomID.SelectedItem.ToString()));

            txtRoomNumber.Text = room.roomNumber.ToString();
            cmbRoomType.SelectedItem = room.roomType.ToString();
            cmbBedType.SelectedItem = room.bedType.ToString();
            txtPrice.Text = room.price.ToString();           
        }

        //Get data from form and create a room object.
        public Room getDataFromForm()
        {
            int roomNumber = int.Parse(txtRoomNumber.Text);
            string roomType = cmbRoomType.SelectedItem.ToString();
            string bedType = cmbBedType.SelectedItem.ToString();
            float price = float.Parse(txtPrice.Text);

            Room room = new Room(roomNumber, roomType, bedType, price);

            return room;
        }

        public bool validateInputs()
        {
            bool validated = true;

            if (txtRoomNumber.Text == string.Empty)
            {
                erpRoomNumber.SetError(txtRoomNumber, "Please enter a room number");
                validated = false;
            }
            else
            {
                if (!int.TryParse(txtRoomNumber.Text, out int numValue))
                {
                    erpRoomNumber.SetError(txtRoomNumber, "Please enter a number");
                    validated = false;
                }
                else
                {
                    erpRoomNumber.Clear();
                }
            }

            if (cmbRoomType.Text == string.Empty)
            {
                erpRoomType.SetError(cmbRoomType, "Please select a room type");
                validated = false;
            }
            else
            {
                erpRoomType.Clear();
            }

            if (cmbBedType.Text == string.Empty)
            {
                erpBedType.SetError(cmbBedType, "Please select a room type");
                validated = false;
            }
            else
            {
                erpBedType.Clear();
            }

            if (txtPrice.Text == string.Empty)
            {
                erpPrice.SetError(txtPrice, "Please enter a price");
                validated = false;
            }
            else
            {
                if (!float.TryParse(txtPrice.Text, out float numValue))
                {
                    erpPrice.SetError(txtPrice, "Please enter a valid price");
                    validated = false;
                }
                else
                {
                    erpPrice.Clear();
                }
            }
            return validated;
        }

        //Event handler when a new item is selected in the roomId combobox.
        private void cmbRoomID_DropDownClosed(object sender, EventArgs e)
        {
            setElementsInForm();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (validateInputs())
            {
                RoomManager rm = new RoomManager();

                List<Room> rList = rm.GetAll();

                int dbRoomId = 0;
                foreach(Room r in rList)
                {
                    if(r.roomNumber == int.Parse(txtRoomNumber.Text))
                    {
                        dbRoomId = r.roomId;
                    }
                }

                if (rm.FindRoom(int.Parse(txtRoomNumber.Text)) && dbRoomId != int.Parse(cmbRoomID.SelectedItem.ToString()))
                {
                    MessageBox.Show("Room Number already exits. Please enter a unique number", "Room Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Room room = getDataFromForm();
                    MessageBox.Show("Room is updated", "Room", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    rm.Update(int.Parse(cmbRoomID.SelectedItem.ToString()), room);
                    clearData();
                    disableElements();
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clearData();
        }

        private void btnUpdate_MouseHover(object sender, EventArgs e)
        {
            btnUpdate.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnUpdate.ForeColor = Color.Black;
        }

        private void btnUpdate_MouseLeave(object sender, EventArgs e)
        {
            btnUpdate.BackColor = Color.Black;
            btnUpdate.ForeColor = Color.White;
        }

        private void btnClear_MouseHover(object sender, EventArgs e)
        {
            btnClear.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnClear.ForeColor = Color.Black;
        }

        private void btnClear_MouseLeave(object sender, EventArgs e)
        {
            btnClear.BackColor = Color.Black;
            btnClear.ForeColor = Color.White;
        }
    }
}
