﻿using HotelReservationSystem.DataLayer;
using HotelReservationSystem.Other;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelReservationSystem.RoomFolder
{
    public partial class AddRoom : Form
    {
        public AddRoom()
        {
            InitializeComponent();
            cmbRoomType.SelectedIndex = 0;
            cmbBedType.SelectedIndex = 0;
        }

        //This method clears the textboxes once a room is added.
        public void clearData()
        {
            cmbRoomType.SelectedIndex = 0;
            cmbBedType.SelectedIndex = 0;
            txtRoomNumber.Text = "";
            txtPrice.Text = "";
        }

        //All the data that is entred in the AddRoom Form will be stored in a different variables to form a whole object which is the Room.
        public Room getDataFromForm()
        {
            int roomNumber = int.Parse(txtRoomNumber.Text);
            string roomType = cmbRoomType.SelectedItem.ToString();
            string bedType = cmbBedType.SelectedItem.ToString();
            float price = float.Parse(txtPrice.Text);

            Room room = new Room(roomNumber, roomType, bedType, price);

            return room;
        }

        //Returns all the details of the room validated.
        public bool validateInputs()
        {
            bool validated = true;

            if (txtRoomNumber.Text == string.Empty)
            {
                erpRoomNumber.SetError(txtRoomNumber, "Please enter a room number");
                validated = false;
            }
            else
            {
                if (!int.TryParse(txtRoomNumber.Text, out int numValue))
                {
                    erpRoomNumber.SetError(txtRoomNumber, "Please enter a number");
                    validated = false;
                }
                else
                {
                    erpRoomNumber.Clear();
                }
            }

            if (cmbRoomType.Text == string.Empty)
            {
                erpRoomType.SetError(cmbRoomType, "Please select a room type");
                validated = false;
            }
            else
            {
                erpRoomType.Clear();
            }

            if (cmbBedType.Text == string.Empty)
            {
                erpBedType.SetError(cmbBedType, "Please select a room type");
                validated = false;
            }
            else
            {
                erpBedType.Clear();
            }

            if (txtPrice.Text == string.Empty)
            {
                erpPrice.SetError(txtPrice, "Please enter a price");
                validated = false;
            }
            else
            {
                if (!float.TryParse(txtPrice.Text, out float numValue))
                {
                    erpPrice.SetError(txtPrice, "Please enter a valid price");
                    validated = false;
                }
                else
                {
                    erpPrice.Clear();
                }
            }

            return validated;
        }

        //When the button is clicked all room details are validated and if they are validated then checks if room exists. If it does then, a message will be displayed to the user.
        private void btnAdd_Click(object sender, EventArgs e)
        {
            RoomManager rm = new RoomManager();

            if (validateInputs())
            {
                if (rm.FindRoom(int.Parse(txtRoomNumber.Text)))
                {
                    MessageBox.Show("Room Number already exits. Please enter a unique number", "Room Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Room room = getDataFromForm();
                    rm.Insert(room);
                    MessageBox.Show("Room is added", "Room", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    clearData();
                }                
            }           
        }

        // Event handler for clearing data.
        private void btnClear_Click(object sender, EventArgs e)
        {
            clearData();
        }

        private void btnAdd_MouseHover(object sender, EventArgs e)
        {
            btnAdd.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnAdd.ForeColor = Color.Black;
        }

        private void btnAdd_MouseLeave(object sender, EventArgs e)
        {
            btnAdd.BackColor = Color.Black;
            btnAdd.ForeColor = Color.White;
        }

        private void btnClear_MouseHover(object sender, EventArgs e)
        {
            btnClear.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnClear.ForeColor = Color.Black;
        }

        private void btnClear_MouseLeave(object sender, EventArgs e)
        {
            btnClear.BackColor = Color.Black;
            btnClear.ForeColor = Color.White;
        }
    }
}
