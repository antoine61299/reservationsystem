﻿namespace HotelReservationSystem.Other
{
    partial class MainScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainScreen));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.homeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.roomsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addRoomsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewRoomsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateRoomsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteRoomsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.managersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addManagersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewManagersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateManagersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteManagersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.receptionistsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addReceptionistsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewReceptionistsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateReceptionistsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteReceptionistsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guestsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addGuestsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewGuestsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateGuestsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteGuestsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reservationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addReservationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewReservationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateReservationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteReservationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addCheckInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addCheckInsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.viewCheckInsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateCheckInsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addCheckOutsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewCheckOutsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FloralWhite;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.homeToolStripMenuItem,
            this.roomsToolStripMenuItem,
            this.employeeListToolStripMenuItem,
            this.reservationsToolStripMenuItem,
            this.addCheckInToolStripMenuItem,
            this.checkOutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(960, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // homeToolStripMenuItem
            // 
            this.homeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logOutToolStripMenuItem});
            this.homeToolStripMenuItem.Name = "homeToolStripMenuItem";
            this.homeToolStripMenuItem.Size = new System.Drawing.Size(64, 24);
            this.homeToolStripMenuItem.Text = "Home";
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(145, 26);
            this.logOutToolStripMenuItem.Text = "Log Out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // roomsToolStripMenuItem
            // 
            this.roomsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addRoomsToolStripMenuItem,
            this.viewRoomsToolStripMenuItem,
            this.updateRoomsToolStripMenuItem,
            this.deleteRoomsToolStripMenuItem});
            this.roomsToolStripMenuItem.Name = "roomsToolStripMenuItem";
            this.roomsToolStripMenuItem.Size = new System.Drawing.Size(69, 24);
            this.roomsToolStripMenuItem.Text = "Rooms";
            // 
            // addRoomsToolStripMenuItem
            // 
            this.addRoomsToolStripMenuItem.Name = "addRoomsToolStripMenuItem";
            this.addRoomsToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.addRoomsToolStripMenuItem.Text = "Add Rooms";
            this.addRoomsToolStripMenuItem.Click += new System.EventHandler(this.addRoomsToolStripMenuItem_Click);
            // 
            // viewRoomsToolStripMenuItem
            // 
            this.viewRoomsToolStripMenuItem.Name = "viewRoomsToolStripMenuItem";
            this.viewRoomsToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.viewRoomsToolStripMenuItem.Text = "View Rooms";
            this.viewRoomsToolStripMenuItem.Click += new System.EventHandler(this.viewRoomsToolStripMenuItem_Click);
            // 
            // updateRoomsToolStripMenuItem
            // 
            this.updateRoomsToolStripMenuItem.Name = "updateRoomsToolStripMenuItem";
            this.updateRoomsToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.updateRoomsToolStripMenuItem.Text = "Update Rooms";
            this.updateRoomsToolStripMenuItem.Click += new System.EventHandler(this.updateRoomsToolStripMenuItem_Click);
            // 
            // deleteRoomsToolStripMenuItem
            // 
            this.deleteRoomsToolStripMenuItem.Name = "deleteRoomsToolStripMenuItem";
            this.deleteRoomsToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.deleteRoomsToolStripMenuItem.Text = "Delete Rooms";
            this.deleteRoomsToolStripMenuItem.Click += new System.EventHandler(this.deleteRoomsToolStripMenuItem_Click);
            // 
            // employeeListToolStripMenuItem
            // 
            this.employeeListToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.managersToolStripMenuItem,
            this.receptionistsToolStripMenuItem,
            this.guestsToolStripMenuItem});
            this.employeeListToolStripMenuItem.Name = "employeeListToolStripMenuItem";
            this.employeeListToolStripMenuItem.Size = new System.Drawing.Size(72, 24);
            this.employeeListToolStripMenuItem.Text = "Persons";
            // 
            // managersToolStripMenuItem
            // 
            this.managersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addManagersToolStripMenuItem,
            this.viewManagersToolStripMenuItem,
            this.updateManagersToolStripMenuItem,
            this.deleteManagersToolStripMenuItem});
            this.managersToolStripMenuItem.Name = "managersToolStripMenuItem";
            this.managersToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.managersToolStripMenuItem.Text = "Managers";
            // 
            // addManagersToolStripMenuItem
            // 
            this.addManagersToolStripMenuItem.Name = "addManagersToolStripMenuItem";
            this.addManagersToolStripMenuItem.Size = new System.Drawing.Size(210, 26);
            this.addManagersToolStripMenuItem.Text = "Add Managers";
            this.addManagersToolStripMenuItem.Click += new System.EventHandler(this.addManagerToolStripMenuItem_Click);
            // 
            // viewManagersToolStripMenuItem
            // 
            this.viewManagersToolStripMenuItem.Name = "viewManagersToolStripMenuItem";
            this.viewManagersToolStripMenuItem.Size = new System.Drawing.Size(210, 26);
            this.viewManagersToolStripMenuItem.Text = "View Managers";
            this.viewManagersToolStripMenuItem.Click += new System.EventHandler(this.viewManagersToolStripMenuItem_Click);
            // 
            // updateManagersToolStripMenuItem
            // 
            this.updateManagersToolStripMenuItem.Name = "updateManagersToolStripMenuItem";
            this.updateManagersToolStripMenuItem.Size = new System.Drawing.Size(210, 26);
            this.updateManagersToolStripMenuItem.Text = "Update Managers";
            this.updateManagersToolStripMenuItem.Click += new System.EventHandler(this.updateManagersToolStripMenuItem_Click);
            // 
            // deleteManagersToolStripMenuItem
            // 
            this.deleteManagersToolStripMenuItem.Name = "deleteManagersToolStripMenuItem";
            this.deleteManagersToolStripMenuItem.Size = new System.Drawing.Size(210, 26);
            this.deleteManagersToolStripMenuItem.Text = "Delete Managers";
            this.deleteManagersToolStripMenuItem.Click += new System.EventHandler(this.deleteManagersToolStripMenuItem_Click);
            // 
            // receptionistsToolStripMenuItem
            // 
            this.receptionistsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addReceptionistsToolStripMenuItem,
            this.viewReceptionistsToolStripMenuItem,
            this.updateReceptionistsToolStripMenuItem,
            this.deleteReceptionistsToolStripMenuItem});
            this.receptionistsToolStripMenuItem.Name = "receptionistsToolStripMenuItem";
            this.receptionistsToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.receptionistsToolStripMenuItem.Text = "Receptionists";
            // 
            // addReceptionistsToolStripMenuItem
            // 
            this.addReceptionistsToolStripMenuItem.Name = "addReceptionistsToolStripMenuItem";
            this.addReceptionistsToolStripMenuItem.Size = new System.Drawing.Size(233, 26);
            this.addReceptionistsToolStripMenuItem.Text = "Add Receptionists";
            this.addReceptionistsToolStripMenuItem.Click += new System.EventHandler(this.addReceptionistsToolStripMenuItem_Click);
            // 
            // viewReceptionistsToolStripMenuItem
            // 
            this.viewReceptionistsToolStripMenuItem.Name = "viewReceptionistsToolStripMenuItem";
            this.viewReceptionistsToolStripMenuItem.Size = new System.Drawing.Size(233, 26);
            this.viewReceptionistsToolStripMenuItem.Text = "View Receptionists";
            this.viewReceptionistsToolStripMenuItem.Click += new System.EventHandler(this.viewReceptionistsToolStripMenuItem_Click);
            // 
            // updateReceptionistsToolStripMenuItem
            // 
            this.updateReceptionistsToolStripMenuItem.Name = "updateReceptionistsToolStripMenuItem";
            this.updateReceptionistsToolStripMenuItem.Size = new System.Drawing.Size(233, 26);
            this.updateReceptionistsToolStripMenuItem.Text = "Update Receptionists";
            this.updateReceptionistsToolStripMenuItem.Click += new System.EventHandler(this.updateReceptionistsToolStripMenuItem_Click);
            // 
            // deleteReceptionistsToolStripMenuItem
            // 
            this.deleteReceptionistsToolStripMenuItem.Name = "deleteReceptionistsToolStripMenuItem";
            this.deleteReceptionistsToolStripMenuItem.Size = new System.Drawing.Size(233, 26);
            this.deleteReceptionistsToolStripMenuItem.Text = "Delete Receptionists";
            this.deleteReceptionistsToolStripMenuItem.Click += new System.EventHandler(this.deleteReceptionistsToolStripMenuItem_Click);
            // 
            // guestsToolStripMenuItem
            // 
            this.guestsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addGuestsToolStripMenuItem,
            this.viewGuestsToolStripMenuItem,
            this.updateGuestsToolStripMenuItem,
            this.deleteGuestsToolStripMenuItem});
            this.guestsToolStripMenuItem.Name = "guestsToolStripMenuItem";
            this.guestsToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.guestsToolStripMenuItem.Text = "Guests";
            // 
            // addGuestsToolStripMenuItem
            // 
            this.addGuestsToolStripMenuItem.Name = "addGuestsToolStripMenuItem";
            this.addGuestsToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.addGuestsToolStripMenuItem.Text = "Add Guests";
            this.addGuestsToolStripMenuItem.Click += new System.EventHandler(this.addGuestsToolStripMenuItem_Click);
            // 
            // viewGuestsToolStripMenuItem
            // 
            this.viewGuestsToolStripMenuItem.Name = "viewGuestsToolStripMenuItem";
            this.viewGuestsToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.viewGuestsToolStripMenuItem.Text = "View Guests";
            this.viewGuestsToolStripMenuItem.Click += new System.EventHandler(this.viewGuestsToolStripMenuItem_Click);
            // 
            // updateGuestsToolStripMenuItem
            // 
            this.updateGuestsToolStripMenuItem.Name = "updateGuestsToolStripMenuItem";
            this.updateGuestsToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.updateGuestsToolStripMenuItem.Text = "Update Guests";
            this.updateGuestsToolStripMenuItem.Click += new System.EventHandler(this.updateGuestsToolStripMenuItem_Click);
            // 
            // deleteGuestsToolStripMenuItem
            // 
            this.deleteGuestsToolStripMenuItem.Name = "deleteGuestsToolStripMenuItem";
            this.deleteGuestsToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.deleteGuestsToolStripMenuItem.Text = "Delete Guests";
            this.deleteGuestsToolStripMenuItem.Click += new System.EventHandler(this.deleteGuestsToolStripMenuItem_Click);
            // 
            // reservationsToolStripMenuItem
            // 
            this.reservationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addReservationsToolStripMenuItem,
            this.viewReservationsToolStripMenuItem,
            this.updateReservationsToolStripMenuItem,
            this.deleteReservationsToolStripMenuItem});
            this.reservationsToolStripMenuItem.Name = "reservationsToolStripMenuItem";
            this.reservationsToolStripMenuItem.Size = new System.Drawing.Size(106, 24);
            this.reservationsToolStripMenuItem.Text = "Reservations";
            // 
            // addReservationsToolStripMenuItem
            // 
            this.addReservationsToolStripMenuItem.Name = "addReservationsToolStripMenuItem";
            this.addReservationsToolStripMenuItem.Size = new System.Drawing.Size(228, 26);
            this.addReservationsToolStripMenuItem.Text = "Add Reservations";
            this.addReservationsToolStripMenuItem.Click += new System.EventHandler(this.addReservationsToolStripMenuItem_Click);
            // 
            // viewReservationsToolStripMenuItem
            // 
            this.viewReservationsToolStripMenuItem.Name = "viewReservationsToolStripMenuItem";
            this.viewReservationsToolStripMenuItem.Size = new System.Drawing.Size(228, 26);
            this.viewReservationsToolStripMenuItem.Text = "View Reservations";
            this.viewReservationsToolStripMenuItem.Click += new System.EventHandler(this.viewReservationsToolStripMenuItem_Click);
            // 
            // updateReservationsToolStripMenuItem
            // 
            this.updateReservationsToolStripMenuItem.Name = "updateReservationsToolStripMenuItem";
            this.updateReservationsToolStripMenuItem.Size = new System.Drawing.Size(228, 26);
            this.updateReservationsToolStripMenuItem.Text = "Update Reservations";
            this.updateReservationsToolStripMenuItem.Click += new System.EventHandler(this.updateReservationsToolStripMenuItem_Click);
            // 
            // deleteReservationsToolStripMenuItem
            // 
            this.deleteReservationsToolStripMenuItem.Name = "deleteReservationsToolStripMenuItem";
            this.deleteReservationsToolStripMenuItem.Size = new System.Drawing.Size(228, 26);
            this.deleteReservationsToolStripMenuItem.Text = "Delete Reservations";
            this.deleteReservationsToolStripMenuItem.Click += new System.EventHandler(this.deleteReservationsToolStripMenuItem_Click);
            // 
            // addCheckInToolStripMenuItem
            // 
            this.addCheckInToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addCheckInsToolStripMenuItem1,
            this.viewCheckInsToolStripMenuItem,
            this.updateCheckInsToolStripMenuItem});
            this.addCheckInToolStripMenuItem.Name = "addCheckInToolStripMenuItem";
            this.addCheckInToolStripMenuItem.Size = new System.Drawing.Size(78, 24);
            this.addCheckInToolStripMenuItem.Text = "Check In";
            // 
            // addCheckInsToolStripMenuItem1
            // 
            this.addCheckInsToolStripMenuItem1.Name = "addCheckInsToolStripMenuItem1";
            this.addCheckInsToolStripMenuItem1.Size = new System.Drawing.Size(224, 26);
            this.addCheckInsToolStripMenuItem1.Text = "Add Check Ins";
            this.addCheckInsToolStripMenuItem1.Click += new System.EventHandler(this.addCheckInsToolStripMenuItem1_Click);
            // 
            // viewCheckInsToolStripMenuItem
            // 
            this.viewCheckInsToolStripMenuItem.Name = "viewCheckInsToolStripMenuItem";
            this.viewCheckInsToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.viewCheckInsToolStripMenuItem.Text = "View Check Ins";
            this.viewCheckInsToolStripMenuItem.Click += new System.EventHandler(this.viewCheckInsToolStripMenuItem_Click);
            // 
            // updateCheckInsToolStripMenuItem
            // 
            this.updateCheckInsToolStripMenuItem.Name = "updateCheckInsToolStripMenuItem";
            this.updateCheckInsToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.updateCheckInsToolStripMenuItem.Text = "Update Check Ins";
            this.updateCheckInsToolStripMenuItem.Click += new System.EventHandler(this.updateCheckInsToolStripMenuItem_Click);
            // 
            // checkOutToolStripMenuItem
            // 
            this.checkOutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addCheckOutsToolStripMenuItem,
            this.viewCheckOutsToolStripMenuItem});
            this.checkOutToolStripMenuItem.Name = "checkOutToolStripMenuItem";
            this.checkOutToolStripMenuItem.Size = new System.Drawing.Size(90, 24);
            this.checkOutToolStripMenuItem.Text = "Check Out";
            // 
            // addCheckOutsToolStripMenuItem
            // 
            this.addCheckOutsToolStripMenuItem.Name = "addCheckOutsToolStripMenuItem";
            this.addCheckOutsToolStripMenuItem.Size = new System.Drawing.Size(201, 26);
            this.addCheckOutsToolStripMenuItem.Text = "Add Check Outs";
            this.addCheckOutsToolStripMenuItem.Click += new System.EventHandler(this.addCheckOutsToolStripMenuItem_Click);
            // 
            // viewCheckOutsToolStripMenuItem
            // 
            this.viewCheckOutsToolStripMenuItem.Name = "viewCheckOutsToolStripMenuItem";
            this.viewCheckOutsToolStripMenuItem.Size = new System.Drawing.Size(201, 26);
            this.viewCheckOutsToolStripMenuItem.Text = "View Check Outs";
            this.viewCheckOutsToolStripMenuItem.Click += new System.EventHandler(this.viewCheckOutsToolStripMenuItem_Click);
            // 
            // MainScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(960, 554);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "MainScreen";
            this.Text = "Main Screen";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem homeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem roomsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addRoomsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewRoomsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem managersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem receptionistsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addManagersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteManagersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addReceptionistsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteReceptionistsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateRoomsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteRoomsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guestsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reservationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addReservationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewReservationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateReservationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteReservationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addCheckInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addCheckInsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem viewCheckInsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateCheckInsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addCheckOutsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewCheckOutsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewManagersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateManagersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addGuestsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewGuestsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateGuestsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteGuestsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewReceptionistsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateReceptionistsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
    }
}