﻿using HotelReservationSystem.DataLayer;
using HotelReservationSystem.PersonFolder;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelReservationSystem.Other
{
    public partial class LogIn : Form
    {
        public LogIn()
        {
            InitializeComponent();
        }

        public void cleardata()
        {
            txtUsername.Text = "";
            txtPassword.Text = "";
        }

        public bool validateInputs()
        {
            bool validated = true;

            if (txtUsername.Text == string.Empty)
            {
                erpUsername.SetError(txtUsername, "Please enter your username");
                validated = false;
            }
            else
            {
                erpUsername.Clear();
            }

            if (txtPassword.Text == string.Empty)
            {
                erpPassword.SetError(txtPassword, "Please enter your password");
                validated = false;
            }
            else
            {
                erpPassword.Clear();
            }
            return validated;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (validateInputs())
            {
                EmployeeManager em = new EmployeeManager();
                string personType = em.checkUsernamePassword(txtUsername.Text, txtPassword.Text);

                if (personType.Equals(""))
                {
                    MessageBox.Show("Username or Password is incorrect!", "Username", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cleardata();
                }
                else 
                {
                    MessageBox.Show("Welcome to Twanny's estate booking system", "Welcome", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    MainScreen mainScreenForm = new MainScreen(personType);
                    mainScreenForm.Show();
                    Visible = false;
                    cleardata();
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            cleardata();
        }

        private void btnLogin_MouseHover(object sender, EventArgs e)
        {
            btnLogin.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnLogin.ForeColor = Color.Black;
        }

        private void btnLogin_MouseLeave(object sender, EventArgs e)
        {
            btnLogin.BackColor = Color.Black;
            btnLogin.ForeColor = Color.White;
        }

        private void btnClear_MouseHover(object sender, EventArgs e)
        {
            btnClear.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnClear.ForeColor = Color.Black;
        }

        private void btnClear_MouseLeave(object sender, EventArgs e)
        {
            btnClear.BackColor = Color.Black;
            btnClear.ForeColor = Color.White;
        }
    }
}
