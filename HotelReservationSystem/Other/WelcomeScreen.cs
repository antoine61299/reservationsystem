﻿using HotelReservationSystem.PersonFolder;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelReservationSystem.Other
{
    public partial class WelcomeScreen : Form
    {
        public WelcomeScreen()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Welcome to Twanny's Estate. Please Log in", "Greeting", MessageBoxButtons.OK, MessageBoxIcon.Information);
            LogIn logInForm = new LogIn();
            logInForm.Show();
            Visible = false;
        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            AddPerson addPersonForm = new AddPerson();
            addPersonForm.rbnReceptionist.Checked = true;
            addPersonForm.gbxPersonType.Visible = false;
            addPersonForm.gbxEmployeeDetails.Visible = true;
            addPersonForm.Show(); 
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnLogin_MouseHover(object sender, EventArgs e)
        {
            btnLogin.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnLogin.ForeColor = Color.Black;
        }

        private void btnLogin_MouseLeave(object sender, EventArgs e)
        {
            btnLogin.BackColor = Color.Black;
            btnLogin.ForeColor = Color.White;
        }

        private void btnSignUp_MouseHover(object sender, EventArgs e)
        {
            btnSignUp.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnSignUp.ForeColor = Color.Black;
        }

        private void btnSignUp_MouseLeave(object sender, EventArgs e)
        {
            btnSignUp.BackColor = Color.Black;
            btnSignUp.ForeColor = Color.White;
        }

        private void btnExit_MouseHover(object sender, EventArgs e)
        {
            btnExit.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnExit.ForeColor = Color.Black;
        }

        private void btnExit_MouseLeave(object sender, EventArgs e)
        {
            btnExit.BackColor = Color.Black;
            btnExit.ForeColor = Color.White;
        }
    }
}
