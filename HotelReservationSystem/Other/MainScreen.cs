﻿using HotelReservationSystem.CheckInFolder;
using HotelReservationSystem.CheckOutFolder;
using HotelReservationSystem.DataLayer;
using HotelReservationSystem.PersonFolder;
using HotelReservationSystem.ReservationFolder;
using HotelReservationSystem.RoomFolder;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelReservationSystem.Other
{
    public partial class MainScreen : Form
    {
        public MainScreen(string userType)
        {
            InitializeComponent();

            if (userType.Equals("Receptionist"))
            {
                addRoomsToolStripMenuItem.Visible = false;
                updateRoomsToolStripMenuItem.Visible = false;
                deleteRoomsToolStripMenuItem.Visible = false;
                managersToolStripMenuItem.Visible = false;
                receptionistsToolStripMenuItem.Visible = false;
            }
        }

        //These event handlers open the room forms
        private void addRoomsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddRoom addRoomForm = new AddRoom();
            addRoomForm.Show();
        }

        private void viewRoomsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewRoom viewRoomForm = new ViewRoom();
            viewRoomForm.Show();
        }

        private void updateRoomsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateRoom updateRoomForm = new UpdateRoom();
            updateRoomForm.Show();
        }

        private void deleteRoomsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteRoom deleteRoomForm = new DeleteRoom();
            deleteRoomForm.Show();
        }

        private void addManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddPerson addPersonForm = new AddPerson();
            addPersonForm.rbnManager.Checked = true;
            addPersonForm.gbxPersonType.Visible = false;
            addPersonForm.gbxEmployeeDetails.Visible = true;
            addPersonForm.Show();
        }

        private void viewManagersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewPerson viewPersonForm = new ViewPerson("Manager");
            viewPersonForm.Show();
        }

        private void updateManagersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdatePerson updatePersonForm = new UpdatePerson();
            updatePersonForm.rbnManager.Checked = true;
            updatePersonForm.gbxPersonType.Visible = false;
            updatePersonForm.gbxEmployeeDetails.Visible = true;
            updatePersonForm.gbxPersonDetails.Enabled = true;
            updatePersonForm.populatePersonIdComboBox("Manager");
            updatePersonForm.Show();
        }

        private void deleteManagersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeletePerson deletePersonForm = new DeletePerson("Manager");
            deletePersonForm.Show();
        }

        private void addReceptionistsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddPerson addPersonForm = new AddPerson();
            addPersonForm.rbnReceptionist.Checked = true;
            addPersonForm.gbxEmployeeDetails.Visible = true;
            addPersonForm.gbxPersonType.Visible = false;
            addPersonForm.Show();
        }

        private void viewReceptionistsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewPerson viewPersonForm = new ViewPerson("Receptionist");
            viewPersonForm.Show();
        }

        private void updateReceptionistsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdatePerson updatePersonForm = new UpdatePerson();
            updatePersonForm.rbnReceptionist.Checked = true;
            updatePersonForm.gbxEmployeeDetails.Visible = true;
            updatePersonForm.gbxPersonType.Visible = false;
            updatePersonForm.populatePersonIdComboBox("Receptionist");
            updatePersonForm.Show();
        }

        private void deleteReceptionistsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeletePerson addPersonForm = new DeletePerson("Receptionist");
            addPersonForm.Show();
        }

        private void addGuestsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddPerson addPersonForm = new AddPerson();
            addPersonForm.rbnGuest.Checked = true;
            addPersonForm.gbxPersonType.Visible = false;
            addPersonForm.gbxEmployeeDetails.Visible = false;
            addPersonForm.Show();
        }

        private void viewGuestsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewPerson viewPersonForm = new ViewPerson("Guest");
            viewPersonForm.Show();
        }

        private void updateGuestsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdatePerson updatePersonForm = new UpdatePerson();
            updatePersonForm.rbnGuest.Checked = true;
            updatePersonForm.gbxPersonType.Visible = false;
            updatePersonForm.gbxEmployeeDetails.Visible = false;
            updatePersonForm.gbxPersonDetails.Enabled = true;
            updatePersonForm.populatePersonIdComboBox("Guest");
            updatePersonForm.Show();
        }

        private void deleteGuestsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeletePerson addPersonForm = new DeletePerson("Guest");
            addPersonForm.Show();
        }

        private void addReservationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddReservation addReservationForm = new AddReservation();
            addReservationForm.Show();
        }

        private void viewReservationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewReservation viewReservationForm = new ViewReservation();
            viewReservationForm.Show();
        }

        private void updateReservationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateReservation updateReservationForm = new UpdateReservation();
            updateReservationForm.Show();
        }

        private void deleteReservationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteReservation deleteReservationForm = new DeleteReservation();
            deleteReservationForm.Show();
        }

        private void addCheckInsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AddCheckIn addCheckInForm = new AddCheckIn();
            addCheckInForm.Show();
        }

        private void viewCheckInsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewCheckIn viewCheckInForm = new ViewCheckIn();
            viewCheckInForm.Show();
        }

        private void updateCheckInsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateCheckIn updateCheckInForm = new UpdateCheckIn();
            updateCheckInForm.Show();
        }

        private void addCheckOutsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddCheckOut addCheckOutForm = new AddCheckOut();
            addCheckOutForm.Show();
        }

        private void viewCheckOutsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewCheckOut viewCheckOutForm = new ViewCheckOut();
            viewCheckOutForm.Show();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Visible = false;
            WelcomeScreen welcomeScreenForm = new WelcomeScreen();
            welcomeScreenForm.Show();
        }
    }
}
