﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelReservationSystem.Other
{
    public class ReservationView
    {
        public string bookingNo;
        public string guestId;
        public string noOfPersons;
        public string noOfNights;
        public string arrival;
        public string departure;
        public string rooms;
        public string personName;
        public string personSurname;
        public string personDob;
        public string personGender;
        public string nationality;
        public string email;
        public string mobile;
        public string roomNumber;
        public string roomType;
        public string bedType;
        public string price;


        public ReservationView(string bookingNo, string guestId, string noOfPersons, string noOfNights, string arrival, string departure, string rooms)
        {
            this.bookingNo = bookingNo;
            this.guestId = guestId;
            this.noOfPersons = noOfPersons;
            this.noOfNights = noOfNights;
            this.arrival = arrival;
            this.departure = departure;
            this.rooms = rooms;
        }


        public ReservationView(string bookingNo, string guestId, string noOfPersons, string noOfNights, string arrival, string departure, string personName, string personSurname, string personDob,string personGender, string nationality, string email, string mobile, string roomNumber, string roomType, string bedType, string price)
        {
            this.bookingNo = bookingNo;
            this.guestId = guestId;
            this.noOfPersons = noOfPersons;
            this.noOfNights = noOfNights;
            this.arrival = arrival;
            this.departure = departure;
            this.personName = personName;
            this.personSurname = personSurname;
            this.personDob = personDob;
            this.personGender = personGender;
            this.nationality = nationality;
            this.email = email;
            this.mobile = mobile;
            this.roomNumber = roomNumber;
            this.roomType = roomType;
            this.bedType = bedType;
            this.price = price;
        }
    }
}
