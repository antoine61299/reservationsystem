﻿using HotelReservationSystem.ReservationFolder.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelReservationSystem.ReservationFolder
{
    public class Booking
    {
        public int bookingNo;
        public DateTime arrivalDate;
        public DateTime departureDate;
        public int numberOfNights;
        public int numberOfPersons;
        public int guestID;
        public string status;
        public double discount;
        public double bill;
        public List<int> rooms;

        public Booking(DateTime arrivalDate, DateTime departureDate, int numberOfNights, int numberOfPersons, int guestID, string status, double discount, double bill, List<int> rooms)
        {
            this.arrivalDate = arrivalDate;
            this.departureDate = departureDate;
            this.numberOfNights = numberOfNights;
            this.numberOfPersons = numberOfPersons;
            this.guestID = guestID;
            this.status = status;
            this.discount = discount;
            this.bill = bill;
            this.rooms = rooms;
        }

        public Booking(int bookingNo, DateTime arrivalDate, DateTime departureDate, int numberOfNights, int numberOfPersons, int guestID, string status, double discount, double bill, List<int> rooms)
        {
            this.bookingNo = bookingNo;
            this.arrivalDate = arrivalDate;
            this.departureDate = departureDate;
            this.numberOfNights = numberOfNights;
            this.numberOfPersons = numberOfPersons;
            this.guestID = guestID;
            this.status = status;
            this.discount = discount;
            this.bill = bill;
            this.rooms = rooms;
        }
    }
}
