﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelReservationSystem.ReservationFolder.Objects
{
    public class BookingRoom
    {
        public int bookingNo;
        public int roomNo;

        public BookingRoom(int bookingNo, int roomNo)
        {
            this.bookingNo = bookingNo;
            this.roomNo = roomNo;
        }
    }
}
