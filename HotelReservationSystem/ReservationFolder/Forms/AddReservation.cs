﻿using HotelReservationSystem.DataLayer;
using HotelReservationSystem.PersonFolder;
using HotelReservationSystem.ReservationFolder.Objects;
using HotelReservationSystem.RoomFolder;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelReservationSystem.ReservationFolder
{
    public partial class AddReservation : Form
    {
        public AddReservation()
        {
            InitializeComponent();      
            disableElements();
            populatePersonIdComboBox();
            cmbGuestID.SelectedIndex = 0;
        }

        public void clearData()
        {
            lblNumberOfNightsValue.Text = "";
            txtNumberOfPersons.Text = "";
            grpPersonDetails.Text = "";
            dtpArrivalDate.Value = DateTime.Now;
            dtpDepartureDate.Value = DateTime.Now;
            lblNumberOfNightsValue.Text = "";
            dgvAvailableRooms.Rows.Clear();
            dgvBookedRooms.Rows.Clear();
        }

        public void enableElements()
        {
            lblNumberOfNightsValue.Enabled = true;
            txtNumberOfPersons.Enabled = true;
            dtpArrivalDate.Enabled = true;
            dtpDepartureDate.Enabled = true;
            grpPersonDetails.Enabled = true;
            lblAvailableRooms.Enabled = true;
            lblBookedRooms.Enabled = true;
            dgvAvailableRooms.Enabled = true;
            dgvBookedRooms.Enabled = true;
            btnAdd.Visible = true;
            btnRemove.Visible = true;
            btnBook.Visible = true;
            btnClear.Visible = true;
        }

        public void disableElements()
        {
            lblNumberOfNightsValue.Enabled = false;
            txtNumberOfPersons.Enabled = false;
            dtpArrivalDate.Enabled = false;
            dtpDepartureDate.Enabled = false;
            grpPersonDetails.Enabled = false;
            lblAvailableRooms.Enabled = false;
            lblBookedRooms.Enabled = false;
            dgvAvailableRooms.Enabled = false;
            dgvBookedRooms.Enabled = false;
            btnAdd.Visible = false;
            btnRemove.Visible = false;
            btnBook.Visible = false;
            btnClear.Visible = false;
        }

        public void populatePersonIdComboBox()
        {
            GuestManager gm = new GuestManager();
            List<int> dbGuestId = gm.GetAllGuestId();
            foreach(int id in dbGuestId)
            {
                cmbGuestID.Items.Add(id);
            }
        }

        //Elements in form are populated with the respective data according to the chosen guest id.
        public void setElementsInForm()
        {
            GuestManager gm = new GuestManager();

            Guest g = gm.GetSingle(int.Parse(cmbGuestID.SelectedItem.ToString()));
            
            lblPersonName.Text = g.name;
            lblPersonSurname.Text = g.surname;
            lblPersonDateOfBirth.Text = g.dateOfBirth;
                
            if (g.gender.Equals("Male"))
            {
                lblPersonGender.Text = "Male";
            }
            else
            {
                lblPersonGender.Text = "Female";
            }
            lblPersonNationality.Text = g.nationality;
            lblPersonEmail.Text = g.email;
            lblPersonMobileNumber.Text = g.mobileNumber;          
        }

        //This method
        private void cmbGuestID_DropDownClosed(object sender, EventArgs e)
        {
            enableElements();
            setElementsInForm();
            appendAvailableRooms();
        }

        public void appendAvailableRooms()
        {
            List<int> bookedRooms = getBookedRooms(DateTime.Parse(dtpArrivalDate.Text), DateTime.Parse(dtpDepartureDate.Text));
            dgvAvailableRooms.Rows.Clear();
            dgvBookedRooms.Rows.Clear();

            RoomManager rm = new RoomManager();
            List<Room> dbRoom = rm.GetAll();
            foreach(Room r in dbRoom)
            {
                if (!bookedRooms.Contains(r.roomId))
                {
                    dgvAvailableRooms.Rows.Add(r.roomNumber, r.roomType, r.bedType, r.price);
                }
            }
        }

        public List<int> getBookedRooms(DateTime formArrival, DateTime formDeparture)
        {
            List<int> bookedRooms = new List<int>();

            List<BookingRoom> dbBookingRooms = getBookingRoom();

            BookingManager bm = new BookingManager();
            List<Booking> dbBooking = bm.GetAll();

            foreach(Booking b in dbBooking)
            {
                DateTime dbArrival = b.arrivalDate;
                DateTime dbDeparture = b.departureDate;
                string bookingStatus = b.status;

                if (!bookingStatus.Equals("Checked Out"))
                {
                    if (formArrival >= dbArrival && formArrival <= dbDeparture)
                    {
                        foreach (BookingRoom br in dbBookingRooms)
                        {
                            if (br.bookingNo == b.bookingNo)
                            {
                                bookedRooms.Add(br.roomNo);
                            }
                        }
                    }
                    else if (formArrival <= dbArrival && formDeparture >= dbArrival)
                    {
                        foreach (BookingRoom br in dbBookingRooms)
                        {
                            if (br.bookingNo == b.bookingNo)
                            {
                                bookedRooms.Add(br.roomNo);
                            }
                        }
                    }
                }
            }
            return bookedRooms;
        }

        public List<BookingRoom> getBookingRoom()
        {
            BookingRoomManager brm = new BookingRoomManager();
            List<BookingRoom> dbBookingRoom = brm.GetAll();
            return dbBookingRoom;
        }

        public Booking getDataFromForm()
        {
            DateTime arrivalDate = DateTime.Parse(dtpArrivalDate.Text).Date;
            DateTime departureDate = DateTime.Parse(dtpDepartureDate.Text).Date;
            int numberOfNights = int.Parse(lblNumberOfNightsValue.Text);
            int numberOfPersons = int.Parse(txtNumberOfPersons.Text);
            int guestID = int.Parse(cmbGuestID.SelectedItem.ToString());
            string status = "Reserved";

            List<int> rooms = new List<int>();
            foreach (DataGridViewRow row in dgvBookedRooms.Rows)
            {
                rooms.Add(int.Parse(row.Cells[0].Value.ToString()));
            }

            Booking booking = new Booking(arrivalDate, departureDate, numberOfNights, numberOfPersons, guestID, status, 0, 0, rooms);
            return booking;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow selRow in dgvAvailableRooms.SelectedRows.OfType<DataGridViewRow>().ToArray())
            {
                dgvAvailableRooms.Rows.Remove(selRow);
                dgvBookedRooms.Rows.Add(selRow);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow selRow in dgvBookedRooms.SelectedRows.OfType<DataGridViewRow>().ToArray())
            {
                dgvBookedRooms.Rows.Remove(selRow);
                dgvAvailableRooms.Rows.Add(selRow);
            }
        }

        public void setNumberOfNights()
        {
            int numberOfNights = (int) DateTime.Parse(dtpDepartureDate.Text).Subtract(DateTime.Parse(dtpArrivalDate.Text)).TotalDays;
            lblNumberOfNightsValue.Text = numberOfNights.ToString();
        }

        private void dtpArrivalDate_ValueChanged(object sender, EventArgs e)
        {
            setNumberOfNights();
            appendAvailableRooms();
        }

        private void dtpDepartureDate_ValueChanged(object sender, EventArgs e)
        {
            setNumberOfNights();
            appendAvailableRooms();
        }

        //Returns all the details of the room validated.
        public bool validateInputs()
        {
            bool validated = true;

            if (cmbGuestID.Text == string.Empty)
            {
                erpGuestID.SetError(cmbGuestID, "Please select the guest id");
                validated = false;
            }
            else
            {
                if (!int.TryParse(cmbGuestID.Text, out int numValue))
                {
                    erpGuestID.SetError(cmbGuestID, "Please select the guest id");
                    validated = false;
                }
                else
                {
                    erpGuestID.Clear();
                }
            }

            if (txtNumberOfPersons.Text == string.Empty)
            {
                erpNumberOfPersons.SetError(txtNumberOfPersons, "Please enter the number of persons");
                validated = false;
            }
            else
            {
                if (!int.TryParse(txtNumberOfPersons.Text, out int numValue))
                {
                    erpNumberOfPersons.SetError(txtNumberOfPersons, "Please select the guest id");
                    validated = false;
                }
                else
                {
                    erpNumberOfPersons.Clear();
                }
            }

            if (dtpArrivalDate.Value >= dtpDepartureDate.Value)
            {
                erpArrivalDate.SetError(dtpArrivalDate, "Arrival date must be before departure date");
                erpDepartureDate.SetError(dtpDepartureDate, "Departure date must be after arrival date");
                validated = false;
            }
            else
            {
                erpArrivalDate.Clear();
                erpDepartureDate.Clear();
            }

            if (dgvBookedRooms.Rows == null || dgvBookedRooms.Rows.Count == 0)
            {
                erpDepartureDate.SetError(dgvBookedRooms, "A room must be added");
                validated = false;
            }
            else
            {
                erpDepartureDate.Clear();
            }

            return validated;
        }

        private void btnBook_Click(object sender, EventArgs e)
        {
            if (validateInputs())
            {
                BookingManager bm = new BookingManager();
                Booking booking = getDataFromForm();
                bm.Insert(booking);
                clearData();
                disableElements();
            }

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clearData();
            cmbGuestID.SelectedIndex = 0;
        }

        private void btnBook_MouseHover(object sender, EventArgs e)
        {
            btnBook.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnBook.ForeColor = Color.Black;
        }

        private void btnBook_MouseLeave(object sender, EventArgs e)
        {
            btnBook.BackColor = Color.Black;
            btnBook.ForeColor = Color.White;
        }

        private void btnClear_MouseHover(object sender, EventArgs e)
        {
            btnClear.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnClear.ForeColor = Color.Black;
        }

        private void btnClear_MouseLeave(object sender, EventArgs e)
        {
            btnClear.BackColor = Color.Black;
            btnClear.ForeColor = Color.White;
        }

        private void btnAdd_MouseHover(object sender, EventArgs e)
        {
            btnAdd.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnAdd.ForeColor = Color.Black;
        }

        private void btnAdd_MouseLeave(object sender, EventArgs e)
        {
            btnAdd.BackColor = Color.Black;
            btnAdd.ForeColor = Color.White;
        }

        private void btnRemove_MouseHover(object sender, EventArgs e)
        {
            btnAdd.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnAdd.ForeColor = Color.Black;
        }

        private void btnRemove_MouseLeave(object sender, EventArgs e)
        {
            btnRemove.BackColor = Color.Black;
            btnRemove.ForeColor = Color.White;
        }
    }
}


