﻿namespace HotelReservationSystem.ReservationFolder
{
    partial class DeleteReservation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeleteReservation));
            this.pnlDeleteReservation = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblDeleteReservation = new System.Windows.Forms.Label();
            this.pnlReservationDetails = new System.Windows.Forms.Panel();
            this.lblDeleteReservations = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.dgvReservations = new System.Windows.Forms.DataGridView();
            this.BookingNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GuestID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumberOfPersons = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumberOfNights = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ArrivalDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DepartureDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RoomNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlDeleteReservation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlReservationDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReservations)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlDeleteReservation
            // 
            this.pnlDeleteReservation.BackColor = System.Drawing.Color.DarkTurquoise;
            this.pnlDeleteReservation.Controls.Add(this.pictureBox1);
            this.pnlDeleteReservation.Controls.Add(this.lblDeleteReservation);
            this.pnlDeleteReservation.Location = new System.Drawing.Point(0, 0);
            this.pnlDeleteReservation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlDeleteReservation.Name = "pnlDeleteReservation";
            this.pnlDeleteReservation.Size = new System.Drawing.Size(1089, 123);
            this.pnlDeleteReservation.TabIndex = 40;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(16, 15);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(295, 97);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 58;
            this.pictureBox1.TabStop = false;
            // 
            // lblDeleteReservation
            // 
            this.lblDeleteReservation.AutoSize = true;
            this.lblDeleteReservation.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeleteReservation.ForeColor = System.Drawing.Color.White;
            this.lblDeleteReservation.Location = new System.Drawing.Point(690, 44);
            this.lblDeleteReservation.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDeleteReservation.Name = "lblDeleteReservation";
            this.lblDeleteReservation.Size = new System.Drawing.Size(354, 42);
            this.lblDeleteReservation.TabIndex = 3;
            this.lblDeleteReservation.Text = "Delete Reservation";
            // 
            // pnlReservationDetails
            // 
            this.pnlReservationDetails.BackColor = System.Drawing.Color.FloralWhite;
            this.pnlReservationDetails.Controls.Add(this.lblDeleteReservations);
            this.pnlReservationDetails.Controls.Add(this.btnDelete);
            this.pnlReservationDetails.Controls.Add(this.dgvReservations);
            this.pnlReservationDetails.Location = new System.Drawing.Point(0, 123);
            this.pnlReservationDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlReservationDetails.Name = "pnlReservationDetails";
            this.pnlReservationDetails.Size = new System.Drawing.Size(1089, 700);
            this.pnlReservationDetails.TabIndex = 41;
            // 
            // lblDeleteReservations
            // 
            this.lblDeleteReservations.AutoSize = true;
            this.lblDeleteReservations.BackColor = System.Drawing.Color.Transparent;
            this.lblDeleteReservations.Font = new System.Drawing.Font("Candara", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeleteReservations.Location = new System.Drawing.Point(360, 30);
            this.lblDeleteReservations.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDeleteReservations.Name = "lblDeleteReservations";
            this.lblDeleteReservations.Size = new System.Drawing.Size(414, 37);
            this.lblDeleteReservations.TabIndex = 57;
            this.lblDeleteReservations.Text = "Choose a reservation to delete";
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnDelete.Font = new System.Drawing.Font("Candara", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Location = new System.Drawing.Point(917, 633);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(155, 44);
            this.btnDelete.TabIndex = 54;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            this.btnDelete.MouseLeave += new System.EventHandler(this.btnDelete_MouseLeave);
            this.btnDelete.MouseHover += new System.EventHandler(this.btnDelete_MouseHover);
            // 
            // dgvReservations
            // 
            this.dgvReservations.AllowUserToAddRows = false;
            this.dgvReservations.BackgroundColor = System.Drawing.SystemColors.WindowFrame;
            this.dgvReservations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReservations.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BookingNumber,
            this.GuestID,
            this.NumberOfPersons,
            this.NumberOfNights,
            this.ArrivalDate,
            this.DepartureDate,
            this.RoomNumber});
            this.dgvReservations.Location = new System.Drawing.Point(17, 86);
            this.dgvReservations.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvReservations.Name = "dgvReservations";
            this.dgvReservations.RowHeadersWidth = 5;
            this.dgvReservations.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvReservations.Size = new System.Drawing.Size(1055, 521);
            this.dgvReservations.TabIndex = 1;
            // 
            // BookingNumber
            // 
            this.BookingNumber.HeaderText = "Booking Number";
            this.BookingNumber.MinimumWidth = 6;
            this.BookingNumber.Name = "BookingNumber";
            this.BookingNumber.Width = 110;
            // 
            // GuestID
            // 
            this.GuestID.HeaderText = "Guest ID";
            this.GuestID.MinimumWidth = 6;
            this.GuestID.Name = "GuestID";
            this.GuestID.Width = 75;
            // 
            // NumberOfPersons
            // 
            this.NumberOfPersons.HeaderText = "Number Of Persons";
            this.NumberOfPersons.MinimumWidth = 6;
            this.NumberOfPersons.Name = "NumberOfPersons";
            this.NumberOfPersons.Width = 125;
            // 
            // NumberOfNights
            // 
            this.NumberOfNights.HeaderText = "Number Of Nights";
            this.NumberOfNights.MinimumWidth = 6;
            this.NumberOfNights.Name = "NumberOfNights";
            this.NumberOfNights.Width = 125;
            // 
            // ArrivalDate
            // 
            this.ArrivalDate.HeaderText = "Arrival Date";
            this.ArrivalDate.MinimumWidth = 6;
            this.ArrivalDate.Name = "ArrivalDate";
            this.ArrivalDate.Width = 125;
            // 
            // DepartureDate
            // 
            this.DepartureDate.DividerWidth = 2;
            this.DepartureDate.HeaderText = "Departure Date";
            this.DepartureDate.MinimumWidth = 6;
            this.DepartureDate.Name = "DepartureDate";
            this.DepartureDate.Width = 125;
            // 
            // RoomNumber
            // 
            this.RoomNumber.HeaderText = "Room Number";
            this.RoomNumber.MinimumWidth = 6;
            this.RoomNumber.Name = "RoomNumber";
            this.RoomNumber.Width = 125;
            // 
            // DeleteReservation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1093, 821);
            this.Controls.Add(this.pnlDeleteReservation);
            this.Controls.Add(this.pnlReservationDetails);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "DeleteReservation";
            this.Text = "Delete Reservations";
            this.pnlDeleteReservation.ResumeLayout(false);
            this.pnlDeleteReservation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlReservationDetails.ResumeLayout(false);
            this.pnlReservationDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReservations)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlDeleteReservation;
        private System.Windows.Forms.Label lblDeleteReservation;
        private System.Windows.Forms.Panel pnlReservationDetails;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.DataGridView dgvReservations;
        private System.Windows.Forms.Label lblDeleteReservations;
        private System.Windows.Forms.DataGridViewTextBoxColumn BookingNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn GuestID;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumberOfPersons;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumberOfNights;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArrivalDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn DepartureDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoomNumber;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}