﻿using System;

namespace HotelReservationSystem.ReservationFolder
{
    partial class AddReservation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddReservation));
            this.pnlReservationDetails = new System.Windows.Forms.Panel();
            this.grpPersonDetails = new System.Windows.Forms.GroupBox();
            this.lblPersonMobileNumber = new System.Windows.Forms.Label();
            this.lblMoNumber = new System.Windows.Forms.Label();
            this.lblPersonEmail = new System.Windows.Forms.Label();
            this.lblPersonNationality = new System.Windows.Forms.Label();
            this.lblPersonGender = new System.Windows.Forms.Label();
            this.lblPersonDateOfBirth = new System.Windows.Forms.Label();
            this.lblPersonSurname = new System.Windows.Forms.Label();
            this.lblSurname = new System.Windows.Forms.Label();
            this.lblPersonName = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblMobileNumber = new System.Windows.Forms.Label();
            this.txtMobileNumber = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblDateOfBirth = new System.Windows.Forms.Label();
            this.lblGender = new System.Windows.Forms.Label();
            this.lblNationality = new System.Windows.Forms.Label();
            this.grpBookingDetails = new System.Windows.Forms.GroupBox();
            this.lblNumberOfNightsValue = new System.Windows.Forms.Label();
            this.lblGuestID = new System.Windows.Forms.Label();
            this.cmbGuestID = new System.Windows.Forms.ComboBox();
            this.lblNumberOfNights = new System.Windows.Forms.Label();
            this.lblNumberOfPersons = new System.Windows.Forms.Label();
            this.txtNumberOfPersons = new System.Windows.Forms.TextBox();
            this.dtpDepartureDate = new System.Windows.Forms.DateTimePicker();
            this.lblArrivalDate = new System.Windows.Forms.Label();
            this.dtpArrivalDate = new System.Windows.Forms.DateTimePicker();
            this.lblDepartureDate = new System.Windows.Forms.Label();
            this.btnRemove = new System.Windows.Forms.Button();
            this.lblAddReservations = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.dgvBookedRooms = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnBook = new System.Windows.Forms.Button();
            this.lblBookedRooms = new System.Windows.Forms.Label();
            this.lblAvailableRooms = new System.Windows.Forms.Label();
            this.dgvAvailableRooms = new System.Windows.Forms.DataGridView();
            this.RoomNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RoomType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BedType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblAddReservation = new System.Windows.Forms.Label();
            this.pnlAddReservation = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.erpGuestID = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpNumberOfNights = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpNumberOfPersons = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpArrivalDate = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpDepartureDate = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpBookedRooms = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlReservationDetails.SuspendLayout();
            this.grpPersonDetails.SuspendLayout();
            this.grpBookingDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBookedRooms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAvailableRooms)).BeginInit();
            this.pnlAddReservation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpGuestID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpNumberOfNights)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpNumberOfPersons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpArrivalDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpDepartureDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpBookedRooms)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlReservationDetails
            // 
            this.pnlReservationDetails.BackColor = System.Drawing.Color.FloralWhite;
            this.pnlReservationDetails.Controls.Add(this.grpPersonDetails);
            this.pnlReservationDetails.Controls.Add(this.grpBookingDetails);
            this.pnlReservationDetails.Controls.Add(this.btnRemove);
            this.pnlReservationDetails.Controls.Add(this.lblAddReservations);
            this.pnlReservationDetails.Controls.Add(this.btnAdd);
            this.pnlReservationDetails.Controls.Add(this.dgvBookedRooms);
            this.pnlReservationDetails.Controls.Add(this.btnClear);
            this.pnlReservationDetails.Controls.Add(this.btnBook);
            this.pnlReservationDetails.Controls.Add(this.lblBookedRooms);
            this.pnlReservationDetails.Controls.Add(this.lblAvailableRooms);
            this.pnlReservationDetails.Controls.Add(this.dgvAvailableRooms);
            this.pnlReservationDetails.Location = new System.Drawing.Point(0, 123);
            this.pnlReservationDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlReservationDetails.Name = "pnlReservationDetails";
            this.pnlReservationDetails.Size = new System.Drawing.Size(1088, 698);
            this.pnlReservationDetails.TabIndex = 37;
            // 
            // grpPersonDetails
            // 
            this.grpPersonDetails.Controls.Add(this.lblPersonMobileNumber);
            this.grpPersonDetails.Controls.Add(this.lblMoNumber);
            this.grpPersonDetails.Controls.Add(this.lblPersonEmail);
            this.grpPersonDetails.Controls.Add(this.lblPersonNationality);
            this.grpPersonDetails.Controls.Add(this.lblPersonGender);
            this.grpPersonDetails.Controls.Add(this.lblPersonDateOfBirth);
            this.grpPersonDetails.Controls.Add(this.lblPersonSurname);
            this.grpPersonDetails.Controls.Add(this.lblSurname);
            this.grpPersonDetails.Controls.Add(this.lblPersonName);
            this.grpPersonDetails.Controls.Add(this.lblName);
            this.grpPersonDetails.Controls.Add(this.lblMobileNumber);
            this.grpPersonDetails.Controls.Add(this.txtMobileNumber);
            this.grpPersonDetails.Controls.Add(this.lblEmail);
            this.grpPersonDetails.Controls.Add(this.lblDateOfBirth);
            this.grpPersonDetails.Controls.Add(this.lblGender);
            this.grpPersonDetails.Controls.Add(this.lblNationality);
            this.grpPersonDetails.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpPersonDetails.Location = new System.Drawing.Point(593, 60);
            this.grpPersonDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpPersonDetails.Name = "grpPersonDetails";
            this.grpPersonDetails.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpPersonDetails.Size = new System.Drawing.Size(451, 300);
            this.grpPersonDetails.TabIndex = 65;
            this.grpPersonDetails.TabStop = false;
            this.grpPersonDetails.Text = "Person Details";
            // 
            // lblPersonMobileNumber
            // 
            this.lblPersonMobileNumber.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonMobileNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonMobileNumber.Location = new System.Drawing.Point(168, 261);
            this.lblPersonMobileNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonMobileNumber.Name = "lblPersonMobileNumber";
            this.lblPersonMobileNumber.Size = new System.Drawing.Size(275, 20);
            this.lblPersonMobileNumber.TabIndex = 73;
            // 
            // lblMoNumber
            // 
            this.lblMoNumber.AutoSize = true;
            this.lblMoNumber.BackColor = System.Drawing.Color.FloralWhite;
            this.lblMoNumber.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMoNumber.Location = new System.Drawing.Point(8, 261);
            this.lblMoNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMoNumber.Name = "lblMoNumber";
            this.lblMoNumber.Size = new System.Drawing.Size(145, 24);
            this.lblMoNumber.TabIndex = 72;
            this.lblMoNumber.Text = "Mobile Number:";
            // 
            // lblPersonEmail
            // 
            this.lblPersonEmail.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonEmail.Location = new System.Drawing.Point(168, 222);
            this.lblPersonEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonEmail.Name = "lblPersonEmail";
            this.lblPersonEmail.Size = new System.Drawing.Size(275, 20);
            this.lblPersonEmail.TabIndex = 71;
            // 
            // lblPersonNationality
            // 
            this.lblPersonNationality.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonNationality.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonNationality.Location = new System.Drawing.Point(168, 183);
            this.lblPersonNationality.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonNationality.Name = "lblPersonNationality";
            this.lblPersonNationality.Size = new System.Drawing.Size(275, 20);
            this.lblPersonNationality.TabIndex = 70;
            // 
            // lblPersonGender
            // 
            this.lblPersonGender.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonGender.Location = new System.Drawing.Point(168, 148);
            this.lblPersonGender.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonGender.Name = "lblPersonGender";
            this.lblPersonGender.Size = new System.Drawing.Size(275, 20);
            this.lblPersonGender.TabIndex = 69;
            // 
            // lblPersonDateOfBirth
            // 
            this.lblPersonDateOfBirth.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonDateOfBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonDateOfBirth.Location = new System.Drawing.Point(168, 113);
            this.lblPersonDateOfBirth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonDateOfBirth.Name = "lblPersonDateOfBirth";
            this.lblPersonDateOfBirth.Size = new System.Drawing.Size(275, 20);
            this.lblPersonDateOfBirth.TabIndex = 68;
            // 
            // lblPersonSurname
            // 
            this.lblPersonSurname.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonSurname.Location = new System.Drawing.Point(168, 79);
            this.lblPersonSurname.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonSurname.Name = "lblPersonSurname";
            this.lblPersonSurname.Size = new System.Drawing.Size(275, 20);
            this.lblPersonSurname.TabIndex = 67;
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.BackColor = System.Drawing.Color.FloralWhite;
            this.lblSurname.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSurname.Location = new System.Drawing.Point(8, 79);
            this.lblSurname.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(90, 24);
            this.lblSurname.TabIndex = 2;
            this.lblSurname.Text = "Surname:";
            // 
            // lblPersonName
            // 
            this.lblPersonName.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonName.Location = new System.Drawing.Point(168, 39);
            this.lblPersonName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonName.Name = "lblPersonName";
            this.lblPersonName.Size = new System.Drawing.Size(275, 20);
            this.lblPersonName.TabIndex = 66;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(8, 39);
            this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(64, 24);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Name:";
            // 
            // lblMobileNumber
            // 
            this.lblMobileNumber.AutoSize = true;
            this.lblMobileNumber.BackColor = System.Drawing.Color.FloralWhite;
            this.lblMobileNumber.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMobileNumber.Location = new System.Drawing.Point(8, 345);
            this.lblMobileNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMobileNumber.Name = "lblMobileNumber";
            this.lblMobileNumber.Size = new System.Drawing.Size(145, 24);
            this.lblMobileNumber.TabIndex = 7;
            this.lblMobileNumber.Text = "Mobile Number:";
            // 
            // txtMobileNumber
            // 
            this.txtMobileNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtMobileNumber.Location = new System.Drawing.Point(184, 343);
            this.txtMobileNumber.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtMobileNumber.Name = "txtMobileNumber";
            this.txtMobileNumber.Size = new System.Drawing.Size(165, 23);
            this.txtMobileNumber.TabIndex = 12;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.BackColor = System.Drawing.Color.FloralWhite;
            this.lblEmail.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(8, 222);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(60, 24);
            this.lblEmail.TabIndex = 6;
            this.lblEmail.Text = "Email:";
            // 
            // lblDateOfBirth
            // 
            this.lblDateOfBirth.AutoSize = true;
            this.lblDateOfBirth.BackColor = System.Drawing.Color.FloralWhite;
            this.lblDateOfBirth.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateOfBirth.Location = new System.Drawing.Point(8, 113);
            this.lblDateOfBirth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateOfBirth.Name = "lblDateOfBirth";
            this.lblDateOfBirth.Size = new System.Drawing.Size(122, 24);
            this.lblDateOfBirth.TabIndex = 3;
            this.lblDateOfBirth.Text = "Date of Birth:";
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.BackColor = System.Drawing.Color.FloralWhite;
            this.lblGender.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGender.Location = new System.Drawing.Point(8, 148);
            this.lblGender.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(76, 24);
            this.lblGender.TabIndex = 4;
            this.lblGender.Text = "Gender:";
            // 
            // lblNationality
            // 
            this.lblNationality.AutoSize = true;
            this.lblNationality.BackColor = System.Drawing.Color.FloralWhite;
            this.lblNationality.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNationality.Location = new System.Drawing.Point(8, 183);
            this.lblNationality.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNationality.Name = "lblNationality";
            this.lblNationality.Size = new System.Drawing.Size(106, 24);
            this.lblNationality.TabIndex = 5;
            this.lblNationality.Text = "Nationality:";
            // 
            // grpBookingDetails
            // 
            this.grpBookingDetails.Controls.Add(this.lblNumberOfNightsValue);
            this.grpBookingDetails.Controls.Add(this.lblGuestID);
            this.grpBookingDetails.Controls.Add(this.cmbGuestID);
            this.grpBookingDetails.Controls.Add(this.lblNumberOfNights);
            this.grpBookingDetails.Controls.Add(this.lblNumberOfPersons);
            this.grpBookingDetails.Controls.Add(this.txtNumberOfPersons);
            this.grpBookingDetails.Controls.Add(this.dtpDepartureDate);
            this.grpBookingDetails.Controls.Add(this.lblArrivalDate);
            this.grpBookingDetails.Controls.Add(this.dtpArrivalDate);
            this.grpBookingDetails.Controls.Add(this.lblDepartureDate);
            this.grpBookingDetails.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBookingDetails.Location = new System.Drawing.Point(49, 60);
            this.grpBookingDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpBookingDetails.Name = "grpBookingDetails";
            this.grpBookingDetails.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpBookingDetails.Size = new System.Drawing.Size(453, 300);
            this.grpBookingDetails.TabIndex = 64;
            this.grpBookingDetails.TabStop = false;
            this.grpBookingDetails.Text = "Booking Details";
            // 
            // lblNumberOfNightsValue
            // 
            this.lblNumberOfNightsValue.AutoSize = true;
            this.lblNumberOfNightsValue.BackColor = System.Drawing.Color.Transparent;
            this.lblNumberOfNightsValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumberOfNightsValue.Location = new System.Drawing.Point(215, 261);
            this.lblNumberOfNightsValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumberOfNightsValue.Name = "lblNumberOfNightsValue";
            this.lblNumberOfNightsValue.Size = new System.Drawing.Size(0, 20);
            this.lblNumberOfNightsValue.TabIndex = 74;
            // 
            // lblGuestID
            // 
            this.lblGuestID.AutoSize = true;
            this.lblGuestID.BackColor = System.Drawing.Color.Transparent;
            this.lblGuestID.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGuestID.Location = new System.Drawing.Point(8, 39);
            this.lblGuestID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGuestID.Name = "lblGuestID";
            this.lblGuestID.Size = new System.Drawing.Size(86, 24);
            this.lblGuestID.TabIndex = 26;
            this.lblGuestID.Text = "Guest ID:";
            // 
            // cmbGuestID
            // 
            this.cmbGuestID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGuestID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGuestID.FormattingEnabled = true;
            this.cmbGuestID.Location = new System.Drawing.Point(220, 39);
            this.cmbGuestID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbGuestID.Name = "cmbGuestID";
            this.cmbGuestID.Size = new System.Drawing.Size(165, 25);
            this.cmbGuestID.TabIndex = 63;
            this.cmbGuestID.DropDownClosed += new System.EventHandler(this.cmbGuestID_DropDownClosed);
            // 
            // lblNumberOfNights
            // 
            this.lblNumberOfNights.AutoSize = true;
            this.lblNumberOfNights.BackColor = System.Drawing.Color.Transparent;
            this.lblNumberOfNights.Font = new System.Drawing.Font("Candara", 12F);
            this.lblNumberOfNights.Location = new System.Drawing.Point(8, 261);
            this.lblNumberOfNights.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumberOfNights.Name = "lblNumberOfNights";
            this.lblNumberOfNights.Size = new System.Drawing.Size(166, 24);
            this.lblNumberOfNights.TabIndex = 30;
            this.lblNumberOfNights.Text = "Number Of Nights:";
            // 
            // lblNumberOfPersons
            // 
            this.lblNumberOfPersons.AutoSize = true;
            this.lblNumberOfPersons.BackColor = System.Drawing.Color.Transparent;
            this.lblNumberOfPersons.Font = new System.Drawing.Font("Candara", 12F);
            this.lblNumberOfPersons.Location = new System.Drawing.Point(8, 94);
            this.lblNumberOfPersons.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumberOfPersons.Name = "lblNumberOfPersons";
            this.lblNumberOfPersons.Size = new System.Drawing.Size(175, 24);
            this.lblNumberOfPersons.TabIndex = 29;
            this.lblNumberOfPersons.Text = "Number of Persons:";
            // 
            // txtNumberOfPersons
            // 
            this.txtNumberOfPersons.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtNumberOfPersons.Location = new System.Drawing.Point(220, 94);
            this.txtNumberOfPersons.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNumberOfPersons.Name = "txtNumberOfPersons";
            this.txtNumberOfPersons.Size = new System.Drawing.Size(165, 23);
            this.txtNumberOfPersons.TabIndex = 40;
            // 
            // dtpDepartureDate
            // 
            this.dtpDepartureDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDepartureDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDepartureDate.Location = new System.Drawing.Point(220, 204);
            this.dtpDepartureDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpDepartureDate.Name = "dtpDepartureDate";
            this.dtpDepartureDate.Size = new System.Drawing.Size(165, 23);
            this.dtpDepartureDate.TabIndex = 42;
            this.dtpDepartureDate.Value = new System.DateTime(2019, 5, 13, 0, 0, 0, 0);
            this.dtpDepartureDate.ValueChanged += new System.EventHandler(this.dtpDepartureDate_ValueChanged);
            // 
            // lblArrivalDate
            // 
            this.lblArrivalDate.AutoSize = true;
            this.lblArrivalDate.BackColor = System.Drawing.Color.Transparent;
            this.lblArrivalDate.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArrivalDate.Location = new System.Drawing.Point(8, 149);
            this.lblArrivalDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblArrivalDate.Name = "lblArrivalDate";
            this.lblArrivalDate.Size = new System.Drawing.Size(114, 24);
            this.lblArrivalDate.TabIndex = 27;
            this.lblArrivalDate.Text = "Arrival Date:";
            // 
            // dtpArrivalDate
            // 
            this.dtpArrivalDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpArrivalDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpArrivalDate.Location = new System.Drawing.Point(220, 149);
            this.dtpArrivalDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpArrivalDate.Name = "dtpArrivalDate";
            this.dtpArrivalDate.Size = new System.Drawing.Size(165, 23);
            this.dtpArrivalDate.TabIndex = 41;
            this.dtpArrivalDate.Value = new System.DateTime(2019, 5, 13, 0, 0, 0, 0);
            this.dtpArrivalDate.ValueChanged += new System.EventHandler(this.dtpArrivalDate_ValueChanged);
            // 
            // lblDepartureDate
            // 
            this.lblDepartureDate.AutoSize = true;
            this.lblDepartureDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDepartureDate.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDepartureDate.Location = new System.Drawing.Point(8, 204);
            this.lblDepartureDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDepartureDate.Name = "lblDepartureDate";
            this.lblDepartureDate.Size = new System.Drawing.Size(145, 24);
            this.lblDepartureDate.TabIndex = 28;
            this.lblDepartureDate.Text = "Departure Date:";
            // 
            // btnRemove
            // 
            this.btnRemove.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.ForeColor = System.Drawing.Color.White;
            this.btnRemove.Location = new System.Drawing.Point(521, 510);
            this.btnRemove.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(53, 49);
            this.btnRemove.TabIndex = 61;
            this.btnRemove.Text = "<";
            this.btnRemove.UseVisualStyleBackColor = false;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            this.btnRemove.MouseLeave += new System.EventHandler(this.btnRemove_MouseLeave);
            this.btnRemove.MouseHover += new System.EventHandler(this.btnRemove_MouseHover);
            // 
            // lblAddReservations
            // 
            this.lblAddReservations.AutoSize = true;
            this.lblAddReservations.BackColor = System.Drawing.Color.Transparent;
            this.lblAddReservations.Font = new System.Drawing.Font("Candara", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddReservations.Location = new System.Drawing.Point(405, 16);
            this.lblAddReservations.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddReservations.Name = "lblAddReservations";
            this.lblAddReservations.Size = new System.Drawing.Size(327, 37);
            this.lblAddReservations.TabIndex = 56;
            this.lblAddReservations.Text = "Add Reservation Details";
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(521, 458);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(53, 49);
            this.btnAdd.TabIndex = 60;
            this.btnAdd.Text = ">";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            this.btnAdd.MouseLeave += new System.EventHandler(this.btnAdd_MouseLeave);
            this.btnAdd.MouseHover += new System.EventHandler(this.btnAdd_MouseHover);
            // 
            // dgvBookedRooms
            // 
            this.dgvBookedRooms.AllowUserToAddRows = false;
            this.dgvBookedRooms.BackgroundColor = System.Drawing.SystemColors.WindowFrame;
            this.dgvBookedRooms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBookedRooms.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            this.dgvBookedRooms.Location = new System.Drawing.Point(591, 427);
            this.dgvBookedRooms.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvBookedRooms.Name = "dgvBookedRooms";
            this.dgvBookedRooms.RowHeadersWidth = 5;
            this.dgvBookedRooms.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBookedRooms.Size = new System.Drawing.Size(453, 167);
            this.dgvBookedRooms.TabIndex = 55;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Room Number";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 125;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Room Type";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 125;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Bed Type";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 125;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Price";
            this.dataGridViewTextBoxColumn8.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 125;
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnClear.Font = new System.Drawing.Font("Candara", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(892, 628);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(155, 44);
            this.btnClear.TabIndex = 54;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            this.btnClear.MouseLeave += new System.EventHandler(this.btnClear_MouseLeave);
            this.btnClear.MouseHover += new System.EventHandler(this.btnClear_MouseHover);
            // 
            // btnBook
            // 
            this.btnBook.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnBook.Font = new System.Drawing.Font("Candara", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBook.ForeColor = System.Drawing.Color.White;
            this.btnBook.Location = new System.Drawing.Point(724, 628);
            this.btnBook.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBook.Name = "btnBook";
            this.btnBook.Size = new System.Drawing.Size(155, 44);
            this.btnBook.TabIndex = 52;
            this.btnBook.Text = "Book";
            this.btnBook.UseVisualStyleBackColor = false;
            this.btnBook.Click += new System.EventHandler(this.btnBook_Click);
            this.btnBook.MouseLeave += new System.EventHandler(this.btnBook_MouseLeave);
            this.btnBook.MouseHover += new System.EventHandler(this.btnBook_MouseHover);
            // 
            // lblBookedRooms
            // 
            this.lblBookedRooms.AutoSize = true;
            this.lblBookedRooms.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBookedRooms.Location = new System.Drawing.Point(756, 385);
            this.lblBookedRooms.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBookedRooms.Name = "lblBookedRooms";
            this.lblBookedRooms.Size = new System.Drawing.Size(138, 24);
            this.lblBookedRooms.TabIndex = 38;
            this.lblBookedRooms.Text = "Booked Rooms";
            // 
            // lblAvailableRooms
            // 
            this.lblAvailableRooms.AutoSize = true;
            this.lblAvailableRooms.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAvailableRooms.Location = new System.Drawing.Point(209, 385);
            this.lblAvailableRooms.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAvailableRooms.Name = "lblAvailableRooms";
            this.lblAvailableRooms.Size = new System.Drawing.Size(152, 24);
            this.lblAvailableRooms.TabIndex = 37;
            this.lblAvailableRooms.Text = "Available Rooms";
            // 
            // dgvAvailableRooms
            // 
            this.dgvAvailableRooms.AllowUserToAddRows = false;
            this.dgvAvailableRooms.BackgroundColor = System.Drawing.SystemColors.WindowFrame;
            this.dgvAvailableRooms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAvailableRooms.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RoomNumber,
            this.RoomType,
            this.BedType,
            this.Price});
            this.dgvAvailableRooms.Location = new System.Drawing.Point(49, 427);
            this.dgvAvailableRooms.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvAvailableRooms.Name = "dgvAvailableRooms";
            this.dgvAvailableRooms.RowHeadersWidth = 5;
            this.dgvAvailableRooms.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAvailableRooms.Size = new System.Drawing.Size(453, 167);
            this.dgvAvailableRooms.TabIndex = 36;
            // 
            // RoomNumber
            // 
            this.RoomNumber.HeaderText = "Room Number";
            this.RoomNumber.MinimumWidth = 6;
            this.RoomNumber.Name = "RoomNumber";
            this.RoomNumber.Width = 125;
            // 
            // RoomType
            // 
            this.RoomType.HeaderText = "Room Type";
            this.RoomType.MinimumWidth = 6;
            this.RoomType.Name = "RoomType";
            this.RoomType.Width = 125;
            // 
            // BedType
            // 
            this.BedType.HeaderText = "Bed Type";
            this.BedType.MinimumWidth = 6;
            this.BedType.Name = "BedType";
            this.BedType.Width = 125;
            // 
            // Price
            // 
            this.Price.HeaderText = "Price";
            this.Price.MinimumWidth = 6;
            this.Price.Name = "Price";
            this.Price.Width = 125;
            // 
            // lblAddReservation
            // 
            this.lblAddReservation.AutoSize = true;
            this.lblAddReservation.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddReservation.ForeColor = System.Drawing.Color.White;
            this.lblAddReservation.Location = new System.Drawing.Point(737, 45);
            this.lblAddReservation.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddReservation.Name = "lblAddReservation";
            this.lblAddReservation.Size = new System.Drawing.Size(310, 42);
            this.lblAddReservation.TabIndex = 3;
            this.lblAddReservation.Text = "Add Reservation";
            // 
            // pnlAddReservation
            // 
            this.pnlAddReservation.BackColor = System.Drawing.Color.DarkTurquoise;
            this.pnlAddReservation.Controls.Add(this.pictureBox1);
            this.pnlAddReservation.Controls.Add(this.lblAddReservation);
            this.pnlAddReservation.Location = new System.Drawing.Point(0, 0);
            this.pnlAddReservation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlAddReservation.Name = "pnlAddReservation";
            this.pnlAddReservation.Size = new System.Drawing.Size(1088, 123);
            this.pnlAddReservation.TabIndex = 36;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(16, 15);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(295, 97);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 44;
            this.pictureBox1.TabStop = false;
            // 
            // erpGuestID
            // 
            this.erpGuestID.ContainerControl = this;
            // 
            // erpNumberOfNights
            // 
            this.erpNumberOfNights.ContainerControl = this;
            // 
            // erpNumberOfPersons
            // 
            this.erpNumberOfPersons.ContainerControl = this;
            // 
            // erpArrivalDate
            // 
            this.erpArrivalDate.ContainerControl = this;
            // 
            // erpDepartureDate
            // 
            this.erpDepartureDate.ContainerControl = this;
            // 
            // erpBookedRooms
            // 
            this.erpBookedRooms.ContainerControl = this;
            // 
            // AddReservation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1089, 821);
            this.Controls.Add(this.pnlReservationDetails);
            this.Controls.Add(this.pnlAddReservation);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "AddReservation";
            this.Text = "Add Reservations";
            this.pnlReservationDetails.ResumeLayout(false);
            this.pnlReservationDetails.PerformLayout();
            this.grpPersonDetails.ResumeLayout(false);
            this.grpPersonDetails.PerformLayout();
            this.grpBookingDetails.ResumeLayout(false);
            this.grpBookingDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBookedRooms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAvailableRooms)).EndInit();
            this.pnlAddReservation.ResumeLayout(false);
            this.pnlAddReservation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpGuestID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpNumberOfNights)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpNumberOfPersons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpArrivalDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpDepartureDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpBookedRooms)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlReservationDetails;
        private System.Windows.Forms.Label lblAddReservation;
        private System.Windows.Forms.Panel pnlAddReservation;
        private System.Windows.Forms.Label lblBookedRooms;
        private System.Windows.Forms.Label lblAvailableRooms;
        private System.Windows.Forms.DataGridView dgvAvailableRooms;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnBook;
        private System.Windows.Forms.TextBox txtNumberOfPersons;
        private System.Windows.Forms.Label lblGuestID;
        private System.Windows.Forms.DateTimePicker dtpDepartureDate;
        private System.Windows.Forms.Label lblDepartureDate;
        private System.Windows.Forms.Label lblNumberOfNights;
        private System.Windows.Forms.DateTimePicker dtpArrivalDate;
        private System.Windows.Forms.Label lblNumberOfPersons;
        private System.Windows.Forms.Label lblArrivalDate;
        private System.Windows.Forms.DataGridView dgvBookedRooms;
        private System.Windows.Forms.Label lblAddReservations;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ComboBox cmbGuestID;
        private System.Windows.Forms.GroupBox grpBookingDetails;
        private System.Windows.Forms.ErrorProvider erpGuestID;
        private System.Windows.Forms.ErrorProvider erpNumberOfNights;
        private System.Windows.Forms.ErrorProvider erpNumberOfPersons;
        private System.Windows.Forms.ErrorProvider erpArrivalDate;
        private System.Windows.Forms.ErrorProvider erpDepartureDate;
        private System.Windows.Forms.Label lblNumberOfNightsValue;
        private System.Windows.Forms.ErrorProvider erpBookedRooms;
        private System.Windows.Forms.GroupBox grpPersonDetails;
        private System.Windows.Forms.Label lblPersonMobileNumber;
        private System.Windows.Forms.Label lblMoNumber;
        private System.Windows.Forms.Label lblPersonEmail;
        private System.Windows.Forms.Label lblPersonNationality;
        private System.Windows.Forms.Label lblPersonGender;
        private System.Windows.Forms.Label lblPersonDateOfBirth;
        private System.Windows.Forms.Label lblPersonSurname;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.Label lblPersonName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblMobileNumber;
        private System.Windows.Forms.TextBox txtMobileNumber;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblDateOfBirth;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label lblNationality;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoomNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoomType;
        private System.Windows.Forms.DataGridViewTextBoxColumn BedType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}