﻿namespace HotelReservationSystem.ReservationFolder
{
    partial class ViewReservation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewReservation));
            this.pnlReservationDetails = new System.Windows.Forms.Panel();
            this.dgvViewReservations = new System.Windows.Forms.DataGridView();
            this.BookingNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GuestID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumberOfPersons = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumberOfNights = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ArrivalDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DepartureDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RoomNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblReservedRooms = new System.Windows.Forms.Label();
            this.lblViewReservation = new System.Windows.Forms.Label();
            this.pnlViewReservations = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlReservationDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvViewReservations)).BeginInit();
            this.pnlViewReservations.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlReservationDetails
            // 
            this.pnlReservationDetails.BackColor = System.Drawing.Color.FloralWhite;
            this.pnlReservationDetails.Controls.Add(this.dgvViewReservations);
            this.pnlReservationDetails.Controls.Add(this.lblReservedRooms);
            this.pnlReservationDetails.Location = new System.Drawing.Point(0, 123);
            this.pnlReservationDetails.Margin = new System.Windows.Forms.Padding(4);
            this.pnlReservationDetails.Name = "pnlReservationDetails";
            this.pnlReservationDetails.Size = new System.Drawing.Size(1111, 703);
            this.pnlReservationDetails.TabIndex = 39;
            // 
            // dgvViewReservations
            // 
            this.dgvViewReservations.AllowUserToAddRows = false;
            this.dgvViewReservations.BackgroundColor = System.Drawing.SystemColors.WindowFrame;
            this.dgvViewReservations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvViewReservations.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BookingNumber,
            this.GuestID,
            this.NumberOfPersons,
            this.NumberOfNights,
            this.ArrivalDate,
            this.DepartureDate,
            this.RoomNumber});
            this.dgvViewReservations.Location = new System.Drawing.Point(24, 96);
            this.dgvViewReservations.Margin = new System.Windows.Forms.Padding(4);
            this.dgvViewReservations.Name = "dgvViewReservations";
            this.dgvViewReservations.RowHeadersWidth = 5;
            this.dgvViewReservations.Size = new System.Drawing.Size(1057, 570);
            this.dgvViewReservations.TabIndex = 57;
            this.dgvViewReservations.SelectionChanged += new System.EventHandler(this.dgvViewReservations_SelectionChanged);
            // 
            // BookingNumber
            // 
            this.BookingNumber.HeaderText = "Booking Number";
            this.BookingNumber.MinimumWidth = 6;
            this.BookingNumber.Name = "BookingNumber";
            this.BookingNumber.Width = 110;
            // 
            // GuestID
            // 
            this.GuestID.HeaderText = "Guest ID";
            this.GuestID.MinimumWidth = 6;
            this.GuestID.Name = "GuestID";
            this.GuestID.Width = 75;
            // 
            // NumberOfPersons
            // 
            this.NumberOfPersons.HeaderText = "Number Of Persons";
            this.NumberOfPersons.MinimumWidth = 6;
            this.NumberOfPersons.Name = "NumberOfPersons";
            this.NumberOfPersons.Width = 125;
            // 
            // NumberOfNights
            // 
            this.NumberOfNights.HeaderText = "Number Of Nights";
            this.NumberOfNights.MinimumWidth = 6;
            this.NumberOfNights.Name = "NumberOfNights";
            this.NumberOfNights.Width = 125;
            // 
            // ArrivalDate
            // 
            this.ArrivalDate.HeaderText = "Arrival Date";
            this.ArrivalDate.MinimumWidth = 6;
            this.ArrivalDate.Name = "ArrivalDate";
            this.ArrivalDate.Width = 125;
            // 
            // DepartureDate
            // 
            this.DepartureDate.DividerWidth = 2;
            this.DepartureDate.HeaderText = "Departure Date";
            this.DepartureDate.MinimumWidth = 6;
            this.DepartureDate.Name = "DepartureDate";
            this.DepartureDate.Width = 125;
            // 
            // RoomNumber
            // 
            this.RoomNumber.HeaderText = "Room Number";
            this.RoomNumber.MinimumWidth = 6;
            this.RoomNumber.Name = "RoomNumber";
            this.RoomNumber.Width = 125;
            // 
            // lblReservedRooms
            // 
            this.lblReservedRooms.AutoSize = true;
            this.lblReservedRooms.BackColor = System.Drawing.Color.Transparent;
            this.lblReservedRooms.Font = new System.Drawing.Font("Candara", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReservedRooms.Location = new System.Drawing.Point(424, 30);
            this.lblReservedRooms.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblReservedRooms.Name = "lblReservedRooms";
            this.lblReservedRooms.Size = new System.Drawing.Size(304, 37);
            this.lblReservedRooms.TabIndex = 56;
            this.lblReservedRooms.Text = "View Reserved Rooms";
            // 
            // lblViewReservation
            // 
            this.lblViewReservation.AutoSize = true;
            this.lblViewReservation.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblViewReservation.ForeColor = System.Drawing.Color.White;
            this.lblViewReservation.Location = new System.Drawing.Point(756, 43);
            this.lblViewReservation.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblViewReservation.Name = "lblViewReservation";
            this.lblViewReservation.Size = new System.Drawing.Size(325, 42);
            this.lblViewReservation.TabIndex = 3;
            this.lblViewReservation.Text = "View Reservation";
            // 
            // pnlViewReservations
            // 
            this.pnlViewReservations.BackColor = System.Drawing.Color.DarkTurquoise;
            this.pnlViewReservations.Controls.Add(this.pictureBox1);
            this.pnlViewReservations.Controls.Add(this.lblViewReservation);
            this.pnlViewReservations.Location = new System.Drawing.Point(0, 0);
            this.pnlViewReservations.Margin = new System.Windows.Forms.Padding(4);
            this.pnlViewReservations.Name = "pnlViewReservations";
            this.pnlViewReservations.Size = new System.Drawing.Size(1111, 123);
            this.pnlViewReservations.TabIndex = 38;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(16, 15);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(295, 97);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 45;
            this.pictureBox1.TabStop = false;
            // 
            // ViewReservation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 821);
            this.Controls.Add(this.pnlReservationDetails);
            this.Controls.Add(this.pnlViewReservations);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ViewReservation";
            this.Text = "s";
            this.pnlReservationDetails.ResumeLayout(false);
            this.pnlReservationDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvViewReservations)).EndInit();
            this.pnlViewReservations.ResumeLayout(false);
            this.pnlViewReservations.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlReservationDetails;
        private System.Windows.Forms.Label lblReservedRooms;
        private System.Windows.Forms.Label lblViewReservation;
        private System.Windows.Forms.Panel pnlViewReservations;
        private System.Windows.Forms.DataGridView dgvViewReservations;
        private System.Windows.Forms.DataGridViewTextBoxColumn BookingNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn GuestID;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumberOfPersons;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumberOfNights;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArrivalDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn DepartureDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoomNumber;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}