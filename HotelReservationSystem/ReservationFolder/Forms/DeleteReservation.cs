﻿using HotelReservationSystem.DataLayer;
using HotelReservationSystem.Other;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelReservationSystem.ReservationFolder
{
    public partial class DeleteReservation : Form
    {
        public DeleteReservation()
        {
            InitializeComponent();
            addDataToTable();
        }

        //Add data from database to the table in form. 
        public void addDataToTable()
        {
            BookingRoomManager brm = new BookingRoomManager();

            List <ReservationView> rvList = brm.GetView();

            foreach(ReservationView rv in rvList)
            {
                dgvReservations.Rows.Add(rv.bookingNo, rv.guestId, rv.noOfPersons, rv.noOfNights, rv.arrival, rv.departure, rv.rooms);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvReservations.SelectedCells.Count > 0)
            {
                BookingRoomManager brm = new BookingRoomManager();

                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result = MessageBox.Show("Are you sure you want to delete this record?", "Delete Record", buttons, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    int bookingNo = int.Parse(dgvReservations.SelectedCells[0].Value.ToString());
                    dgvReservations.Rows.RemoveAt(dgvReservations.SelectedCells[0].RowIndex);
                    brm.Delete(bookingNo);
                }
            }
        }

        private void btnDelete_MouseHover(object sender, EventArgs e)
        {
            btnDelete.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnDelete.ForeColor = Color.Black;
        }

        private void btnDelete_MouseLeave(object sender, EventArgs e)
        {
            btnDelete.BackColor = Color.Black;
            btnDelete.ForeColor = Color.White;
        }
    }
}
