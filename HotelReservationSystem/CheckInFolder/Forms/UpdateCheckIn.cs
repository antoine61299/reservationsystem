﻿using HotelReservationSystem.DataLayer;
using HotelReservationSystem.Other;
using HotelReservationSystem.ReservationFolder;
using HotelReservationSystem.ReservationFolder.Objects;
using HotelReservationSystem.RoomFolder;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelReservationSystem.CheckInFolder
{
    public partial class UpdateCheckIn : Form
    {
        public UpdateCheckIn()
        {
            InitializeComponent();
            disableElements();
            populateBookingNumberComboBox();

            btnUpdate.Visible = false;
            btnClear.Visible = false;

            if (cmbBookingNumber.Items.Count == 0)
            {
                disableElements();
            }
        }

        public void clearData()
        {
            lblGuestIDValue.Text = "";
            txtNumberOfPersons.Text = "";
            grpPersonDetails.Text = "";
            dtpArrivalDate.Value = DateTime.Now;
            dtpDepartureDate.Value = DateTime.Now;
            lblNumberOfNightsValue.Text = "";
            dgvAvailableRooms.Rows.Clear();
            dgvBookedRooms.Rows.Clear();
        }

        public void enableElements()
        {
            lblGuestIDValue.Enabled = true;
            lblNumberOfNightsValue.Enabled = true;
            txtNumberOfPersons.Enabled = true;
            dtpArrivalDate.Enabled = true;
            dtpDepartureDate.Enabled = true;
            grpPersonDetails.Enabled = true;
            grpPersonDetails.Enabled = true;
            lblAvailableRooms.Enabled = true;
            lblBookedRooms.Enabled = true;
            dgvAvailableRooms.Enabled = true;
            dgvBookedRooms.Enabled = true;
            btnAdd.Visible = true;
            btnRemove.Visible = true;
            btnUpdate.Visible = true;
            btnClear.Visible = true;
        }

        public void disableElements()
        {
            lblGuestIDValue.Enabled = false;
            lblNumberOfNightsValue.Enabled = false;
            txtNumberOfPersons.Enabled = false;
            dtpArrivalDate.Enabled = false;
            dtpDepartureDate.Enabled = false;
            grpPersonDetails.Enabled = false;
            grpPersonDetails.Enabled = false;
            lblAvailableRooms.Enabled = false;
            lblBookedRooms.Enabled = false;
            dgvAvailableRooms.Enabled = false;
            dgvBookedRooms.Enabled = false;
            btnAdd.Visible = false;
            btnRemove.Visible = false;
            btnUpdate.Visible = false;
            btnClear.Visible = false;
        }

        //Populate combobox with all room id's so that the one to be updated is selected.
        public void populateBookingNumberComboBox()
        {
            BookingManager bm = new BookingManager();
            List<int> bnList = bm.GetCheckedInBookingNo();

            foreach(int bn in bnList)
            {
                cmbBookingNumber.Items.Add(bn);
            }
        }

        public void setElementsInForm()
        {
            enableElements();
            dgvBookedRooms.Rows.Clear();

            BookingManager bm = new BookingManager();

            List<ReservationView> rvList = bm.GetView(int.Parse(cmbBookingNumber.SelectedItem.ToString()));

            foreach (ReservationView rv in rvList)
            {
                lblGuestIDValue.Text = rv.guestId;
                txtNumberOfPersons.Text = rv.noOfPersons;
                dtpDepartureDate.Text = rv.departure;
                dtpArrivalDate.Text = rv.arrival;
                lblNumberOfNightsValue.Text = rv.noOfNights;
                lblPersonName.Text = rv.personName;
                lblPersonSurname.Text = rv.personSurname;
                lblPersonDateOfBirth.Text = rv.personDob;
                lblPersonGender.Text = rv.personGender;
                lblPersonNationality.Text = rv.nationality;
                lblPersonEmail.Text = rv.email;
                lblPersonMobileNumber.Text = rv.mobile;
                dgvBookedRooms.Rows.Add(rv.roomNumber, rv.roomType, rv.bedType, rv.price);
            }
        }

        public void appendAvailableRooms()
        {
            RoomManager rm = new RoomManager();

            dgvAvailableRooms.Rows.Clear();
            List<int> bookedRooms = getBookedRooms(DateTime.Parse(dtpArrivalDate.Text), DateTime.Parse(dtpDepartureDate.Text));

            List<Room> roomList = rm.GetAll();

            List<int> roomsInBookingTableForm = new List<int>();

            foreach (DataGridViewRow row in dgvBookedRooms.Rows)
            {
                roomsInBookingTableForm.Add(int.Parse(row.Cells[0].Value.ToString()));
            }

            foreach (Room r in roomList)
            {
                if (!bookedRooms.Contains(r.roomId) && !roomsInBookingTableForm.Contains(r.roomNumber))
                {
                    dgvAvailableRooms.Rows.Add(r.roomNumber, r.roomType, r.bedType, r.price);
                }
            }
        }

        public List<int> getBookedRooms(DateTime formArrival, DateTime formDeparture)
        {
            List<int> bookedRooms = new List<int>();

            List<BookingRoom> dbBookingRooms = getBookingRoom();

            BookingManager bm = new BookingManager();
            List<Booking> dbBooking = bm.GetAll();

            foreach (Booking b in dbBooking)
            {
                DateTime dbArrival = b.arrivalDate;
                DateTime dbDeparture = b.departureDate;
                string bookingStatus = b.status;

                if (!bookingStatus.Equals("Checked Out"))
                {
                    if (formArrival >= dbArrival && formArrival <= dbDeparture)
                    {
                        foreach (BookingRoom br in dbBookingRooms)
                        {
                            if (br.bookingNo == b.bookingNo)
                            {
                                bookedRooms.Add(br.roomNo);
                            }
                        }
                    }
                    else if (formArrival <= dbArrival && formDeparture >= dbArrival)
                    {
                        foreach (BookingRoom br in dbBookingRooms)
                        {
                            if (br.bookingNo == b.bookingNo)
                            {
                                bookedRooms.Add(br.roomNo);
                            }
                        }
                    }
                }
            }
            return bookedRooms;
        }

        public List<BookingRoom> getBookingRoom()
        {
            BookingRoomManager brm = new BookingRoomManager();
            List<BookingRoom> dbBookingRoom = brm.GetAll();
            return dbBookingRoom;
        }

        public Booking getDataFromForm()
        {
            DateTime arrivalDate = DateTime.Parse(dtpArrivalDate.Text).Date;
            DateTime departureDate = DateTime.Parse(dtpDepartureDate.Text).Date;
            int numberOfNights = int.Parse(lblNumberOfNightsValue.Text);
            int numberOfPersons = int.Parse(txtNumberOfPersons.Text);
            int guestID = int.Parse(cmbBookingNumber.SelectedItem.ToString());
            string status = "Checked In";

            List<int> rooms = new List<int>();
            foreach (DataGridViewRow row in dgvBookedRooms.Rows)
            {
                rooms.Add(int.Parse(row.Cells[0].Value.ToString()));
            }

            Booking booking = new Booking(arrivalDate, departureDate, numberOfNights, numberOfPersons, guestID, status, 0, 0, rooms);
            return booking;
        }

        public void setNumberOfNights()
        {
            int numberOfNights = (int)DateTime.Parse(dtpDepartureDate.Text).Subtract(DateTime.Parse(dtpArrivalDate.Text)).TotalDays;
            lblNumberOfNightsValue.Text = numberOfNights.ToString();
        }

        public bool validateInputs()
        {
            bool validated = true;

            if (cmbBookingNumber.Text == string.Empty)
            {
                erpBookingNumber.SetError(cmbBookingNumber, "Please select the booking number");
                validated = false;
            }
            else
            {
                if (!int.TryParse(cmbBookingNumber.Text, out int numValue))
                {
                    erpBookingNumber.SetError(cmbBookingNumber, "Please select the guest id");
                    validated = false;
                }
                else
                {
                    erpBookingNumber.Clear();
                }
            }

            if (txtNumberOfPersons.Text == string.Empty)
            {
                erpNumberOfPersons.SetError(txtNumberOfPersons, "Please enter the number of persons");
                validated = false;
            }
            else
            {
                if (!int.TryParse(txtNumberOfPersons.Text, out int numValue))
                {
                    erpNumberOfPersons.SetError(txtNumberOfPersons, "Please select the guest id");
                    validated = false;
                }
                else
                {
                    erpNumberOfPersons.Clear();
                }
            }

            if (dtpArrivalDate.Value >= dtpDepartureDate.Value)
            {
                erpArrivalDate.SetError(dtpArrivalDate, "Arrival date must be before departure date");
                erpDepartureDate.SetError(dtpDepartureDate, "Departure date must be after arrival date");
                validated = false;
            }
            else
            {
                erpArrivalDate.Clear();
                erpDepartureDate.Clear();
            }

            if (dgvBookedRooms.Rows == null || dgvBookedRooms.Rows.Count == 0)
            {
                erpDepartureDate.SetError(dgvBookedRooms, "A room must be added");
                validated = false;
            }
            else
            {
                erpDepartureDate.Clear();
            }

            return validated;
        }

        private void cmbBookingNumber_DropDownClosed(object sender, EventArgs e)
        {
            enableElements();
            setElementsInForm();
            appendAvailableRooms();
            if (cmbBookingNumber.SelectedItem != null)
            {
                setElementsInForm();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow selRow in dgvAvailableRooms.SelectedRows.OfType<DataGridViewRow>().ToArray())
            {
                dgvAvailableRooms.Rows.Remove(selRow);
                dgvBookedRooms.Rows.Add(selRow);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow selRow in dgvBookedRooms.SelectedRows.OfType<DataGridViewRow>().ToArray())
            {
                dgvBookedRooms.Rows.Remove(selRow);
                dgvAvailableRooms.Rows.Add(selRow);
            }
        }

        private void dtpArrivalDate_ValueChanged(object sender, EventArgs e)
        {
            setNumberOfNights();
            appendAvailableRooms();
        }

        private void dtpDepartureDate_ValueChanged(object sender, EventArgs e)
        {
            setNumberOfNights();
            appendAvailableRooms();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (validateInputs())
            {
                BookingManager bm = new BookingManager();
                Booking booking = getDataFromForm();
                bm.Update(int.Parse(cmbBookingNumber.SelectedItem.ToString()), booking);
                clearData();
                disableElements();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clearData();
            disableElements();
        }

        private void btnUpdate_MouseHover(object sender, EventArgs e)
        {
            btnUpdate.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnUpdate.ForeColor = Color.Black;
        }

        private void btnUpdate_MouseLeave(object sender, EventArgs e)
        {
            btnUpdate.BackColor = Color.Black;
            btnUpdate.ForeColor = Color.White;
        }

        private void btnClear_MouseHover(object sender, EventArgs e)
        {
            btnClear.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnClear.ForeColor = Color.Black;
        }

        private void btnClear_MouseLeave(object sender, EventArgs e)
        {
            btnClear.BackColor = Color.Black;
            btnClear.ForeColor = Color.White;
        }

        private void btnAdd_MouseHover(object sender, EventArgs e)
        {
            btnAdd.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnAdd.ForeColor = Color.Black;
        }

        private void btnAdd_MouseLeave(object sender, EventArgs e)
        {
            btnAdd.BackColor = Color.Black;
            btnAdd.ForeColor = Color.White;
        }

        private void btnRemove_MouseHover(object sender, EventArgs e)
        {
            btnRemove.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnRemove.ForeColor = Color.Black;
        }

        private void btnRemove_MouseLeave(object sender, EventArgs e)
        {
            btnRemove.BackColor = Color.Black;
            btnRemove.ForeColor = Color.White;
        }
    }
}
