﻿using HotelReservationSystem.DataLayer;
using HotelReservationSystem.Other;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelReservationSystem.CheckInFolder
{
    public partial class ViewCheckIn : Form
    {
        public ViewCheckIn()
        {
            InitializeComponent();
            addDataToTable();
        }

        public void addDataToTable()
        {
            BookingRoomManager brm = new BookingRoomManager();
            List<ReservationView> rvList = brm.GetViewCheckedIn();

            foreach(ReservationView rv in rvList)
            {
                dgvCheckedInRooms.Rows.Add(rv.bookingNo, rv.guestId, rv.noOfPersons, rv.noOfNights, rv.arrival, rv.departure, rv.rooms);
            }
        }

        private void dgvCheckedInRooms_SelectionChanged(object sender, EventArgs e)
        {
            dgvCheckedInRooms.ClearSelection();
        }
    }
}
