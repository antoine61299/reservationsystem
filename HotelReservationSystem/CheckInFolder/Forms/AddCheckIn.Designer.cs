﻿namespace HotelReservationSystem.CheckInFolder
{
    partial class AddCheckIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddCheckIn));
            this.dgvBookedRooms = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblAddReservation = new System.Windows.Forms.Label();
            this.lblGuestID = new System.Windows.Forms.Label();
            this.btnCheckIn = new System.Windows.Forms.Button();
            this.lblDepartureDate = new System.Windows.Forms.Label();
            this.lblBookedRooms = new System.Windows.Forms.Label();
            this.pnlAddCheckIn = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblNumberOfPersons = new System.Windows.Forms.Label();
            this.lblArrivalDate = new System.Windows.Forms.Label();
            this.lblNumberOfNights = new System.Windows.Forms.Label();
            this.pnlRoomDetails = new System.Windows.Forms.Panel();
            this.btnClear = new System.Windows.Forms.Button();
            this.grpPersonDetails = new System.Windows.Forms.GroupBox();
            this.lblPersonMobileNumber = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPersonEmail = new System.Windows.Forms.Label();
            this.lblPersonNationality = new System.Windows.Forms.Label();
            this.lblPersonGender = new System.Windows.Forms.Label();
            this.lblPersonDateOfBirth = new System.Windows.Forms.Label();
            this.lblPersonSurname = new System.Windows.Forms.Label();
            this.lblSurname = new System.Windows.Forms.Label();
            this.lblPersonName = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblMobileNumber = new System.Windows.Forms.Label();
            this.txtMobileNumber = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblGender = new System.Windows.Forms.Label();
            this.lblNationality = new System.Windows.Forms.Label();
            this.grpCheckInDetails = new System.Windows.Forms.GroupBox();
            this.lblNumberOfNightsValue = new System.Windows.Forms.Label();
            this.lblNumberOfPersonsValue = new System.Windows.Forms.Label();
            this.lblDepartureDateValue = new System.Windows.Forms.Label();
            this.lblArrivalDateValue = new System.Windows.Forms.Label();
            this.lblGuestIDValue = new System.Windows.Forms.Label();
            this.lblBookingNumber = new System.Windows.Forms.Label();
            this.cmbBookingNumber = new System.Windows.Forms.ComboBox();
            this.lblAddCheckIns = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBookedRooms)).BeginInit();
            this.pnlAddCheckIn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlRoomDetails.SuspendLayout();
            this.grpPersonDetails.SuspendLayout();
            this.grpCheckInDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvBookedRooms
            // 
            this.dgvBookedRooms.AllowUserToAddRows = false;
            this.dgvBookedRooms.BackgroundColor = System.Drawing.SystemColors.WindowFrame;
            this.dgvBookedRooms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBookedRooms.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            this.dgvBookedRooms.Location = new System.Drawing.Point(284, 444);
            this.dgvBookedRooms.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvBookedRooms.Name = "dgvBookedRooms";
            this.dgvBookedRooms.RowHeadersWidth = 5;
            this.dgvBookedRooms.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBookedRooms.Size = new System.Drawing.Size(544, 133);
            this.dgvBookedRooms.TabIndex = 55;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Room Number";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 125;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Room Type";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 125;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Bed Type";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 125;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Price";
            this.dataGridViewTextBoxColumn8.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 125;
            // 
            // lblAddReservation
            // 
            this.lblAddReservation.AutoSize = true;
            this.lblAddReservation.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddReservation.ForeColor = System.Drawing.Color.White;
            this.lblAddReservation.Location = new System.Drawing.Point(776, 45);
            this.lblAddReservation.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddReservation.Name = "lblAddReservation";
            this.lblAddReservation.Size = new System.Drawing.Size(254, 42);
            this.lblAddReservation.TabIndex = 3;
            this.lblAddReservation.Text = "Add Check In";
            // 
            // lblGuestID
            // 
            this.lblGuestID.AutoSize = true;
            this.lblGuestID.BackColor = System.Drawing.Color.Transparent;
            this.lblGuestID.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGuestID.Location = new System.Drawing.Point(8, 79);
            this.lblGuestID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGuestID.Name = "lblGuestID";
            this.lblGuestID.Size = new System.Drawing.Size(84, 24);
            this.lblGuestID.TabIndex = 26;
            this.lblGuestID.Text = "Guest Id:";
            // 
            // btnCheckIn
            // 
            this.btnCheckIn.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnCheckIn.Font = new System.Drawing.Font("Candara", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckIn.ForeColor = System.Drawing.Color.White;
            this.btnCheckIn.Location = new System.Drawing.Point(687, 628);
            this.btnCheckIn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCheckIn.Name = "btnCheckIn";
            this.btnCheckIn.Size = new System.Drawing.Size(155, 44);
            this.btnCheckIn.TabIndex = 52;
            this.btnCheckIn.Text = "Check In";
            this.btnCheckIn.UseVisualStyleBackColor = false;
            this.btnCheckIn.Click += new System.EventHandler(this.btnCheckIn_Click);
            this.btnCheckIn.MouseLeave += new System.EventHandler(this.btnCheckIn_MouseLeave);
            this.btnCheckIn.MouseHover += new System.EventHandler(this.btnCheckIn_MouseHover);
            // 
            // lblDepartureDate
            // 
            this.lblDepartureDate.AutoSize = true;
            this.lblDepartureDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDepartureDate.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDepartureDate.Location = new System.Drawing.Point(8, 209);
            this.lblDepartureDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDepartureDate.Name = "lblDepartureDate";
            this.lblDepartureDate.Size = new System.Drawing.Size(145, 24);
            this.lblDepartureDate.TabIndex = 28;
            this.lblDepartureDate.Text = "Departure Date:";
            // 
            // lblBookedRooms
            // 
            this.lblBookedRooms.AutoSize = true;
            this.lblBookedRooms.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold);
            this.lblBookedRooms.Location = new System.Drawing.Point(447, 394);
            this.lblBookedRooms.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBookedRooms.Name = "lblBookedRooms";
            this.lblBookedRooms.Size = new System.Drawing.Size(222, 24);
            this.lblBookedRooms.TabIndex = 38;
            this.lblBookedRooms.Text = "Details of Booked Rooms";
            // 
            // pnlAddCheckIn
            // 
            this.pnlAddCheckIn.BackColor = System.Drawing.Color.DarkTurquoise;
            this.pnlAddCheckIn.Controls.Add(this.pictureBox1);
            this.pnlAddCheckIn.Controls.Add(this.lblAddReservation);
            this.pnlAddCheckIn.Location = new System.Drawing.Point(0, 0);
            this.pnlAddCheckIn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlAddCheckIn.Name = "pnlAddCheckIn";
            this.pnlAddCheckIn.Size = new System.Drawing.Size(1073, 123);
            this.pnlAddCheckIn.TabIndex = 38;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(16, 15);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(295, 97);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 40;
            this.pictureBox1.TabStop = false;
            // 
            // lblNumberOfPersons
            // 
            this.lblNumberOfPersons.AutoSize = true;
            this.lblNumberOfPersons.BackColor = System.Drawing.Color.Transparent;
            this.lblNumberOfPersons.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumberOfPersons.Location = new System.Drawing.Point(8, 119);
            this.lblNumberOfPersons.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumberOfPersons.Name = "lblNumberOfPersons";
            this.lblNumberOfPersons.Size = new System.Drawing.Size(175, 24);
            this.lblNumberOfPersons.TabIndex = 29;
            this.lblNumberOfPersons.Text = "Number of Persons:";
            // 
            // lblArrivalDate
            // 
            this.lblArrivalDate.AutoSize = true;
            this.lblArrivalDate.BackColor = System.Drawing.Color.Transparent;
            this.lblArrivalDate.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArrivalDate.Location = new System.Drawing.Point(8, 164);
            this.lblArrivalDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblArrivalDate.Name = "lblArrivalDate";
            this.lblArrivalDate.Size = new System.Drawing.Size(114, 24);
            this.lblArrivalDate.TabIndex = 27;
            this.lblArrivalDate.Text = "Arrival Date:";
            // 
            // lblNumberOfNights
            // 
            this.lblNumberOfNights.AutoSize = true;
            this.lblNumberOfNights.BackColor = System.Drawing.Color.Transparent;
            this.lblNumberOfNights.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumberOfNights.Location = new System.Drawing.Point(8, 256);
            this.lblNumberOfNights.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumberOfNights.Name = "lblNumberOfNights";
            this.lblNumberOfNights.Size = new System.Drawing.Size(166, 24);
            this.lblNumberOfNights.TabIndex = 30;
            this.lblNumberOfNights.Text = "Number Of Nights:";
            // 
            // pnlRoomDetails
            // 
            this.pnlRoomDetails.BackColor = System.Drawing.Color.FloralWhite;
            this.pnlRoomDetails.Controls.Add(this.btnClear);
            this.pnlRoomDetails.Controls.Add(this.grpPersonDetails);
            this.pnlRoomDetails.Controls.Add(this.grpCheckInDetails);
            this.pnlRoomDetails.Controls.Add(this.lblAddCheckIns);
            this.pnlRoomDetails.Controls.Add(this.dgvBookedRooms);
            this.pnlRoomDetails.Controls.Add(this.btnCheckIn);
            this.pnlRoomDetails.Controls.Add(this.lblBookedRooms);
            this.pnlRoomDetails.Location = new System.Drawing.Point(0, 123);
            this.pnlRoomDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlRoomDetails.Name = "pnlRoomDetails";
            this.pnlRoomDetails.Size = new System.Drawing.Size(1069, 705);
            this.pnlRoomDetails.TabIndex = 39;
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnClear.Font = new System.Drawing.Font("Candara", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(873, 628);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(155, 44);
            this.btnClear.TabIndex = 69;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            this.btnClear.MouseLeave += new System.EventHandler(this.btnClear_MouseLeave);
            this.btnClear.MouseHover += new System.EventHandler(this.btnClear_MouseHover);
            // 
            // grpPersonDetails
            // 
            this.grpPersonDetails.Controls.Add(this.lblPersonMobileNumber);
            this.grpPersonDetails.Controls.Add(this.label2);
            this.grpPersonDetails.Controls.Add(this.lblPersonEmail);
            this.grpPersonDetails.Controls.Add(this.lblPersonNationality);
            this.grpPersonDetails.Controls.Add(this.lblPersonGender);
            this.grpPersonDetails.Controls.Add(this.lblPersonDateOfBirth);
            this.grpPersonDetails.Controls.Add(this.lblPersonSurname);
            this.grpPersonDetails.Controls.Add(this.lblSurname);
            this.grpPersonDetails.Controls.Add(this.lblPersonName);
            this.grpPersonDetails.Controls.Add(this.lblName);
            this.grpPersonDetails.Controls.Add(this.lblMobileNumber);
            this.grpPersonDetails.Controls.Add(this.txtMobileNumber);
            this.grpPersonDetails.Controls.Add(this.lblEmail);
            this.grpPersonDetails.Controls.Add(this.label1);
            this.grpPersonDetails.Controls.Add(this.lblGender);
            this.grpPersonDetails.Controls.Add(this.lblNationality);
            this.grpPersonDetails.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpPersonDetails.Location = new System.Drawing.Point(579, 73);
            this.grpPersonDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpPersonDetails.Name = "grpPersonDetails";
            this.grpPersonDetails.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpPersonDetails.Size = new System.Drawing.Size(451, 300);
            this.grpPersonDetails.TabIndex = 67;
            this.grpPersonDetails.TabStop = false;
            this.grpPersonDetails.Text = "Person Details";
            // 
            // lblPersonMobileNumber
            // 
            this.lblPersonMobileNumber.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonMobileNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonMobileNumber.Location = new System.Drawing.Point(168, 246);
            this.lblPersonMobileNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonMobileNumber.Name = "lblPersonMobileNumber";
            this.lblPersonMobileNumber.Size = new System.Drawing.Size(275, 20);
            this.lblPersonMobileNumber.TabIndex = 73;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FloralWhite;
            this.label2.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 244);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 24);
            this.label2.TabIndex = 72;
            this.label2.Text = "Mobile Number:";
            // 
            // lblPersonEmail
            // 
            this.lblPersonEmail.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonEmail.Location = new System.Drawing.Point(168, 209);
            this.lblPersonEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonEmail.Name = "lblPersonEmail";
            this.lblPersonEmail.Size = new System.Drawing.Size(275, 20);
            this.lblPersonEmail.TabIndex = 71;
            // 
            // lblPersonNationality
            // 
            this.lblPersonNationality.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonNationality.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonNationality.Location = new System.Drawing.Point(168, 174);
            this.lblPersonNationality.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonNationality.Name = "lblPersonNationality";
            this.lblPersonNationality.Size = new System.Drawing.Size(275, 20);
            this.lblPersonNationality.TabIndex = 70;
            // 
            // lblPersonGender
            // 
            this.lblPersonGender.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonGender.Location = new System.Drawing.Point(168, 139);
            this.lblPersonGender.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonGender.Name = "lblPersonGender";
            this.lblPersonGender.Size = new System.Drawing.Size(275, 20);
            this.lblPersonGender.TabIndex = 69;
            // 
            // lblPersonDateOfBirth
            // 
            this.lblPersonDateOfBirth.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonDateOfBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonDateOfBirth.Location = new System.Drawing.Point(168, 103);
            this.lblPersonDateOfBirth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonDateOfBirth.Name = "lblPersonDateOfBirth";
            this.lblPersonDateOfBirth.Size = new System.Drawing.Size(275, 20);
            this.lblPersonDateOfBirth.TabIndex = 68;
            // 
            // lblPersonSurname
            // 
            this.lblPersonSurname.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonSurname.Location = new System.Drawing.Point(168, 68);
            this.lblPersonSurname.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonSurname.Name = "lblPersonSurname";
            this.lblPersonSurname.Size = new System.Drawing.Size(275, 20);
            this.lblPersonSurname.TabIndex = 67;
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.BackColor = System.Drawing.Color.FloralWhite;
            this.lblSurname.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSurname.Location = new System.Drawing.Point(8, 68);
            this.lblSurname.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(90, 24);
            this.lblSurname.TabIndex = 2;
            this.lblSurname.Text = "Surname:";
            // 
            // lblPersonName
            // 
            this.lblPersonName.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonName.Location = new System.Drawing.Point(168, 33);
            this.lblPersonName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonName.Name = "lblPersonName";
            this.lblPersonName.Size = new System.Drawing.Size(275, 20);
            this.lblPersonName.TabIndex = 66;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(8, 33);
            this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(64, 24);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Name:";
            // 
            // lblMobileNumber
            // 
            this.lblMobileNumber.AutoSize = true;
            this.lblMobileNumber.BackColor = System.Drawing.Color.FloralWhite;
            this.lblMobileNumber.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMobileNumber.Location = new System.Drawing.Point(8, 345);
            this.lblMobileNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMobileNumber.Name = "lblMobileNumber";
            this.lblMobileNumber.Size = new System.Drawing.Size(145, 24);
            this.lblMobileNumber.TabIndex = 7;
            this.lblMobileNumber.Text = "Mobile Number:";
            // 
            // txtMobileNumber
            // 
            this.txtMobileNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtMobileNumber.Location = new System.Drawing.Point(184, 343);
            this.txtMobileNumber.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtMobileNumber.Name = "txtMobileNumber";
            this.txtMobileNumber.Size = new System.Drawing.Size(165, 23);
            this.txtMobileNumber.TabIndex = 12;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.BackColor = System.Drawing.Color.FloralWhite;
            this.lblEmail.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(8, 209);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(60, 24);
            this.lblEmail.TabIndex = 6;
            this.lblEmail.Text = "Email:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FloralWhite;
            this.label1.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 103);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "Date of Birth:";
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.BackColor = System.Drawing.Color.FloralWhite;
            this.lblGender.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGender.Location = new System.Drawing.Point(8, 139);
            this.lblGender.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(76, 24);
            this.lblGender.TabIndex = 4;
            this.lblGender.Text = "Gender:";
            // 
            // lblNationality
            // 
            this.lblNationality.AutoSize = true;
            this.lblNationality.BackColor = System.Drawing.Color.FloralWhite;
            this.lblNationality.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNationality.Location = new System.Drawing.Point(8, 174);
            this.lblNationality.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNationality.Name = "lblNationality";
            this.lblNationality.Size = new System.Drawing.Size(106, 24);
            this.lblNationality.TabIndex = 5;
            this.lblNationality.Text = "Nationality:";
            // 
            // grpCheckInDetails
            // 
            this.grpCheckInDetails.Controls.Add(this.lblNumberOfNightsValue);
            this.grpCheckInDetails.Controls.Add(this.lblNumberOfPersonsValue);
            this.grpCheckInDetails.Controls.Add(this.lblDepartureDateValue);
            this.grpCheckInDetails.Controls.Add(this.lblArrivalDateValue);
            this.grpCheckInDetails.Controls.Add(this.lblGuestIDValue);
            this.grpCheckInDetails.Controls.Add(this.lblBookingNumber);
            this.grpCheckInDetails.Controls.Add(this.cmbBookingNumber);
            this.grpCheckInDetails.Controls.Add(this.lblGuestID);
            this.grpCheckInDetails.Controls.Add(this.lblNumberOfNights);
            this.grpCheckInDetails.Controls.Add(this.lblDepartureDate);
            this.grpCheckInDetails.Controls.Add(this.lblArrivalDate);
            this.grpCheckInDetails.Controls.Add(this.lblNumberOfPersons);
            this.grpCheckInDetails.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpCheckInDetails.Location = new System.Drawing.Point(37, 73);
            this.grpCheckInDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpCheckInDetails.Name = "grpCheckInDetails";
            this.grpCheckInDetails.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpCheckInDetails.Size = new System.Drawing.Size(459, 300);
            this.grpCheckInDetails.TabIndex = 68;
            this.grpCheckInDetails.TabStop = false;
            this.grpCheckInDetails.Text = "Check In details";
            // 
            // lblNumberOfNightsValue
            // 
            this.lblNumberOfNightsValue.AutoSize = true;
            this.lblNumberOfNightsValue.BackColor = System.Drawing.Color.Transparent;
            this.lblNumberOfNightsValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblNumberOfNightsValue.Location = new System.Drawing.Point(216, 261);
            this.lblNumberOfNightsValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumberOfNightsValue.Name = "lblNumberOfNightsValue";
            this.lblNumberOfNightsValue.Size = new System.Drawing.Size(0, 17);
            this.lblNumberOfNightsValue.TabIndex = 71;
            // 
            // lblNumberOfPersonsValue
            // 
            this.lblNumberOfPersonsValue.AutoSize = true;
            this.lblNumberOfPersonsValue.BackColor = System.Drawing.Color.Transparent;
            this.lblNumberOfPersonsValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblNumberOfPersonsValue.Location = new System.Drawing.Point(216, 124);
            this.lblNumberOfPersonsValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumberOfPersonsValue.Name = "lblNumberOfPersonsValue";
            this.lblNumberOfPersonsValue.Size = new System.Drawing.Size(0, 17);
            this.lblNumberOfPersonsValue.TabIndex = 70;
            // 
            // lblDepartureDateValue
            // 
            this.lblDepartureDateValue.AutoSize = true;
            this.lblDepartureDateValue.BackColor = System.Drawing.Color.Transparent;
            this.lblDepartureDateValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblDepartureDateValue.Location = new System.Drawing.Point(215, 214);
            this.lblDepartureDateValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDepartureDateValue.Name = "lblDepartureDateValue";
            this.lblDepartureDateValue.Size = new System.Drawing.Size(0, 17);
            this.lblDepartureDateValue.TabIndex = 69;
            // 
            // lblArrivalDateValue
            // 
            this.lblArrivalDateValue.AutoSize = true;
            this.lblArrivalDateValue.BackColor = System.Drawing.Color.Transparent;
            this.lblArrivalDateValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblArrivalDateValue.Location = new System.Drawing.Point(215, 169);
            this.lblArrivalDateValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblArrivalDateValue.Name = "lblArrivalDateValue";
            this.lblArrivalDateValue.Size = new System.Drawing.Size(0, 17);
            this.lblArrivalDateValue.TabIndex = 68;
            // 
            // lblGuestIDValue
            // 
            this.lblGuestIDValue.AutoSize = true;
            this.lblGuestIDValue.BackColor = System.Drawing.Color.Transparent;
            this.lblGuestIDValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblGuestIDValue.Location = new System.Drawing.Point(216, 86);
            this.lblGuestIDValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGuestIDValue.Name = "lblGuestIDValue";
            this.lblGuestIDValue.Size = new System.Drawing.Size(0, 17);
            this.lblGuestIDValue.TabIndex = 67;
            // 
            // lblBookingNumber
            // 
            this.lblBookingNumber.AutoSize = true;
            this.lblBookingNumber.BackColor = System.Drawing.Color.Transparent;
            this.lblBookingNumber.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBookingNumber.Location = new System.Drawing.Point(8, 38);
            this.lblBookingNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBookingNumber.Name = "lblBookingNumber";
            this.lblBookingNumber.Size = new System.Drawing.Size(157, 24);
            this.lblBookingNumber.TabIndex = 65;
            this.lblBookingNumber.Text = "Booking Number:";
            // 
            // cmbBookingNumber
            // 
            this.cmbBookingNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBookingNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBookingNumber.FormattingEnabled = true;
            this.cmbBookingNumber.Location = new System.Drawing.Point(220, 38);
            this.cmbBookingNumber.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbBookingNumber.Name = "cmbBookingNumber";
            this.cmbBookingNumber.Size = new System.Drawing.Size(132, 25);
            this.cmbBookingNumber.TabIndex = 66;
            this.cmbBookingNumber.DropDownClosed += new System.EventHandler(this.cmbBookingNumber_DropDownClosed);
            // 
            // lblAddCheckIns
            // 
            this.lblAddCheckIns.AutoSize = true;
            this.lblAddCheckIns.BackColor = System.Drawing.Color.Transparent;
            this.lblAddCheckIns.Font = new System.Drawing.Font("Candara", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddCheckIns.Location = new System.Drawing.Point(409, 18);
            this.lblAddCheckIns.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddCheckIns.Name = "lblAddCheckIns";
            this.lblAddCheckIns.Size = new System.Drawing.Size(281, 37);
            this.lblAddCheckIns.TabIndex = 67;
            this.lblAddCheckIns.Text = "Add Check In Details";
            // 
            // AddCheckIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 821);
            this.Controls.Add(this.pnlAddCheckIn);
            this.Controls.Add(this.pnlRoomDetails);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "AddCheckIn";
            this.Text = "AddCheckIn";
            ((System.ComponentModel.ISupportInitialize)(this.dgvBookedRooms)).EndInit();
            this.pnlAddCheckIn.ResumeLayout(false);
            this.pnlAddCheckIn.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlRoomDetails.ResumeLayout(false);
            this.pnlRoomDetails.PerformLayout();
            this.grpPersonDetails.ResumeLayout(false);
            this.grpPersonDetails.PerformLayout();
            this.grpCheckInDetails.ResumeLayout(false);
            this.grpCheckInDetails.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvBookedRooms;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.Label lblAddReservation;
        private System.Windows.Forms.Label lblGuestID;
        private System.Windows.Forms.Button btnCheckIn;
        private System.Windows.Forms.Label lblDepartureDate;
        private System.Windows.Forms.Label lblBookedRooms;
        private System.Windows.Forms.Panel pnlAddCheckIn;
        private System.Windows.Forms.Label lblNumberOfPersons;
        private System.Windows.Forms.Label lblArrivalDate;
        private System.Windows.Forms.Label lblNumberOfNights;
        private System.Windows.Forms.Panel pnlRoomDetails;
        private System.Windows.Forms.ComboBox cmbBookingNumber;
        private System.Windows.Forms.Label lblBookingNumber;
        private System.Windows.Forms.Label lblAddCheckIns;
        private System.Windows.Forms.GroupBox grpCheckInDetails;
        private System.Windows.Forms.GroupBox grpPersonDetails;
        private System.Windows.Forms.Label lblPersonMobileNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPersonEmail;
        private System.Windows.Forms.Label lblPersonNationality;
        private System.Windows.Forms.Label lblPersonGender;
        private System.Windows.Forms.Label lblPersonDateOfBirth;
        private System.Windows.Forms.Label lblPersonSurname;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.Label lblPersonName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblMobileNumber;
        private System.Windows.Forms.TextBox txtMobileNumber;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label lblNationality;
        private System.Windows.Forms.Label lblGuestIDValue;
        private System.Windows.Forms.Label lblNumberOfNightsValue;
        private System.Windows.Forms.Label lblNumberOfPersonsValue;
        private System.Windows.Forms.Label lblDepartureDateValue;
        private System.Windows.Forms.Label lblArrivalDateValue;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}