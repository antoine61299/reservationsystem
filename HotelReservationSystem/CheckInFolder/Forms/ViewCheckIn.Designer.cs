﻿namespace HotelReservationSystem.CheckInFolder
{
    partial class ViewCheckIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewCheckIn));
            this.pnlCheckInRoomDetails = new System.Windows.Forms.Panel();
            this.dgvCheckedInRooms = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RoomNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblCheckedInRooms = new System.Windows.Forms.Label();
            this.lblViewCheckIn = new System.Windows.Forms.Label();
            this.pnlViewCheckIn = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlCheckInRoomDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCheckedInRooms)).BeginInit();
            this.pnlViewCheckIn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlCheckInRoomDetails
            // 
            this.pnlCheckInRoomDetails.BackColor = System.Drawing.Color.FloralWhite;
            this.pnlCheckInRoomDetails.Controls.Add(this.dgvCheckedInRooms);
            this.pnlCheckInRoomDetails.Controls.Add(this.lblCheckedInRooms);
            this.pnlCheckInRoomDetails.Location = new System.Drawing.Point(0, 123);
            this.pnlCheckInRoomDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlCheckInRoomDetails.Name = "pnlCheckInRoomDetails";
            this.pnlCheckInRoomDetails.Size = new System.Drawing.Size(1171, 700);
            this.pnlCheckInRoomDetails.TabIndex = 41;
            // 
            // dgvCheckedInRooms
            // 
            this.dgvCheckedInRooms.AllowUserToAddRows = false;
            this.dgvCheckedInRooms.BackgroundColor = System.Drawing.SystemColors.WindowFrame;
            this.dgvCheckedInRooms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCheckedInRooms.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.RoomNumber});
            this.dgvCheckedInRooms.Location = new System.Drawing.Point(53, 78);
            this.dgvCheckedInRooms.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvCheckedInRooms.Name = "dgvCheckedInRooms";
            this.dgvCheckedInRooms.ReadOnly = true;
            this.dgvCheckedInRooms.RowHeadersWidth = 5;
            this.dgvCheckedInRooms.Size = new System.Drawing.Size(1057, 581);
            this.dgvCheckedInRooms.TabIndex = 58;
            this.dgvCheckedInRooms.SelectionChanged += new System.EventHandler(this.dgvCheckedInRooms_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Booking Number";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 110;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Guest ID";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 75;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Number Of Persons";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 125;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Number Of Nights";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 125;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Arrival Date";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 125;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DividerWidth = 2;
            this.dataGridViewTextBoxColumn7.HeaderText = "Departure Date";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 125;
            // 
            // RoomNumber
            // 
            this.RoomNumber.HeaderText = "Room Number";
            this.RoomNumber.MinimumWidth = 6;
            this.RoomNumber.Name = "RoomNumber";
            this.RoomNumber.ReadOnly = true;
            this.RoomNumber.Width = 125;
            // 
            // lblCheckedInRooms
            // 
            this.lblCheckedInRooms.AutoSize = true;
            this.lblCheckedInRooms.BackColor = System.Drawing.Color.Transparent;
            this.lblCheckedInRooms.Font = new System.Drawing.Font("Candara", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckedInRooms.Location = new System.Drawing.Point(477, 15);
            this.lblCheckedInRooms.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCheckedInRooms.Name = "lblCheckedInRooms";
            this.lblCheckedInRooms.Size = new System.Drawing.Size(283, 41);
            this.lblCheckedInRooms.TabIndex = 57;
            this.lblCheckedInRooms.Text = "Checked In Rooms";
            // 
            // lblViewCheckIn
            // 
            this.lblViewCheckIn.AutoSize = true;
            this.lblViewCheckIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblViewCheckIn.ForeColor = System.Drawing.Color.White;
            this.lblViewCheckIn.Location = new System.Drawing.Point(841, 41);
            this.lblViewCheckIn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblViewCheckIn.Name = "lblViewCheckIn";
            this.lblViewCheckIn.Size = new System.Drawing.Size(269, 42);
            this.lblViewCheckIn.TabIndex = 3;
            this.lblViewCheckIn.Text = "View Check In";
            // 
            // pnlViewCheckIn
            // 
            this.pnlViewCheckIn.BackColor = System.Drawing.Color.DarkTurquoise;
            this.pnlViewCheckIn.Controls.Add(this.pictureBox1);
            this.pnlViewCheckIn.Controls.Add(this.lblViewCheckIn);
            this.pnlViewCheckIn.Location = new System.Drawing.Point(0, 0);
            this.pnlViewCheckIn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlViewCheckIn.Name = "pnlViewCheckIn";
            this.pnlViewCheckIn.Size = new System.Drawing.Size(1175, 123);
            this.pnlViewCheckIn.TabIndex = 40;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(16, 15);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(295, 97);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 43;
            this.pictureBox1.TabStop = false;
            // 
            // ViewCheckIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1168, 821);
            this.Controls.Add(this.pnlCheckInRoomDetails);
            this.Controls.Add(this.pnlViewCheckIn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ViewCheckIn";
            this.Text = "View Check In";
            this.pnlCheckInRoomDetails.ResumeLayout(false);
            this.pnlCheckInRoomDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCheckedInRooms)).EndInit();
            this.pnlViewCheckIn.ResumeLayout(false);
            this.pnlViewCheckIn.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnlCheckInRoomDetails;
        private System.Windows.Forms.Label lblViewCheckIn;
        private System.Windows.Forms.Panel pnlViewCheckIn;
        private System.Windows.Forms.Label lblCheckedInRooms;
        private System.Windows.Forms.DataGridView dgvCheckedInRooms;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoomNumber;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}