﻿using HotelReservationSystem.DataLayer;
using HotelReservationSystem.Other;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelReservationSystem.CheckInFolder
{
    public partial class AddCheckIn : Form
    {
        public AddCheckIn()
        {
            InitializeComponent();
            populateBookingNumberComboBox();
            btnCheckIn.Visible = false;
            btnClear.Visible = false;
            
            if (cmbBookingNumber.Items.Count == 0)
            {
                cmbBookingNumber.Enabled = false;
                btnCheckIn.Visible = false;
                lblBookedRooms.Enabled = false;
                dgvBookedRooms.Enabled = false;
            }
        }

        public void clearData()
        {
            lblGuestIDValue.Text = "";
            lblNumberOfPersonsValue.Text = "";
            lblArrivalDateValue.Text = "";
            lblArrivalDateValue.Text = "";
            lblDepartureDateValue.Text = "";
            lblNumberOfNightsValue.Text = "";
            btnCheckIn.Visible = false;
            btnClear.Visible = false;
            dgvBookedRooms.Rows.Clear();
        }

        public void populateBookingNumberComboBox()
        {
            BookingManager bm = new BookingManager();
            List<int> bnList = bm.GetReservedBookingNo();
            foreach(int bn in bnList)
            {
                cmbBookingNumber.Items.Add(bn);
            }
        }

        public void setElementsInForm()
        {
            dgvBookedRooms.Rows.Clear();
            btnCheckIn.Visible = true;
            btnClear.Visible = true;

            BookingManager bm = new BookingManager();

            List<ReservationView> rvList = bm.GetView(int.Parse(cmbBookingNumber.SelectedItem.ToString()));

            foreach (ReservationView rv in rvList)
            {
                lblGuestIDValue.Text = rv.guestId;
                lblNumberOfPersonsValue.Text = rv.noOfPersons;
                lblDepartureDateValue.Text = rv.departure;
                lblArrivalDateValue.Text = rv.arrival;
                lblNumberOfNightsValue.Text = rv.noOfNights;
                lblPersonName.Text = rv.personName;
                lblPersonSurname.Text = rv.personSurname;
                lblPersonDateOfBirth.Text = rv.personDob;
                lblPersonGender.Text = rv.personGender;
                lblPersonNationality.Text = rv.nationality;
                lblPersonEmail.Text = rv.email;
                lblPersonMobileNumber.Text = rv.mobile;
                dgvBookedRooms.Rows.Add(rv.roomNumber, rv.roomType, rv.bedType, rv.price);
            }
        }

        private void cmbBookingNumber_DropDownClosed(object sender, EventArgs e)
        {
            if(cmbBookingNumber.SelectedItem != null)
            {
                setElementsInForm();
            }            
        }

        private void btnCheckIn_Click(object sender, EventArgs e)
        {
            BookingManager bm = new BookingManager();
            bm.UpdateStatusCheckIn(int.Parse(cmbBookingNumber.SelectedItem.ToString()));
            clearData();
            cmbBookingNumber.Items.Remove(cmbBookingNumber.SelectedItem);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clearData();
        }

        private void btnCheckIn_MouseHover(object sender, EventArgs e)
        {
            btnCheckIn.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnCheckIn.ForeColor = Color.Black;
        }

        private void btnCheckIn_MouseLeave(object sender, EventArgs e)
        {
            btnCheckIn.BackColor = Color.Black;
            btnCheckIn.ForeColor = Color.White;
        }

        private void btnClear_MouseHover(object sender, EventArgs e)
        {
            btnClear.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnClear.ForeColor = Color.Black;
        }

        private void btnClear_MouseLeave(object sender, EventArgs e)
        {
            btnClear.BackColor = Color.Black;
            btnClear.ForeColor = Color.White;
        }
    }
}
