﻿using System.Collections.Generic;
using System.Linq;
using HotelReservationSystem.DataLayer;
using NUnit.Framework;

namespace HotelReservationSystem.UnitTests
{
    [TestFixture]
    public class UnitTesting
    {
        [Test]
        public void FindRoomWithId12()
        {
            RoomManager rm = new RoomManager();

            bool roomFound = rm.FindRoom(12);

            Assert.IsTrue(roomFound);
        }

        [Test]
        public void HotelHasGuests()
        {
            GuestManager gm = new GuestManager();

            int noOfGuest = gm.GetAll().Count();

            Assert.Greater(noOfGuest, 0);
        }

        [Test]
        public void Booking44IsReserved()
        {
            BookingManager bm = new BookingManager();

            List<int> ciList = bm.GetCheckedInBookingNo();

            Assert.Contains(44, ciList);
        }

    }
}
