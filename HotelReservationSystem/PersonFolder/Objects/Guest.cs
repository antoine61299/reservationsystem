﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelReservationSystem.PersonFolder
{
    public class Guest : Person
    {

        public Guest(string name, string surname, string dateOfBirth, string nationality, string gender, string email, string mobileNumber) : base(name, surname, dateOfBirth, nationality, gender, email, mobileNumber)
        {
        }

        public Guest(int personId, string name, string surname, string dateOfBirth, string nationality, string gender, string email, string mobileNumber) : base(personId, name, surname, dateOfBirth, nationality, gender, email, mobileNumber)
        {
        }
    }
}
