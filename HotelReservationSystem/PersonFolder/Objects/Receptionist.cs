﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelReservationSystem.PersonFolder
{
    public class Receptionist: Person
    {
        public string username;
        public string password;

        public Receptionist(string name, string surname, string dateOfBirth, string nationality, string gender, string email, string mobileNumber, string username, string password) : base(name, surname, dateOfBirth, nationality, gender, email, mobileNumber)
        {
            this.username = username;
            this.password = password;
        }

        public Receptionist(int personId, string name, string surname, string dateOfBirth, string nationality, string gender, string email, string mobileNumber, string username, string password) : base(personId, name, surname, dateOfBirth, nationality, gender, email, mobileNumber)
        {
            this.username = username;
            this.password = password;
        }
    }
}
