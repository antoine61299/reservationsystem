﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelReservationSystem.PersonFolder
{
    public class Manager : Receptionist
    {
        public Manager(string name, string surname, string dateOfBirth, string nationality, string gender, string email, string mobileNumber, string username, string password) : base(name, surname, dateOfBirth, nationality, gender, email, mobileNumber, username, password)
        {

        }

        public Manager(int personId, string name, string surname, string dateOfBirth, string nationality, string gender, string email, string mobileNumber, string username, string password) : base(personId, name, surname, dateOfBirth, nationality, gender, email, mobileNumber, username, password)
        {

        }
    }
}
