﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelReservationSystem.PersonFolder
{
    public class Person
    {
        public int personId;
        public string name;
        public string surname;
        public string dateOfBirth;
        public string nationality;
        public string gender;
        public string email;
        public string mobileNumber;

        public Person(string name, string surname, string dateOfBirth, string nationality, string gender, string email, string mobileNumber)
        {
            this.name = name;
            this.surname = surname;
            this.dateOfBirth = dateOfBirth;
            this.nationality = nationality;
            this.gender = gender;
            this.email = email;
            this.mobileNumber = mobileNumber;
        }

        public Person(int personId, string name, string surname, string dateOfBirth, string nationality, string gender, string email, string mobileNumber)
        {
            this.personId = personId;
            this.name = name;
            this.surname = surname;
            this.dateOfBirth = dateOfBirth;
            this.nationality = nationality;
            this.gender = gender;
            this.email = email;
            this.mobileNumber = mobileNumber;
        }
    }
}
