﻿namespace HotelReservationSystem.PersonFolder
{
    partial class AddPerson
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddPerson));
            this.lblName = new System.Windows.Forms.Label();
            this.lblSurname = new System.Windows.Forms.Label();
            this.lblDateOfBirth = new System.Windows.Forms.Label();
            this.lblGender = new System.Windows.Forms.Label();
            this.lblNationality = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblMobileNumber = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtMobileNumber = new System.Windows.Forms.TextBox();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtNationality = new System.Windows.Forms.TextBox();
            this.txtSurname = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.rbnMale = new System.Windows.Forms.RadioButton();
            this.rbnFemale = new System.Windows.Forms.RadioButton();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblAddPerson = new System.Windows.Forms.Label();
            this.pnlAddPerson = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.gbxEmployeeDetails = new System.Windows.Forms.GroupBox();
            this.rbnReceptionist = new System.Windows.Forms.RadioButton();
            this.rbnManager = new System.Windows.Forms.RadioButton();
            this.rbnGuest = new System.Windows.Forms.RadioButton();
            this.lblPersonType = new System.Windows.Forms.Label();
            this.pnlPersonDetails = new System.Windows.Forms.Panel();
            this.lblPersonDetails = new System.Windows.Forms.Label();
            this.gbxPersonType = new System.Windows.Forms.GroupBox();
            this.gbxPersonDetails = new System.Windows.Forms.GroupBox();
            this.dtpDateOfBirth = new System.Windows.Forms.DateTimePicker();
            this.erpName = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpSurname = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpNationality = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpGender = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpDateOfBirth = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpEmail = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpMobileNumber = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpPersonType = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpUsername = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpPassword = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlAddPerson.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gbxEmployeeDetails.SuspendLayout();
            this.pnlPersonDetails.SuspendLayout();
            this.gbxPersonType.SuspendLayout();
            this.gbxPersonDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.erpName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpSurname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpNationality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpGender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpDateOfBirth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpMobileNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpPersonType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpUsername)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpPassword)).BeginInit();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(8, 33);
            this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(64, 24);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Name:";
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.BackColor = System.Drawing.Color.FloralWhite;
            this.lblSurname.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSurname.Location = new System.Drawing.Point(8, 86);
            this.lblSurname.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(90, 24);
            this.lblSurname.TabIndex = 2;
            this.lblSurname.Text = "Surname:";
            // 
            // lblDateOfBirth
            // 
            this.lblDateOfBirth.AutoSize = true;
            this.lblDateOfBirth.BackColor = System.Drawing.Color.FloralWhite;
            this.lblDateOfBirth.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateOfBirth.Location = new System.Drawing.Point(8, 135);
            this.lblDateOfBirth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateOfBirth.Name = "lblDateOfBirth";
            this.lblDateOfBirth.Size = new System.Drawing.Size(122, 24);
            this.lblDateOfBirth.TabIndex = 3;
            this.lblDateOfBirth.Text = "Date of Birth:";
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.BackColor = System.Drawing.Color.FloralWhite;
            this.lblGender.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGender.Location = new System.Drawing.Point(8, 185);
            this.lblGender.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(76, 24);
            this.lblGender.TabIndex = 4;
            this.lblGender.Text = "Gender:";
            // 
            // lblNationality
            // 
            this.lblNationality.AutoSize = true;
            this.lblNationality.BackColor = System.Drawing.Color.FloralWhite;
            this.lblNationality.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNationality.Location = new System.Drawing.Point(8, 238);
            this.lblNationality.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNationality.Name = "lblNationality";
            this.lblNationality.Size = new System.Drawing.Size(106, 24);
            this.lblNationality.TabIndex = 5;
            this.lblNationality.Text = "Nationality:";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.BackColor = System.Drawing.Color.FloralWhite;
            this.lblEmail.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(8, 289);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(60, 24);
            this.lblEmail.TabIndex = 6;
            this.lblEmail.Text = "Email:";
            // 
            // lblMobileNumber
            // 
            this.lblMobileNumber.AutoSize = true;
            this.lblMobileNumber.BackColor = System.Drawing.Color.FloralWhite;
            this.lblMobileNumber.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMobileNumber.Location = new System.Drawing.Point(8, 345);
            this.lblMobileNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMobileNumber.Name = "lblMobileNumber";
            this.lblMobileNumber.Size = new System.Drawing.Size(145, 24);
            this.lblMobileNumber.TabIndex = 7;
            this.lblMobileNumber.Text = "Mobile Number:";
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.BackColor = System.Drawing.Color.Transparent;
            this.lblUsername.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.Location = new System.Drawing.Point(8, 37);
            this.lblUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(101, 24);
            this.lblUsername.TabIndex = 8;
            this.lblUsername.Text = "Username:";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.BackColor = System.Drawing.Color.Transparent;
            this.lblPassword.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPassword.Location = new System.Drawing.Point(8, 81);
            this.lblPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(96, 24);
            this.lblPassword.TabIndex = 9;
            this.lblPassword.Text = "Password:";
            // 
            // txtMobileNumber
            // 
            this.txtMobileNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtMobileNumber.Location = new System.Drawing.Point(184, 345);
            this.txtMobileNumber.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtMobileNumber.Name = "txtMobileNumber";
            this.txtMobileNumber.Size = new System.Drawing.Size(165, 23);
            this.txtMobileNumber.TabIndex = 12;
            // 
            // txtUsername
            // 
            this.txtUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsername.Location = new System.Drawing.Point(181, 37);
            this.txtUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(165, 23);
            this.txtUsername.TabIndex = 13;
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtEmail.Location = new System.Drawing.Point(184, 289);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(165, 23);
            this.txtEmail.TabIndex = 14;
            // 
            // txtNationality
            // 
            this.txtNationality.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtNationality.Location = new System.Drawing.Point(184, 238);
            this.txtNationality.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNationality.Name = "txtNationality";
            this.txtNationality.Size = new System.Drawing.Size(165, 23);
            this.txtNationality.TabIndex = 15;
            // 
            // txtSurname
            // 
            this.txtSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtSurname.Location = new System.Drawing.Point(184, 86);
            this.txtSurname.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.Size = new System.Drawing.Size(165, 23);
            this.txtSurname.TabIndex = 17;
            // 
            // txtName
            // 
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.Location = new System.Drawing.Point(184, 33);
            this.txtName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(165, 23);
            this.txtName.TabIndex = 18;
            // 
            // rbnMale
            // 
            this.rbnMale.AutoSize = true;
            this.rbnMale.BackColor = System.Drawing.Color.FloralWhite;
            this.rbnMale.Checked = true;
            this.rbnMale.Font = new System.Drawing.Font("Candara", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbnMale.Location = new System.Drawing.Point(184, 185);
            this.rbnMale.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbnMale.Name = "rbnMale";
            this.rbnMale.Size = new System.Drawing.Size(59, 21);
            this.rbnMale.TabIndex = 19;
            this.rbnMale.TabStop = true;
            this.rbnMale.Text = "Male";
            this.rbnMale.UseVisualStyleBackColor = false;
            // 
            // rbnFemale
            // 
            this.rbnFemale.AutoSize = true;
            this.rbnFemale.BackColor = System.Drawing.Color.FloralWhite;
            this.rbnFemale.Font = new System.Drawing.Font("Candara", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbnFemale.Location = new System.Drawing.Point(272, 185);
            this.rbnFemale.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbnFemale.Name = "rbnFemale";
            this.rbnFemale.Size = new System.Drawing.Size(73, 21);
            this.rbnFemale.TabIndex = 20;
            this.rbnFemale.Text = "Female";
            this.rbnFemale.UseVisualStyleBackColor = false;
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(181, 81);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(165, 23);
            this.txtPassword.TabIndex = 21;
            // 
            // lblAddPerson
            // 
            this.lblAddPerson.AutoSize = true;
            this.lblAddPerson.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddPerson.ForeColor = System.Drawing.Color.White;
            this.lblAddPerson.Location = new System.Drawing.Point(692, 43);
            this.lblAddPerson.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddPerson.Name = "lblAddPerson";
            this.lblAddPerson.Size = new System.Drawing.Size(224, 42);
            this.lblAddPerson.TabIndex = 3;
            this.lblAddPerson.Text = "Add Person";
            // 
            // pnlAddPerson
            // 
            this.pnlAddPerson.BackColor = System.Drawing.Color.DarkTurquoise;
            this.pnlAddPerson.Controls.Add(this.pictureBox1);
            this.pnlAddPerson.Controls.Add(this.lblAddPerson);
            this.pnlAddPerson.Location = new System.Drawing.Point(0, 0);
            this.pnlAddPerson.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlAddPerson.Name = "pnlAddPerson";
            this.pnlAddPerson.Size = new System.Drawing.Size(952, 123);
            this.pnlAddPerson.TabIndex = 28;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(16, 15);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(295, 97);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 43;
            this.pictureBox1.TabStop = false;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnAdd.Font = new System.Drawing.Font("Candara", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(593, 430);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(155, 44);
            this.btnAdd.TabIndex = 29;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            this.btnAdd.MouseLeave += new System.EventHandler(this.btnAdd_MouseLeave);
            this.btnAdd.MouseHover += new System.EventHandler(this.btnAdd_MouseHover);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnClear.Font = new System.Drawing.Font("Candara", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(781, 430);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(155, 44);
            this.btnClear.TabIndex = 30;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            this.btnClear.MouseLeave += new System.EventHandler(this.btnClear_MouseLeave);
            this.btnClear.MouseHover += new System.EventHandler(this.btnClear_MouseHover);
            // 
            // gbxEmployeeDetails
            // 
            this.gbxEmployeeDetails.BackColor = System.Drawing.Color.Transparent;
            this.gbxEmployeeDetails.Controls.Add(this.lblUsername);
            this.gbxEmployeeDetails.Controls.Add(this.lblPassword);
            this.gbxEmployeeDetails.Controls.Add(this.txtPassword);
            this.gbxEmployeeDetails.Controls.Add(this.txtUsername);
            this.gbxEmployeeDetails.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxEmployeeDetails.Location = new System.Drawing.Point(509, 91);
            this.gbxEmployeeDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxEmployeeDetails.Name = "gbxEmployeeDetails";
            this.gbxEmployeeDetails.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxEmployeeDetails.Size = new System.Drawing.Size(427, 123);
            this.gbxEmployeeDetails.TabIndex = 33;
            this.gbxEmployeeDetails.TabStop = false;
            this.gbxEmployeeDetails.Text = "Employee Details";
            // 
            // rbnReceptionist
            // 
            this.rbnReceptionist.AutoSize = true;
            this.rbnReceptionist.BackColor = System.Drawing.Color.Transparent;
            this.rbnReceptionist.Font = new System.Drawing.Font("Candara", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbnReceptionist.Location = new System.Drawing.Point(304, 41);
            this.rbnReceptionist.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbnReceptionist.Name = "rbnReceptionist";
            this.rbnReceptionist.Size = new System.Drawing.Size(103, 21);
            this.rbnReceptionist.TabIndex = 24;
            this.rbnReceptionist.Text = "Receptionist";
            this.rbnReceptionist.UseVisualStyleBackColor = false;
            this.rbnReceptionist.Click += new System.EventHandler(this.rbnReceptionist_Click);
            // 
            // rbnManager
            // 
            this.rbnManager.AutoSize = true;
            this.rbnManager.BackColor = System.Drawing.Color.Transparent;
            this.rbnManager.Font = new System.Drawing.Font("Candara", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbnManager.Location = new System.Drawing.Point(216, 41);
            this.rbnManager.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbnManager.Name = "rbnManager";
            this.rbnManager.Size = new System.Drawing.Size(83, 21);
            this.rbnManager.TabIndex = 23;
            this.rbnManager.Text = "Manager";
            this.rbnManager.UseVisualStyleBackColor = false;
            this.rbnManager.Click += new System.EventHandler(this.rbnManager_Click);
            // 
            // rbnGuest
            // 
            this.rbnGuest.AutoSize = true;
            this.rbnGuest.BackColor = System.Drawing.Color.Transparent;
            this.rbnGuest.Checked = true;
            this.rbnGuest.Font = new System.Drawing.Font("Candara", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbnGuest.Location = new System.Drawing.Point(144, 41);
            this.rbnGuest.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbnGuest.Name = "rbnGuest";
            this.rbnGuest.Size = new System.Drawing.Size(64, 21);
            this.rbnGuest.TabIndex = 22;
            this.rbnGuest.TabStop = true;
            this.rbnGuest.Text = "Guest";
            this.rbnGuest.UseVisualStyleBackColor = false;
            this.rbnGuest.Click += new System.EventHandler(this.rbnGuest_Click);
            // 
            // lblPersonType
            // 
            this.lblPersonType.AutoSize = true;
            this.lblPersonType.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonType.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonType.Location = new System.Drawing.Point(8, 41);
            this.lblPersonType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonType.Name = "lblPersonType";
            this.lblPersonType.Size = new System.Drawing.Size(116, 24);
            this.lblPersonType.TabIndex = 21;
            this.lblPersonType.Text = "Person Type:";
            // 
            // pnlPersonDetails
            // 
            this.pnlPersonDetails.BackColor = System.Drawing.Color.FloralWhite;
            this.pnlPersonDetails.Controls.Add(this.lblPersonDetails);
            this.pnlPersonDetails.Controls.Add(this.gbxPersonType);
            this.pnlPersonDetails.Controls.Add(this.gbxPersonDetails);
            this.pnlPersonDetails.Controls.Add(this.btnClear);
            this.pnlPersonDetails.Controls.Add(this.gbxEmployeeDetails);
            this.pnlPersonDetails.Controls.Add(this.btnAdd);
            this.pnlPersonDetails.Location = new System.Drawing.Point(0, 123);
            this.pnlPersonDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlPersonDetails.Name = "pnlPersonDetails";
            this.pnlPersonDetails.Size = new System.Drawing.Size(948, 497);
            this.pnlPersonDetails.TabIndex = 35;
            // 
            // lblPersonDetails
            // 
            this.lblPersonDetails.AutoSize = true;
            this.lblPersonDetails.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonDetails.Font = new System.Drawing.Font("Candara", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonDetails.Location = new System.Drawing.Point(253, 20);
            this.lblPersonDetails.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonDetails.Name = "lblPersonDetails";
            this.lblPersonDetails.Size = new System.Drawing.Size(442, 37);
            this.lblPersonDetails.TabIndex = 57;
            this.lblPersonDetails.Text = "Enter the details to add a person";
            // 
            // gbxPersonType
            // 
            this.gbxPersonType.Controls.Add(this.rbnReceptionist);
            this.gbxPersonType.Controls.Add(this.lblPersonType);
            this.gbxPersonType.Controls.Add(this.rbnManager);
            this.gbxPersonType.Controls.Add(this.rbnGuest);
            this.gbxPersonType.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxPersonType.Location = new System.Drawing.Point(509, 278);
            this.gbxPersonType.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxPersonType.Name = "gbxPersonType";
            this.gbxPersonType.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxPersonType.Size = new System.Drawing.Size(427, 80);
            this.gbxPersonType.TabIndex = 37;
            this.gbxPersonType.TabStop = false;
            this.gbxPersonType.Text = "Person Type";
            // 
            // gbxPersonDetails
            // 
            this.gbxPersonDetails.Controls.Add(this.dtpDateOfBirth);
            this.gbxPersonDetails.Controls.Add(this.lblSurname);
            this.gbxPersonDetails.Controls.Add(this.lblName);
            this.gbxPersonDetails.Controls.Add(this.lblMobileNumber);
            this.gbxPersonDetails.Controls.Add(this.txtMobileNumber);
            this.gbxPersonDetails.Controls.Add(this.lblEmail);
            this.gbxPersonDetails.Controls.Add(this.rbnFemale);
            this.gbxPersonDetails.Controls.Add(this.txtEmail);
            this.gbxPersonDetails.Controls.Add(this.txtName);
            this.gbxPersonDetails.Controls.Add(this.txtSurname);
            this.gbxPersonDetails.Controls.Add(this.lblDateOfBirth);
            this.gbxPersonDetails.Controls.Add(this.rbnMale);
            this.gbxPersonDetails.Controls.Add(this.txtNationality);
            this.gbxPersonDetails.Controls.Add(this.lblGender);
            this.gbxPersonDetails.Controls.Add(this.lblNationality);
            this.gbxPersonDetails.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxPersonDetails.Location = new System.Drawing.Point(17, 80);
            this.gbxPersonDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxPersonDetails.Name = "gbxPersonDetails";
            this.gbxPersonDetails.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxPersonDetails.Size = new System.Drawing.Size(471, 396);
            this.gbxPersonDetails.TabIndex = 36;
            this.gbxPersonDetails.TabStop = false;
            this.gbxPersonDetails.Text = "Person Details";
            // 
            // dtpDateOfBirth
            // 
            this.dtpDateOfBirth.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.dtpDateOfBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.dtpDateOfBirth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDateOfBirth.Location = new System.Drawing.Point(184, 135);
            this.dtpDateOfBirth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpDateOfBirth.Name = "dtpDateOfBirth";
            this.dtpDateOfBirth.Size = new System.Drawing.Size(165, 23);
            this.dtpDateOfBirth.TabIndex = 25;
            this.dtpDateOfBirth.Value = new System.DateTime(2019, 5, 11, 0, 0, 0, 0);
            // 
            // erpName
            // 
            this.erpName.ContainerControl = this;
            // 
            // erpSurname
            // 
            this.erpSurname.ContainerControl = this;
            // 
            // erpNationality
            // 
            this.erpNationality.ContainerControl = this;
            // 
            // erpGender
            // 
            this.erpGender.ContainerControl = this;
            // 
            // erpDateOfBirth
            // 
            this.erpDateOfBirth.ContainerControl = this;
            // 
            // erpEmail
            // 
            this.erpEmail.ContainerControl = this;
            // 
            // erpMobileNumber
            // 
            this.erpMobileNumber.ContainerControl = this;
            // 
            // erpPersonType
            // 
            this.erpPersonType.ContainerControl = this;
            // 
            // erpUsername
            // 
            this.erpUsername.ContainerControl = this;
            // 
            // erpPassword
            // 
            this.erpPassword.ContainerControl = this;
            // 
            // AddPerson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(949, 617);
            this.Controls.Add(this.pnlPersonDetails);
            this.Controls.Add(this.pnlAddPerson);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "AddPerson";
            this.Text = "Add Persons";
            this.pnlAddPerson.ResumeLayout(false);
            this.pnlAddPerson.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gbxEmployeeDetails.ResumeLayout(false);
            this.gbxEmployeeDetails.PerformLayout();
            this.pnlPersonDetails.ResumeLayout(false);
            this.pnlPersonDetails.PerformLayout();
            this.gbxPersonType.ResumeLayout(false);
            this.gbxPersonType.PerformLayout();
            this.gbxPersonDetails.ResumeLayout(false);
            this.gbxPersonDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.erpName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpSurname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpNationality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpGender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpDateOfBirth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpMobileNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpPersonType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpUsername)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpPassword)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.Label lblDateOfBirth;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label lblNationality;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblMobileNumber;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtMobileNumber;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtNationality;
        private System.Windows.Forms.TextBox txtSurname;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.RadioButton rbnMale;
        private System.Windows.Forms.RadioButton rbnFemale;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblAddPerson;
        private System.Windows.Forms.Panel pnlAddPerson;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblPersonType;
        private System.Windows.Forms.Panel pnlPersonDetails;
        private System.Windows.Forms.GroupBox gbxPersonDetails;
        private System.Windows.Forms.ErrorProvider erpName;
        private System.Windows.Forms.ErrorProvider erpSurname;
        private System.Windows.Forms.ErrorProvider erpNationality;
        private System.Windows.Forms.ErrorProvider erpGender;
        private System.Windows.Forms.ErrorProvider erpDateOfBirth;
        private System.Windows.Forms.ErrorProvider erpEmail;
        private System.Windows.Forms.ErrorProvider erpMobileNumber;
        private System.Windows.Forms.ErrorProvider erpPersonType;
        private System.Windows.Forms.ErrorProvider erpUsername;
        private System.Windows.Forms.ErrorProvider erpPassword;
        private System.Windows.Forms.DateTimePicker dtpDateOfBirth;
        private System.Windows.Forms.Label lblPersonDetails;
        public System.Windows.Forms.RadioButton rbnManager;
        public System.Windows.Forms.GroupBox gbxPersonType;
        public System.Windows.Forms.GroupBox gbxEmployeeDetails;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.RadioButton rbnReceptionist;
        public System.Windows.Forms.RadioButton rbnGuest;
    }
}