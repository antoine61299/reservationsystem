﻿using HotelReservationSystem.DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelReservationSystem.PersonFolder
{
    public partial class AddPerson : Form
    {
        public AddPerson()
        {
            InitializeComponent();
            gbxEmployeeDetails.Visible = false;
            
        }

        //This method clears the textboxes once a person is added.
        public void clearData()
        {
            txtName.Text = "";
            txtSurname.Text = "";
            txtNationality.Text = "";
            txtEmail.Text = "";
            txtMobileNumber.Text = "";
            txtUsername.Text = "";
            txtPassword.Text = "";
        }

        //This method clears the error providers once the clear button is clicked.
        public void clearErrors()
        {
            erpName.Clear();
            erpSurname.Clear();
            erpDateOfBirth.Clear();
            erpNationality.Clear();
            erpEmail.Clear();
            erpMobileNumber.Clear();
            erpUsername.Clear();
            erpPassword.Clear();
        }

        //All the data that is entred in the AddPerson Form will be stored in a different variables to form a whole object which is the Person.
        public Object getDataFromForm()
        {
            string name = txtName.Text;
            string surname = txtSurname.Text;
            string dob = dtpDateOfBirth.Text;
            
            string nationality = txtNationality.Text;
            string gender = "";

            //The value of the gender radio button will be assigned to the gender variable.
            if(rbnMale.Checked)
            {
                gender = rbnMale.Text;   
            }
            else
            {
                gender = rbnFemale.Text;
            }
            string email = txtEmail.Text;
            string mobileNumber = txtMobileNumber.Text;
            string userName = txtUsername.Text;
            string password = txtPassword.Text;

            Guest guest;
            Receptionist receptionist;
            Manager manager;

            //The value of the  person type radio button will be assigned to the person type variable.
            if (rbnGuest.Checked)
            {
                guest = new Guest(name, surname, dob, nationality, gender, email, mobileNumber);
                return guest;
            }
            else if(rbnReceptionist.Checked)
            {
                receptionist = new Receptionist(name, surname, dob, nationality, gender, email, mobileNumber, userName, password);
                return receptionist;
            }
            else
            {
                manager = new Manager(name, surname, dob, nationality, gender, email, mobileNumber, userName, password);
                return manager;
            }
        }

        //Returns all the details of the Person validated.
        public bool validateInputs()
        {
            bool validated = true;

            if (txtName.Text == string.Empty)
            {
                erpName.SetError(txtName, "Please enter your name");
                validated = false;
            }
            else
            {
                if (int.TryParse(txtName.Text, out int numValue))
                {
                    erpName.SetError(txtName, "Only letters are accepted please try again");
                    validated = false;
                }
                else
                {
                    erpName.Clear();
                }
            }

            if (txtSurname.Text == string.Empty)
            {
                erpSurname.SetError(txtSurname, "Please enter your surname");
                validated = false;
            }
            else
            {
                if (int.TryParse(txtSurname.Text, out int numValue))
                {
                    erpSurname.SetError(txtSurname, "Only letters are accepted please try again");
                    validated = false;
                }
                else
                {
                    erpSurname.Clear();
                }
            }

            if (dtpDateOfBirth.Text == string.Empty)
            {
                erpDateOfBirth.SetError(dtpDateOfBirth, "Please enter your birth of date");
                validated = false;
            }
            else if (DateTime.Parse(dtpDateOfBirth.Text) > DateTime.Now)
            {
                erpDateOfBirth.SetError(dtpDateOfBirth, "Date is in the future! Please enter a valid date!");
                validated = false;
            }
            else
            {
                erpDateOfBirth.Clear();
            }

            if (txtNationality.Text == string.Empty)
            {
                erpNationality.SetError(txtNationality, "Please enter your nationality");
                validated = false;
            }
            else
            {
                if (int.TryParse(txtNationality.Text, out int numValue))
                {
                    erpNationality.SetError(txtNationality, "Only letters are accepted please try again");
                    validated = false;
                }
                else
                {
                    erpNationality.Clear();
                }
            }

            if (txtEmail.Text == string.Empty)
            {
                erpEmail.SetError(txtEmail, "Please enter your email");
                validated = false;
            }
            else
            {
                if (int.TryParse(txtEmail.Text, out int numValue))
                {
                    erpEmail.SetError(txtEmail, "Only letters and characters are accepted please try again");
                    validated = false;
                }
                else
                {
                    erpEmail.Clear();
                }
            }

            if (txtMobileNumber.Text == string.Empty)
            {
                erpMobileNumber.SetError(txtMobileNumber, "Please enter your mobile number");
                validated = false;
            }
            else
            {
                if (!int.TryParse(txtMobileNumber.Text, out int numValue))
                {
                    erpMobileNumber.SetError(txtMobileNumber, "Only numbers are accepted please try again");
                    validated = false;
                }
                else
                {
                    erpMobileNumber.Clear();
                }
            }

            if (rbnReceptionist.Checked || rbnManager.Checked)
            {
                if (txtUsername.Text == string.Empty)
                {
                    erpUsername.SetError(txtUsername, "Please enter your new username");
                    validated = false;
                }
                else
                {
                    erpUsername.Clear();
                }

                if (txtPassword.Text == string.Empty)
                {
                    erpPassword.SetError(txtPassword, "Please enter your new password");
                    validated = false;
                }
                else
                {
                    erpPassword.Clear();
                }
            }
            return validated;
        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (validateInputs())
            {
                GuestManager gm = new GuestManager();
                EmployeeManager em = new EmployeeManager();

                Object obj = getDataFromForm();

                if (obj.GetType().Name.Equals("Guest"))
                {
                    gm.Insert((Guest)obj);
                    MessageBox.Show("Guest is added", "Guest", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (obj.GetType().Name.Equals("Receptionist"))
                {
                    em.InsertReceptionist((Receptionist)obj);
                    MessageBox.Show("Receptionist is added", "Receptionist", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    em.InsertManager((Manager)obj);
                    MessageBox.Show("Manager is added", "Manager", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                clearData();
            }
        }
        //Event handler to hide the group box which contains the username and password details since it is only required for receptionist and manager.
        private void rbnGuest_Click(object sender, EventArgs e)
        {
            gbxEmployeeDetails.Visible = false;
        }

        //Event handler to display the group box which contains the username and password to receptionists.This is hidden from guests.
        private void rbnManager_Click(object sender, EventArgs e)
        {
            gbxEmployeeDetails.Visible = true;
        }

        //Event handler to display the group box which contains the username and password to receptionists.This is hidden from guests.
        private void rbnReceptionist_Click(object sender, EventArgs e)
        {
            gbxEmployeeDetails.Visible = true;
        }

        //Event handler to dsiplay the group box which contains the username
        private void btnClear_Click(object sender, EventArgs e)
        {
            clearData();
            clearErrors();
        }

        private void btnAdd_MouseHover(object sender, EventArgs e)
        {
            btnAdd.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnAdd.ForeColor = Color.Black;
        }

        private void btnAdd_MouseLeave(object sender, EventArgs e)
        {
            btnAdd.BackColor = Color.Black;
            btnAdd.ForeColor = Color.White;
        }

        private void btnClear_MouseHover(object sender, EventArgs e)
        {
            btnClear.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnClear.ForeColor = Color.Black;
        }

        private void btnClear_MouseLeave(object sender, EventArgs e)
        {
            btnClear.BackColor = Color.Black;
            btnClear.ForeColor = Color.White;
        }
    }
}
