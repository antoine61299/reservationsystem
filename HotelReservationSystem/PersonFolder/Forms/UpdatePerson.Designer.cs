﻿namespace HotelReservationSystem.PersonFolder
{
    partial class UpdatePerson
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdatePerson));
            this.pnlPersonDetails = new System.Windows.Forms.Panel();
            this.lblPersonDetails = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.gbxPersonDetails = new System.Windows.Forms.GroupBox();
            this.dtpDateOfBirth = new System.Windows.Forms.DateTimePicker();
            this.lblPersonName = new System.Windows.Forms.Label();
            this.cmbPersonId = new System.Windows.Forms.ComboBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblMobileNumber = new System.Windows.Forms.Label();
            this.rbnFemale = new System.Windows.Forms.RadioButton();
            this.lblSurname = new System.Windows.Forms.Label();
            this.lblDateOfBirth = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtMobileNumber = new System.Windows.Forms.TextBox();
            this.lblNationality = new System.Windows.Forms.Label();
            this.rbnMale = new System.Windows.Forms.RadioButton();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtNationality = new System.Windows.Forms.TextBox();
            this.lblGender = new System.Windows.Forms.Label();
            this.txtSurname = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.gbxEmployeeDetails = new System.Windows.Forms.GroupBox();
            this.lblUsername = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.gbxPersonType = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.rbnReceptionist = new System.Windows.Forms.RadioButton();
            this.rbnManager = new System.Windows.Forms.RadioButton();
            this.rbnGuest = new System.Windows.Forms.RadioButton();
            this.lblUpdatePerson = new System.Windows.Forms.Label();
            this.pnlUpdatePerson = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.erpName = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpSurname = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpDateOfBirth = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpGender = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpNationality = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpEmail = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpMobileNumber = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpPersonType = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpUsername = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpPassword = new System.Windows.Forms.ErrorProvider(this.components);
            this.erpPersonId = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlPersonDetails.SuspendLayout();
            this.gbxPersonDetails.SuspendLayout();
            this.gbxEmployeeDetails.SuspendLayout();
            this.gbxPersonType.SuspendLayout();
            this.pnlUpdatePerson.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpSurname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpDateOfBirth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpGender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpNationality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpMobileNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpPersonType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpUsername)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpPersonId)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlPersonDetails
            // 
            this.pnlPersonDetails.BackColor = System.Drawing.Color.FloralWhite;
            this.pnlPersonDetails.Controls.Add(this.lblPersonDetails);
            this.pnlPersonDetails.Controls.Add(this.btnClear);
            this.pnlPersonDetails.Controls.Add(this.gbxPersonDetails);
            this.pnlPersonDetails.Controls.Add(this.btnUpdate);
            this.pnlPersonDetails.Controls.Add(this.gbxEmployeeDetails);
            this.pnlPersonDetails.Controls.Add(this.gbxPersonType);
            this.pnlPersonDetails.Location = new System.Drawing.Point(0, 123);
            this.pnlPersonDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlPersonDetails.Name = "pnlPersonDetails";
            this.pnlPersonDetails.Size = new System.Drawing.Size(884, 548);
            this.pnlPersonDetails.TabIndex = 32;
            // 
            // lblPersonDetails
            // 
            this.lblPersonDetails.AutoSize = true;
            this.lblPersonDetails.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonDetails.Font = new System.Drawing.Font("Candara", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonDetails.Location = new System.Drawing.Point(252, 15);
            this.lblPersonDetails.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonDetails.Name = "lblPersonDetails";
            this.lblPersonDetails.Size = new System.Drawing.Size(400, 37);
            this.lblPersonDetails.TabIndex = 58;
            this.lblPersonDetails.Text = "Update an employee or guest";
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnClear.Font = new System.Drawing.Font("Candara", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(707, 463);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(155, 44);
            this.btnClear.TabIndex = 49;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            // 
            // gbxPersonDetails
            // 
            this.gbxPersonDetails.BackColor = System.Drawing.Color.Transparent;
            this.gbxPersonDetails.Controls.Add(this.dtpDateOfBirth);
            this.gbxPersonDetails.Controls.Add(this.lblPersonName);
            this.gbxPersonDetails.Controls.Add(this.cmbPersonId);
            this.gbxPersonDetails.Controls.Add(this.lblName);
            this.gbxPersonDetails.Controls.Add(this.lblMobileNumber);
            this.gbxPersonDetails.Controls.Add(this.rbnFemale);
            this.gbxPersonDetails.Controls.Add(this.lblSurname);
            this.gbxPersonDetails.Controls.Add(this.lblDateOfBirth);
            this.gbxPersonDetails.Controls.Add(this.txtName);
            this.gbxPersonDetails.Controls.Add(this.txtMobileNumber);
            this.gbxPersonDetails.Controls.Add(this.lblNationality);
            this.gbxPersonDetails.Controls.Add(this.rbnMale);
            this.gbxPersonDetails.Controls.Add(this.lblEmail);
            this.gbxPersonDetails.Controls.Add(this.txtNationality);
            this.gbxPersonDetails.Controls.Add(this.lblGender);
            this.gbxPersonDetails.Controls.Add(this.txtSurname);
            this.gbxPersonDetails.Controls.Add(this.txtEmail);
            this.gbxPersonDetails.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxPersonDetails.Location = new System.Drawing.Point(17, 64);
            this.gbxPersonDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxPersonDetails.Name = "gbxPersonDetails";
            this.gbxPersonDetails.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxPersonDetails.Size = new System.Drawing.Size(471, 446);
            this.gbxPersonDetails.TabIndex = 34;
            this.gbxPersonDetails.TabStop = false;
            this.gbxPersonDetails.Text = "Person Details";
            // 
            // dtpDateOfBirth
            // 
            this.dtpDateOfBirth.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.dtpDateOfBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.dtpDateOfBirth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDateOfBirth.Location = new System.Drawing.Point(181, 198);
            this.dtpDateOfBirth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpDateOfBirth.Name = "dtpDateOfBirth";
            this.dtpDateOfBirth.Size = new System.Drawing.Size(165, 23);
            this.dtpDateOfBirth.TabIndex = 50;
            // 
            // lblPersonName
            // 
            this.lblPersonName.AutoSize = true;
            this.lblPersonName.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonName.Location = new System.Drawing.Point(8, 41);
            this.lblPersonName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonName.Name = "lblPersonName";
            this.lblPersonName.Size = new System.Drawing.Size(94, 24);
            this.lblPersonName.TabIndex = 1;
            this.lblPersonName.Text = "Person Id:";
            // 
            // cmbPersonId
            // 
            this.cmbPersonId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPersonId.FormattingEnabled = true;
            this.cmbPersonId.Location = new System.Drawing.Point(181, 41);
            this.cmbPersonId.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbPersonId.Name = "cmbPersonId";
            this.cmbPersonId.Size = new System.Drawing.Size(165, 25);
            this.cmbPersonId.TabIndex = 0;
            this.cmbPersonId.DropDownClosed += new System.EventHandler(this.cmbPersonId_DropDownClosed);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(8, 91);
            this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(64, 24);
            this.lblName.TabIndex = 26;
            this.lblName.Text = "Name:";
            // 
            // lblMobileNumber
            // 
            this.lblMobileNumber.AutoSize = true;
            this.lblMobileNumber.BackColor = System.Drawing.Color.Transparent;
            this.lblMobileNumber.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMobileNumber.Location = new System.Drawing.Point(8, 398);
            this.lblMobileNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMobileNumber.Name = "lblMobileNumber";
            this.lblMobileNumber.Size = new System.Drawing.Size(145, 24);
            this.lblMobileNumber.TabIndex = 32;
            this.lblMobileNumber.Text = "Mobile Number:";
            // 
            // rbnFemale
            // 
            this.rbnFemale.AutoSize = true;
            this.rbnFemale.BackColor = System.Drawing.Color.Transparent;
            this.rbnFemale.Font = new System.Drawing.Font("Candara", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbnFemale.Location = new System.Drawing.Point(267, 298);
            this.rbnFemale.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbnFemale.Name = "rbnFemale";
            this.rbnFemale.Size = new System.Drawing.Size(73, 21);
            this.rbnFemale.TabIndex = 41;
            this.rbnFemale.TabStop = true;
            this.rbnFemale.Text = "Female";
            this.rbnFemale.UseVisualStyleBackColor = false;
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.BackColor = System.Drawing.Color.Transparent;
            this.lblSurname.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSurname.Location = new System.Drawing.Point(8, 145);
            this.lblSurname.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(90, 24);
            this.lblSurname.TabIndex = 27;
            this.lblSurname.Text = "Surname:";
            // 
            // lblDateOfBirth
            // 
            this.lblDateOfBirth.AutoSize = true;
            this.lblDateOfBirth.BackColor = System.Drawing.Color.Transparent;
            this.lblDateOfBirth.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateOfBirth.Location = new System.Drawing.Point(8, 197);
            this.lblDateOfBirth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateOfBirth.Name = "lblDateOfBirth";
            this.lblDateOfBirth.Size = new System.Drawing.Size(122, 24);
            this.lblDateOfBirth.TabIndex = 28;
            this.lblDateOfBirth.Text = "Date of Birth:";
            // 
            // txtName
            // 
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.Location = new System.Drawing.Point(181, 92);
            this.txtName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(165, 23);
            this.txtName.TabIndex = 39;
            // 
            // txtMobileNumber
            // 
            this.txtMobileNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMobileNumber.Location = new System.Drawing.Point(181, 399);
            this.txtMobileNumber.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtMobileNumber.Name = "txtMobileNumber";
            this.txtMobileNumber.Size = new System.Drawing.Size(165, 23);
            this.txtMobileNumber.TabIndex = 34;
            // 
            // lblNationality
            // 
            this.lblNationality.AutoSize = true;
            this.lblNationality.BackColor = System.Drawing.Color.Transparent;
            this.lblNationality.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNationality.Location = new System.Drawing.Point(8, 249);
            this.lblNationality.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNationality.Name = "lblNationality";
            this.lblNationality.Size = new System.Drawing.Size(106, 24);
            this.lblNationality.TabIndex = 30;
            this.lblNationality.Text = "Nationality:";
            // 
            // rbnMale
            // 
            this.rbnMale.AutoSize = true;
            this.rbnMale.BackColor = System.Drawing.Color.Transparent;
            this.rbnMale.Font = new System.Drawing.Font("Candara", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbnMale.Location = new System.Drawing.Point(181, 298);
            this.rbnMale.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbnMale.Name = "rbnMale";
            this.rbnMale.Size = new System.Drawing.Size(59, 21);
            this.rbnMale.TabIndex = 40;
            this.rbnMale.TabStop = true;
            this.rbnMale.Text = "Male";
            this.rbnMale.UseVisualStyleBackColor = false;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.BackColor = System.Drawing.Color.Transparent;
            this.lblEmail.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(8, 347);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(60, 24);
            this.lblEmail.TabIndex = 31;
            this.lblEmail.Text = "Email:";
            // 
            // txtNationality
            // 
            this.txtNationality.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNationality.Location = new System.Drawing.Point(181, 250);
            this.txtNationality.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNationality.Name = "txtNationality";
            this.txtNationality.Size = new System.Drawing.Size(165, 23);
            this.txtNationality.TabIndex = 36;
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.BackColor = System.Drawing.Color.Transparent;
            this.lblGender.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGender.Location = new System.Drawing.Point(8, 298);
            this.lblGender.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(76, 24);
            this.lblGender.TabIndex = 29;
            this.lblGender.Text = "Gender:";
            // 
            // txtSurname
            // 
            this.txtSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSurname.Location = new System.Drawing.Point(181, 145);
            this.txtSurname.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.Size = new System.Drawing.Size(165, 23);
            this.txtSurname.TabIndex = 38;
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Location = new System.Drawing.Point(181, 348);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(165, 23);
            this.txtEmail.TabIndex = 35;
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnUpdate.Font = new System.Drawing.Font("Candara", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(541, 463);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(155, 44);
            this.btnUpdate.TabIndex = 48;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // gbxEmployeeDetails
            // 
            this.gbxEmployeeDetails.BackColor = System.Drawing.Color.Transparent;
            this.gbxEmployeeDetails.Controls.Add(this.lblUsername);
            this.gbxEmployeeDetails.Controls.Add(this.lblPassword);
            this.gbxEmployeeDetails.Controls.Add(this.txtPassword);
            this.gbxEmployeeDetails.Controls.Add(this.txtUsername);
            this.gbxEmployeeDetails.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxEmployeeDetails.Location = new System.Drawing.Point(504, 63);
            this.gbxEmployeeDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxEmployeeDetails.Name = "gbxEmployeeDetails";
            this.gbxEmployeeDetails.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxEmployeeDetails.Size = new System.Drawing.Size(365, 140);
            this.gbxEmployeeDetails.TabIndex = 47;
            this.gbxEmployeeDetails.TabStop = false;
            this.gbxEmployeeDetails.Text = "New Username and Password";
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.BackColor = System.Drawing.Color.Transparent;
            this.lblUsername.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.Location = new System.Drawing.Point(8, 57);
            this.lblUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(101, 24);
            this.lblUsername.TabIndex = 8;
            this.lblUsername.Text = "Username:";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.BackColor = System.Drawing.Color.Transparent;
            this.lblPassword.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPassword.Location = new System.Drawing.Point(8, 101);
            this.lblPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(96, 24);
            this.lblPassword.TabIndex = 9;
            this.lblPassword.Text = "Password:";
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtPassword.Location = new System.Drawing.Point(123, 101);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(165, 23);
            this.txtPassword.TabIndex = 21;
            // 
            // txtUsername
            // 
            this.txtUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtUsername.Location = new System.Drawing.Point(123, 57);
            this.txtUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(165, 23);
            this.txtUsername.TabIndex = 13;
            // 
            // gbxPersonType
            // 
            this.gbxPersonType.Controls.Add(this.label2);
            this.gbxPersonType.Controls.Add(this.rbnReceptionist);
            this.gbxPersonType.Controls.Add(this.rbnManager);
            this.gbxPersonType.Controls.Add(this.rbnGuest);
            this.gbxPersonType.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxPersonType.Location = new System.Drawing.Point(496, 257);
            this.gbxPersonType.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxPersonType.Name = "gbxPersonType";
            this.gbxPersonType.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxPersonType.Size = new System.Drawing.Size(376, 98);
            this.gbxPersonType.TabIndex = 46;
            this.gbxPersonType.TabStop = false;
            this.gbxPersonType.Text = "Person Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 38);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 24);
            this.label2.TabIndex = 42;
            this.label2.Text = "Person Type:";
            // 
            // rbnReceptionist
            // 
            this.rbnReceptionist.AutoSize = true;
            this.rbnReceptionist.BackColor = System.Drawing.Color.Transparent;
            this.rbnReceptionist.Font = new System.Drawing.Font("Candara", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbnReceptionist.Location = new System.Drawing.Point(151, 70);
            this.rbnReceptionist.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbnReceptionist.Name = "rbnReceptionist";
            this.rbnReceptionist.Size = new System.Drawing.Size(103, 21);
            this.rbnReceptionist.TabIndex = 45;
            this.rbnReceptionist.TabStop = true;
            this.rbnReceptionist.Text = "Receptionist";
            this.rbnReceptionist.UseVisualStyleBackColor = false;
            this.rbnReceptionist.CheckedChanged += new System.EventHandler(this.rbnReceptionist_CheckedChanged);
            // 
            // rbnManager
            // 
            this.rbnManager.AutoSize = true;
            this.rbnManager.BackColor = System.Drawing.Color.Transparent;
            this.rbnManager.Font = new System.Drawing.Font("Candara", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbnManager.Location = new System.Drawing.Point(233, 42);
            this.rbnManager.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbnManager.Name = "rbnManager";
            this.rbnManager.Size = new System.Drawing.Size(83, 21);
            this.rbnManager.TabIndex = 44;
            this.rbnManager.TabStop = true;
            this.rbnManager.Text = "Manager";
            this.rbnManager.UseVisualStyleBackColor = false;
            this.rbnManager.Click += new System.EventHandler(this.rbnManager_Click);
            // 
            // rbnGuest
            // 
            this.rbnGuest.AutoSize = true;
            this.rbnGuest.BackColor = System.Drawing.Color.Transparent;
            this.rbnGuest.Font = new System.Drawing.Font("Candara", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbnGuest.Location = new System.Drawing.Point(149, 42);
            this.rbnGuest.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbnGuest.Name = "rbnGuest";
            this.rbnGuest.Size = new System.Drawing.Size(64, 21);
            this.rbnGuest.TabIndex = 43;
            this.rbnGuest.TabStop = true;
            this.rbnGuest.Text = "Guest";
            this.rbnGuest.UseVisualStyleBackColor = false;
            this.rbnGuest.Click += new System.EventHandler(this.rbnGuest_Click);
            // 
            // lblUpdatePerson
            // 
            this.lblUpdatePerson.AutoSize = true;
            this.lblUpdatePerson.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpdatePerson.ForeColor = System.Drawing.Color.White;
            this.lblUpdatePerson.Location = new System.Drawing.Point(581, 44);
            this.lblUpdatePerson.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUpdatePerson.Name = "lblUpdatePerson";
            this.lblUpdatePerson.Size = new System.Drawing.Size(281, 42);
            this.lblUpdatePerson.TabIndex = 3;
            this.lblUpdatePerson.Text = "Update Person";
            // 
            // pnlUpdatePerson
            // 
            this.pnlUpdatePerson.BackColor = System.Drawing.Color.DarkTurquoise;
            this.pnlUpdatePerson.Controls.Add(this.pictureBox1);
            this.pnlUpdatePerson.Controls.Add(this.lblUpdatePerson);
            this.pnlUpdatePerson.Location = new System.Drawing.Point(0, 0);
            this.pnlUpdatePerson.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlUpdatePerson.Name = "pnlUpdatePerson";
            this.pnlUpdatePerson.Size = new System.Drawing.Size(887, 123);
            this.pnlUpdatePerson.TabIndex = 31;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(16, 15);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(295, 97);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 43;
            this.pictureBox1.TabStop = false;
            // 
            // erpName
            // 
            this.erpName.ContainerControl = this;
            // 
            // erpSurname
            // 
            this.erpSurname.ContainerControl = this;
            // 
            // erpDateOfBirth
            // 
            this.erpDateOfBirth.ContainerControl = this;
            // 
            // erpGender
            // 
            this.erpGender.ContainerControl = this;
            // 
            // erpNationality
            // 
            this.erpNationality.ContainerControl = this;
            // 
            // erpEmail
            // 
            this.erpEmail.ContainerControl = this;
            // 
            // erpMobileNumber
            // 
            this.erpMobileNumber.ContainerControl = this;
            // 
            // erpPersonType
            // 
            this.erpPersonType.ContainerControl = this;
            // 
            // erpUsername
            // 
            this.erpUsername.ContainerControl = this;
            // 
            // erpPassword
            // 
            this.erpPassword.ContainerControl = this;
            // 
            // erpPersonId
            // 
            this.erpPersonId.ContainerControl = this;
            // 
            // UpdatePerson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 667);
            this.Controls.Add(this.pnlPersonDetails);
            this.Controls.Add(this.pnlUpdatePerson);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "UpdatePerson";
            this.Text = "Update Persons";
            this.pnlPersonDetails.ResumeLayout(false);
            this.pnlPersonDetails.PerformLayout();
            this.gbxPersonDetails.ResumeLayout(false);
            this.gbxPersonDetails.PerformLayout();
            this.gbxEmployeeDetails.ResumeLayout(false);
            this.gbxEmployeeDetails.PerformLayout();
            this.gbxPersonType.ResumeLayout(false);
            this.gbxPersonType.PerformLayout();
            this.pnlUpdatePerson.ResumeLayout(false);
            this.pnlUpdatePerson.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpSurname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpDateOfBirth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpGender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpNationality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpMobileNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpPersonType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpUsername)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erpPersonId)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlPersonDetails;
        private System.Windows.Forms.Label lblUpdatePerson;
        private System.Windows.Forms.Panel pnlUpdatePerson;
        private System.Windows.Forms.Label lblPersonName;
        private System.Windows.Forms.ComboBox cmbPersonId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblMobileNumber;
        private System.Windows.Forms.RadioButton rbnFemale;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.Label lblDateOfBirth;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtMobileNumber;
        private System.Windows.Forms.Label lblNationality;
        private System.Windows.Forms.RadioButton rbnMale;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox txtNationality;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.TextBox txtSurname;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.ErrorProvider erpName;
        private System.Windows.Forms.ErrorProvider erpSurname;
        private System.Windows.Forms.ErrorProvider erpDateOfBirth;
        private System.Windows.Forms.ErrorProvider erpGender;
        private System.Windows.Forms.ErrorProvider erpNationality;
        private System.Windows.Forms.ErrorProvider erpEmail;
        private System.Windows.Forms.ErrorProvider erpMobileNumber;
        private System.Windows.Forms.ErrorProvider erpPersonType;
        private System.Windows.Forms.ErrorProvider erpUsername;
        private System.Windows.Forms.ErrorProvider erpPassword;
        private System.Windows.Forms.ErrorProvider erpPersonId;
        private System.Windows.Forms.DateTimePicker dtpDateOfBirth;
        private System.Windows.Forms.Label lblPersonDetails;
        public System.Windows.Forms.GroupBox gbxPersonType;
        public System.Windows.Forms.GroupBox gbxEmployeeDetails;
        public System.Windows.Forms.GroupBox gbxPersonDetails;
        public System.Windows.Forms.RadioButton rbnManager;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.RadioButton rbnReceptionist;
        public System.Windows.Forms.RadioButton rbnGuest;
    }
}