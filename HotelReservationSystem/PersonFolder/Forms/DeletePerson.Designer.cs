﻿namespace HotelReservationSystem.PersonFolder
{
    partial class DeletePerson
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeletePerson));
            this.pnlPersonDetails = new System.Windows.Forms.Panel();
            this.lblPersonsDetails = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.dgvPersons = new System.Windows.Forms.DataGridView();
            this.PersonID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Surname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateOfBirth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Gender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nationality = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MobileNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlRemovePersons = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblRemovePerson = new System.Windows.Forms.Label();
            this.pnlPersonDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPersons)).BeginInit();
            this.pnlRemovePersons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlPersonDetails
            // 
            this.pnlPersonDetails.BackColor = System.Drawing.Color.FloralWhite;
            this.pnlPersonDetails.Controls.Add(this.lblPersonsDetails);
            this.pnlPersonDetails.Controls.Add(this.btnDelete);
            this.pnlPersonDetails.Controls.Add(this.dgvPersons);
            this.pnlPersonDetails.Location = new System.Drawing.Point(0, 123);
            this.pnlPersonDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlPersonDetails.Name = "pnlPersonDetails";
            this.pnlPersonDetails.Size = new System.Drawing.Size(1171, 433);
            this.pnlPersonDetails.TabIndex = 32;
            // 
            // lblPersonsDetails
            // 
            this.lblPersonsDetails.AutoSize = true;
            this.lblPersonsDetails.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonsDetails.Font = new System.Drawing.Font("Candara", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonsDetails.Location = new System.Drawing.Point(420, 17);
            this.lblPersonsDetails.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonsDetails.Name = "lblPersonsDetails";
            this.lblPersonsDetails.Size = new System.Drawing.Size(356, 37);
            this.lblPersonsDetails.TabIndex = 58;
            this.lblPersonsDetails.Text = "Choose a person to delete";
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnDelete.Font = new System.Drawing.Font("Candara", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Location = new System.Drawing.Point(993, 373);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(155, 44);
            this.btnDelete.TabIndex = 50;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            this.btnDelete.MouseLeave += new System.EventHandler(this.btnDelete_MouseLeave);
            this.btnDelete.MouseHover += new System.EventHandler(this.btnDelete_MouseHover);
            // 
            // dgvPersons
            // 
            this.dgvPersons.AllowUserToAddRows = false;
            this.dgvPersons.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPersons.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PersonID,
            this.Name,
            this.Surname,
            this.DateOfBirth,
            this.Gender,
            this.Nationality,
            this.Email,
            this.MobileNumber});
            this.dgvPersons.Location = new System.Drawing.Point(23, 80);
            this.dgvPersons.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvPersons.Name = "dgvPersons";
            this.dgvPersons.RowHeadersWidth = 51;
            this.dgvPersons.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPersons.Size = new System.Drawing.Size(1124, 170);
            this.dgvPersons.TabIndex = 1;
            // 
            // PersonID
            // 
            this.PersonID.HeaderText = "Person ID";
            this.PersonID.MinimumWidth = 6;
            this.PersonID.Name = "PersonID";
            this.PersonID.Width = 125;
            // 
            // Name
            // 
            this.Name.HeaderText = "Name";
            this.Name.MinimumWidth = 6;
            this.Name.Name = "Name";
            this.Name.Width = 125;
            // 
            // Surname
            // 
            this.Surname.HeaderText = "Surname";
            this.Surname.MinimumWidth = 6;
            this.Surname.Name = "Surname";
            this.Surname.Width = 125;
            // 
            // DateOfBirth
            // 
            this.DateOfBirth.HeaderText = "Date Of Birth";
            this.DateOfBirth.MinimumWidth = 6;
            this.DateOfBirth.Name = "DateOfBirth";
            this.DateOfBirth.Width = 125;
            // 
            // Gender
            // 
            this.Gender.HeaderText = "Gender";
            this.Gender.MinimumWidth = 6;
            this.Gender.Name = "Gender";
            this.Gender.Width = 125;
            // 
            // Nationality
            // 
            this.Nationality.HeaderText = "Nationality";
            this.Nationality.MinimumWidth = 6;
            this.Nationality.Name = "Nationality";
            this.Nationality.Width = 125;
            // 
            // Email
            // 
            this.Email.HeaderText = "Email";
            this.Email.MinimumWidth = 6;
            this.Email.Name = "Email";
            this.Email.Width = 125;
            // 
            // MobileNumber
            // 
            this.MobileNumber.HeaderText = "Mobile Number";
            this.MobileNumber.MinimumWidth = 6;
            this.MobileNumber.Name = "MobileNumber";
            this.MobileNumber.Width = 125;
            // 
            // pnlRemovePersons
            // 
            this.pnlRemovePersons.BackColor = System.Drawing.Color.DarkTurquoise;
            this.pnlRemovePersons.Controls.Add(this.pictureBox1);
            this.pnlRemovePersons.Controls.Add(this.lblRemovePerson);
            this.pnlRemovePersons.Location = new System.Drawing.Point(-51, -1);
            this.pnlRemovePersons.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlRemovePersons.Name = "pnlRemovePersons";
            this.pnlRemovePersons.Size = new System.Drawing.Size(1223, 123);
            this.pnlRemovePersons.TabIndex = 31;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(67, 16);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(295, 97);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 43;
            this.pictureBox1.TabStop = false;
            // 
            // lblRemovePerson
            // 
            this.lblRemovePerson.AutoSize = true;
            this.lblRemovePerson.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRemovePerson.ForeColor = System.Drawing.Color.White;
            this.lblRemovePerson.Location = new System.Drawing.Point(899, 43);
            this.lblRemovePerson.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRemovePerson.Name = "lblRemovePerson";
            this.lblRemovePerson.Size = new System.Drawing.Size(299, 42);
            this.lblRemovePerson.TabIndex = 3;
            this.lblRemovePerson.Text = "Remove Person";
            // 
            // DeletePerson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1171, 554);
            this.Controls.Add(this.pnlPersonDetails);
            this.Controls.Add(this.pnlRemovePersons);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "DeletePerson";
            this.Text = "Delete Persons";
            this.pnlPersonDetails.ResumeLayout(false);
            this.pnlPersonDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPersons)).EndInit();
            this.pnlRemovePersons.ResumeLayout(false);
            this.pnlRemovePersons.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnlPersonDetails;
        private System.Windows.Forms.Panel pnlRemovePersons;
        private System.Windows.Forms.Label lblRemovePerson;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lblPersonsDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn PersonID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Surname;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateOfBirth;
        private System.Windows.Forms.DataGridViewTextBoxColumn Gender;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nationality;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn MobileNumber;
        public System.Windows.Forms.DataGridView dgvPersons;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}