﻿using HotelReservationSystem.DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelReservationSystem.PersonFolder
{
    public partial class UpdatePerson : Form
    {
        public UpdatePerson()
        {
            InitializeComponent();
            disableElements();
        }

        public void clearData()
        {
            cmbPersonId.Text = "";
            txtName.Text = "";
            txtSurname.Text = "";
            txtNationality.Text = "";
            txtEmail.Text = "";
            txtMobileNumber.Text = "";
            txtUsername.Text = "";
            txtPassword.Text = "";
        }

        public void enableElements()
        {
            txtName.Enabled = true;
            txtSurname.Enabled = true;
            dtpDateOfBirth.Enabled = true;
            txtNationality.Enabled = true;
            rbnMale.Enabled = true;
            rbnFemale.Enabled = true;
            txtEmail.Enabled = true;
            txtMobileNumber.Enabled = true;
            txtUsername.Enabled = true;
            txtPassword.Enabled = true;
            btnUpdate.Visible = true;
            btnClear.Visible = true;
        }

        public void disableElements()
        {
            txtName.Enabled = false;
            txtSurname.Enabled = false;
            dtpDateOfBirth.Enabled = false;
            txtNationality.Enabled = false;
            rbnMale.Enabled = false;
            rbnFemale.Enabled = false;
            txtEmail.Enabled = false;
            txtMobileNumber.Enabled = false;
            txtUsername.Enabled = false;
            txtPassword.Enabled = false;
            btnUpdate.Visible = false;
            btnClear.Visible = false;
        }

        public Object getDataFromForm()
        {
            string name = txtName.Text;
            string surname = txtSurname.Text;
            string dob = dtpDateOfBirth.Text;
            string nationality = txtNationality.Text;
            string gender = "";

            //The value of the gender radio button will be assigned to the gender variable.
            if (rbnMale.Checked)
            {
                gender = rbnMale.Text;
            }
            else
            {
                gender = rbnFemale.Text;
            }
            string email = txtEmail.Text;
            string mobileNumber = txtMobileNumber.Text;
            string userName = txtUsername.Text;
            string password = txtPassword.Text;

            Guest guest;
            Receptionist receptionist;
            Manager manager;

            //The value of the  person type radio button will be assigned to the person type variable.
            if (rbnGuest.Checked)
            {
                guest = new Guest(name, surname, dob, nationality, gender, email, mobileNumber);
                return guest;

            }
            else if (rbnReceptionist.Checked)
            {
                receptionist = new Receptionist(name, surname, dob, nationality, gender, email, mobileNumber, userName, password);
                return receptionist;
            }
            else
            {
                manager = new Manager(name, surname, dob, nationality, gender, email, mobileNumber, userName, password);
                return manager;
            }
        }

        public void populatePersonIdComboBox(string personType)
        {
            GuestManager gm = new GuestManager();
            EmployeeManager em = new EmployeeManager();

            if (personType.Equals("Guest"))
            {
                List<int> guestList = gm.GetAllGuestId();
                foreach (int g in guestList)
                {
                    cmbPersonId.Items.Add(g);
                }
            }
            else if (personType.Equals("Receptionist"))
            {
                List<int> recList = em.GetAllReceptionistId();
                foreach (int r in recList)
                {
                    cmbPersonId.Items.Add(r);
                }
            }
            else
            {
                List<int> manList = em.GetAllManagerId();
                foreach (int m in manList)
                {
                    cmbPersonId.Items.Add(m);
                }
            }
        }

        //Elements in form are populated with the respective data according to the chosen room id.
        public void setElementsInForm(string personType)
        {
            GuestManager gm = new GuestManager();
            EmployeeManager em = new EmployeeManager();

            if (personType.Equals("Guest"))
            {
                Guest g = gm.GetSingle(int.Parse(cmbPersonId.SelectedItem.ToString()));
                txtName.Text = g.name;
                txtSurname.Text = g.surname;
                dtpDateOfBirth.Text = g.dateOfBirth;
                if (g.gender.Equals("Male"))
                {
                    rbnMale.Checked = true;
                    rbnFemale.Checked = false;
                }
                else
                {
                    rbnMale.Checked = false;
                    rbnFemale.Checked = true;
                }
                txtNationality.Text = g.nationality;
                txtEmail.Text = g.email;
                txtMobileNumber.Text = g.mobileNumber;
            }
            else if (personType.Equals("Receptionist"))
            {
                Receptionist r = em.GetSingleReceptionist(int.Parse(cmbPersonId.SelectedItem.ToString()));
                txtName.Text = r.name;
                txtSurname.Text = r.surname;
                dtpDateOfBirth.Text = r.dateOfBirth;
                if (r.gender.Equals("Male"))
                {
                    rbnMale.Checked = true;
                    rbnFemale.Checked = false;
                }
                else
                {
                    rbnMale.Checked = false;
                    rbnFemale.Checked = true;
                }
                txtNationality.Text = r.nationality;
                txtEmail.Text = r.email;
                txtMobileNumber.Text = r.mobileNumber;
                txtUsername.Text = r.username;
                txtPassword.Text = r.password;
            }
            else
            {
                Manager m = em.GetSingleManager(int.Parse(cmbPersonId.SelectedItem.ToString()));
                txtName.Text = m.name;
                txtSurname.Text = m.surname;
                dtpDateOfBirth.Text = m.dateOfBirth;
                if (m.gender.Equals("Male"))
                {
                    rbnMale.Checked = true;
                    rbnFemale.Checked = false;
                }
                else
                {
                    rbnMale.Checked = false;
                    rbnFemale.Checked = true;
                }
                txtNationality.Text = m.nationality;
                txtEmail.Text = m.email;
                txtMobileNumber.Text = m.mobileNumber;
                txtUsername.Text = m.username;
                txtPassword.Text = m.password;
            }
        }

        //Returns all the details of the Person validated.
        public bool validateInputs()
        {
            bool validated = true;

            if (cmbPersonId.Text == string.Empty)
            {
                erpPersonId.SetError(cmbPersonId, "Please select the person id");
                validated = false;
            }
            else
            {
                if (!int.TryParse(cmbPersonId.Text, out int numValue))
                {
                    erpPersonId.SetError(cmbPersonId, "Please select a valid person id");
                    validated = false;
                }
                else
                {
                    erpPersonId.Clear();
                }
            }

            if (txtName.Text == string.Empty)
            {
                erpName.SetError(txtName, "Please enter your name");
                validated = false;
            }
            else
            {
                if (int.TryParse(txtName.Text, out int numValue))
                {
                    erpName.SetError(txtName, "Only letters are accepted please try again");
                    validated = false;
                }
                else
                {
                    erpName.Clear();
                }
            }

            if (txtSurname.Text == string.Empty)
            {
                erpSurname.SetError(txtSurname, "Please enter your surname");
                validated = false;
            }
            else
            {
                if (int.TryParse(txtSurname.Text, out int numValue))
                {
                    erpSurname.SetError(txtSurname, "Only letters are accepted please try again");
                    validated = false;
                }
                else
                {
                    erpSurname.Clear();
                }
            }

            if (dtpDateOfBirth.Text == string.Empty)
            {
                erpDateOfBirth.SetError(dtpDateOfBirth, "Please enter your birth of date");
                validated = false;
            }
            else if (DateTime.Parse(dtpDateOfBirth.Text) > DateTime.Now)
            {
                erpDateOfBirth.SetError(dtpDateOfBirth, "Date is in the future! Please enter a valid date!");
                validated = false;
            }
            else
            {
                erpDateOfBirth.Clear();
            }

            if (txtNationality.Text == string.Empty)
            {
                erpNationality.SetError(txtNationality, "Please enter your nationality");
                validated = false;
            }
            else
            {
                if (int.TryParse(txtNationality.Text, out int numValue))
                {
                    erpNationality.SetError(txtNationality, "Only letters are accepted please try again");
                    validated = false;
                }
                else
                {
                    erpNationality.Clear();
                }
            }

            if (txtEmail.Text == string.Empty)
            {
                erpEmail.SetError(txtEmail, "Please enter your email");
                validated = false;
            }
            else
            {
                if (int.TryParse(txtEmail.Text, out int numValue))
                {
                    erpEmail.SetError(txtEmail, "Only letters and characters are accepted please try again");
                    validated = false;
                }
                else
                {
                    erpEmail.Clear();
                }
            }

            if (txtMobileNumber.Text == string.Empty)
            {
                erpMobileNumber.SetError(txtMobileNumber, "Please enter your mobile number");
                validated = false;
            }
            else
            {
                if (!int.TryParse(txtMobileNumber.Text, out int numValue))
                {
                    erpMobileNumber.SetError(txtMobileNumber, "Only numbers are accepted please try again");
                    validated = false;
                }
                else
                {
                    erpMobileNumber.Clear();
                }
            }

            if (rbnReceptionist.Checked || rbnManager.Checked)
            {
                if (txtUsername.Text == string.Empty)
                {
                    erpUsername.SetError(txtUsername, "Please enter your new username");
                    validated = false;
                }
                else
                {
                    erpUsername.Clear();
                }

                if (txtPassword.Text == string.Empty)
                {
                    erpPassword.SetError(txtPassword, "Please enter your new password");
                    validated = false;
                }
                else
                {
                    erpPassword.Clear();
                }
            }
            return validated;
        }

        private void rbnGuest_Click(object sender, EventArgs e)
        {
            clearData();
            cmbPersonId.Items.Clear();
            gbxPersonDetails.Enabled = true;
            gbxEmployeeDetails.Visible = false;
            populatePersonIdComboBox("Guest");
        }

        private void rbnManager_Click(object sender, EventArgs e)
        {
            clearData();
            cmbPersonId.Items.Clear();
            gbxEmployeeDetails.Visible = true;
            populatePersonIdComboBox("Manager");
        }

        private void rbnReceptionist_CheckedChanged(object sender, EventArgs e)
        {
            clearData();
            cmbPersonId.Items.Clear();
            populatePersonIdComboBox("Receptionist");
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (validateInputs())
            {
                GuestManager gm = new GuestManager();
                EmployeeManager em = new EmployeeManager();
                Object obj = getDataFromForm();
                if (obj.GetType().Name.Equals("Guest"))
                {
                    gm.Update(int.Parse(cmbPersonId.SelectedItem.ToString()), (Guest)obj);
                    MessageBox.Show("Guest is updated", "Guest", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (obj.GetType().Name.Equals("Receptionist"))
                {
                    em.UpdateReceptionist(int.Parse(cmbPersonId.SelectedItem.ToString()), (Receptionist)obj);
                    MessageBox.Show("Receptionist is updated", "Receptionist", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    em.UpdateManager(int.Parse(cmbPersonId.SelectedItem.ToString()), (Manager)obj);
                    MessageBox.Show("Manager is updated", "Manager", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }                
                clearData();
                disableElements();
            }
        }

        private void cmbPersonId_DropDownClosed(object sender, EventArgs e)
        {
            string selectedPerson = gbxPersonType.Controls.OfType<RadioButton>().FirstOrDefault(n => n.Checked).Text;
            setElementsInForm(selectedPerson);
            enableElements();
        }

        private void btnUpdate_MouseHover(object sender, EventArgs e)
        {
            btnUpdate.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnUpdate.ForeColor = Color.Black;

        }

        private void btnUpdate_MouseLeave(object sender, EventArgs e)
        {
            btnUpdate.BackColor = Color.Black;
            btnUpdate.ForeColor = Color.White;
        }

        private void btnClear_MouseHover(object sender, EventArgs e)
        {
            btnClear.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnClear.ForeColor = Color.Black;
        }

        private void btnClear_MouseLeave(object sender, EventArgs e)
        {
            btnClear.BackColor = Color.Black;
            btnClear.ForeColor = Color.White;
        }
    }
}


