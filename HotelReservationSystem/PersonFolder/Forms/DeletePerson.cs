﻿using HotelReservationSystem.DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelReservationSystem.PersonFolder
{
    public partial class DeletePerson : Form
    {
        string personType = "";

        public DeletePerson(string personType)
        {
            InitializeComponent();
            this.personType = personType;
            addDataToTable();
        }

        //Add data from database to the table in form. 
        public void addDataToTable()
        {
            GuestManager gm = new GuestManager();
            EmployeeManager em = new EmployeeManager();

            if (personType.Equals("Guest"))
            {
                List<Guest> guestList = gm.GetAll();
                foreach (Guest g in guestList)
                {
                    dgvPersons.Rows.Add(g.personId, g.name, g.surname, g.dateOfBirth, g.gender, g.nationality, g.email, g.mobileNumber);
                }
            }
            else if (personType.Equals("Receptionist"))
            {
                List<Receptionist> recList = em.GetAllReceptionist();
                foreach (Receptionist r in recList)
                {
                    dgvPersons.Rows.Add(r.personId, r.name, r.surname, r.dateOfBirth, r.gender, r.nationality, r.email, r.mobileNumber);
                }
            }
            else
            {
                List<Manager> manList = em.GetAllManager();
                foreach (Manager m in manList)
                {
                    dgvPersons.Rows.Add(m.personId, m.name, m.surname, m.dateOfBirth, m.gender, m.nationality, m.email, m.mobileNumber);
                }
            }
        }

        //Delete the selected record in the table form.
        public bool deleteDataInDatabase(int personId)
        {
            bool personIsBooked = false;

            BookingManager bm = new BookingManager();
            GuestManager gm = new GuestManager();
            EmployeeManager em = new EmployeeManager();

            List<int> guestBooked = bm.GetAllGuestId();

            if(guestBooked.Contains(personId))
            {
                personIsBooked = true;
            }

            if (!personIsBooked)
            {            
                if (personType.Equals("Guest"))
                {
                    gm.Delete(personId);
                }
                else
                {
                    em.Delete(personId);
                }
            }
            return personIsBooked;
        }

        // Event handler for when delete button is selected
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvPersons.SelectedCells.Count > 0)
            {
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result = MessageBox.Show("Are you sure you want to delete this record?", "Delete Record", buttons, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    int personId = int.Parse(dgvPersons.SelectedCells[0].Value.ToString());
                    bool isBooked = deleteDataInDatabase(personId);

                    if (!isBooked)
                    {
                        dgvPersons.Rows.RemoveAt(dgvPersons.SelectedCells[0].RowIndex);
                    }
                    else
                    {
                        MessageBox.Show("Person cannot be deleted because it was previosuly booked", "Room Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void btnDelete_MouseHover(object sender, EventArgs e)
        {
            btnDelete.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnDelete.ForeColor = Color.Black;
        }

        private void btnDelete_MouseLeave(object sender, EventArgs e)
        {
            btnDelete.BackColor = Color.Black;
            btnDelete.ForeColor = Color.White;
        }
    }
}

