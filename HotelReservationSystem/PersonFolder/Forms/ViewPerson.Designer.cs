﻿namespace HotelReservationSystem.PersonFolder
{
    partial class ViewPerson
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewPerson));
            this.lblViewPerson = new System.Windows.Forms.Label();
            this.pnlViewPerson = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlRoomDetails = new System.Windows.Forms.Panel();
            this.lblPersonDetails = new System.Windows.Forms.Label();
            this.dgvPersons = new System.Windows.Forms.DataGridView();
            this.GuestID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Surname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateOfBirth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Gender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nationality = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MobileNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlViewPerson.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlRoomDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPersons)).BeginInit();
            this.SuspendLayout();
            // 
            // lblViewPerson
            // 
            this.lblViewPerson.AutoSize = true;
            this.lblViewPerson.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblViewPerson.ForeColor = System.Drawing.Color.White;
            this.lblViewPerson.Location = new System.Drawing.Point(909, 46);
            this.lblViewPerson.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblViewPerson.Name = "lblViewPerson";
            this.lblViewPerson.Size = new System.Drawing.Size(239, 42);
            this.lblViewPerson.TabIndex = 3;
            this.lblViewPerson.Text = "View Person";
            // 
            // pnlViewPerson
            // 
            this.pnlViewPerson.BackColor = System.Drawing.Color.DarkTurquoise;
            this.pnlViewPerson.Controls.Add(this.pictureBox1);
            this.pnlViewPerson.Controls.Add(this.lblViewPerson);
            this.pnlViewPerson.Location = new System.Drawing.Point(0, 0);
            this.pnlViewPerson.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlViewPerson.Name = "pnlViewPerson";
            this.pnlViewPerson.Size = new System.Drawing.Size(1171, 123);
            this.pnlViewPerson.TabIndex = 29;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(16, 15);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(295, 97);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 44;
            this.pictureBox1.TabStop = false;
            // 
            // pnlRoomDetails
            // 
            this.pnlRoomDetails.BackColor = System.Drawing.Color.FloralWhite;
            this.pnlRoomDetails.Controls.Add(this.lblPersonDetails);
            this.pnlRoomDetails.Controls.Add(this.dgvPersons);
            this.pnlRoomDetails.Location = new System.Drawing.Point(0, 123);
            this.pnlRoomDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlRoomDetails.Name = "pnlRoomDetails";
            this.pnlRoomDetails.Size = new System.Drawing.Size(1171, 433);
            this.pnlRoomDetails.TabIndex = 30;
            // 
            // lblPersonDetails
            // 
            this.lblPersonDetails.AutoSize = true;
            this.lblPersonDetails.BackColor = System.Drawing.Color.Transparent;
            this.lblPersonDetails.Font = new System.Drawing.Font("Candara", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPersonDetails.Location = new System.Drawing.Point(409, 23);
            this.lblPersonDetails.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPersonDetails.Name = "lblPersonDetails";
            this.lblPersonDetails.Size = new System.Drawing.Size(379, 37);
            this.lblPersonDetails.TabIndex = 58;
            this.lblPersonDetails.Text = "Persons listed in the system";
            // 
            // dgvPersons
            // 
            this.dgvPersons.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPersons.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GuestID,
            this.Name,
            this.Surname,
            this.DateOfBirth,
            this.Gender,
            this.Nationality,
            this.Email,
            this.MobileNumber});
            this.dgvPersons.Location = new System.Drawing.Point(24, 80);
            this.dgvPersons.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvPersons.Name = "dgvPersons";
            this.dgvPersons.RowHeadersWidth = 51;
            this.dgvPersons.Size = new System.Drawing.Size(1124, 271);
            this.dgvPersons.TabIndex = 0;
            this.dgvPersons.SelectionChanged += new System.EventHandler(this.dgvPersons_SelectionChanged);
            // 
            // GuestID
            // 
            this.GuestID.HeaderText = "Person ID";
            this.GuestID.MinimumWidth = 6;
            this.GuestID.Name = "GuestID";
            this.GuestID.Width = 125;
            // 
            // Name
            // 
            this.Name.HeaderText = "Name";
            this.Name.MinimumWidth = 6;
            this.Name.Name = "Name";
            this.Name.Width = 125;
            // 
            // Surname
            // 
            this.Surname.HeaderText = "Surname";
            this.Surname.MinimumWidth = 6;
            this.Surname.Name = "Surname";
            this.Surname.Width = 125;
            // 
            // DateOfBirth
            // 
            this.DateOfBirth.HeaderText = "Date Of Birth";
            this.DateOfBirth.MinimumWidth = 6;
            this.DateOfBirth.Name = "DateOfBirth";
            this.DateOfBirth.Width = 125;
            // 
            // Gender
            // 
            this.Gender.HeaderText = "Gender";
            this.Gender.MinimumWidth = 6;
            this.Gender.Name = "Gender";
            this.Gender.Width = 125;
            // 
            // Nationality
            // 
            this.Nationality.HeaderText = "Nationality";
            this.Nationality.MinimumWidth = 6;
            this.Nationality.Name = "Nationality";
            this.Nationality.Width = 125;
            // 
            // Email
            // 
            this.Email.HeaderText = "Email";
            this.Email.MinimumWidth = 6;
            this.Email.Name = "Email";
            this.Email.Width = 125;
            // 
            // MobileNumber
            // 
            this.MobileNumber.HeaderText = "Mobile Number";
            this.MobileNumber.MinimumWidth = 6;
            this.MobileNumber.Name = "MobileNumber";
            this.MobileNumber.Width = 125;
            // 
            // ViewPerson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1171, 554);
            this.Controls.Add(this.pnlRoomDetails);
            this.Controls.Add(this.pnlViewPerson);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ViewPerson";
            this.Text = "View Persons";
            this.pnlViewPerson.ResumeLayout(false);
            this.pnlViewPerson.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlRoomDetails.ResumeLayout(false);
            this.pnlRoomDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPersons)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblViewPerson;
        private System.Windows.Forms.Panel pnlViewPerson;
        private System.Windows.Forms.Panel pnlRoomDetails;
        private System.Windows.Forms.DataGridView dgvPersons;
        private System.Windows.Forms.Label lblPersonDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn GuestID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Surname;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateOfBirth;
        private System.Windows.Forms.DataGridViewTextBoxColumn Gender;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nationality;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn MobileNumber;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}