﻿using HotelReservationSystem.DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelReservationSystem.PersonFolder
{
    public partial class ViewPerson : Form
    {
        public ViewPerson(string personType)
        {
            InitializeComponent();
            addDataToTable(personType);
        }

        // Populate table in form with data from database.
        public void addDataToTable(string personType)
        {
            GuestManager gm = new GuestManager();
            EmployeeManager em = new EmployeeManager();

            if(personType.Equals("Guest"))
            {
                List<Guest> guestList = gm.GetAll();
                foreach(Guest g in guestList)
                {
                    dgvPersons.Rows.Add(g.personId , g.name, g.surname, g.dateOfBirth, g.gender, g.nationality, g.email, g.mobileNumber);
                }
            }
            else if(personType.Equals("Receptionist"))
            {
                List<Receptionist> recList = em.GetAllReceptionist();
                foreach (Receptionist r in recList)
                {
                    dgvPersons.Rows.Add(r.personId, r.name, r.surname, r.dateOfBirth, r.gender, r.nationality, r.email, r.mobileNumber);
                }
            }
            else
            {
                List<Manager> manList = em.GetAllManager();
                foreach (Manager m in manList)
                {
                    dgvPersons.Rows.Add(m.personId, m.name, m.surname, m.dateOfBirth, m.gender, m.nationality, m.email, m.mobileNumber);                    
                }
            }
            //dgvPersons.Sort(dgvPersons.Columns[1], ListSortDirection.Ascending);
        }

        private void dgvPersons_SelectionChanged(object sender, EventArgs e)
        {
            dgvPersons.ClearSelection();
        }
    }
}
