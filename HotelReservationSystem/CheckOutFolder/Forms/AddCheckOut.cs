﻿using HotelReservationSystem.DataLayer;
using HotelReservationSystem.Other;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelReservationSystem.CheckOutFolder
{
    public partial class AddCheckOut : Form
    {
        public AddCheckOut()
        {
            InitializeComponent();
            populateBookingNumberComboBox();
            disableElements();

            if (cmbBookingNumber.Items.Count == 0)
            {
                cmbBookingNumber.Enabled = false;
                disableElements();
            }
        }

        public void clearData()
        {
            lblGuestIDValue.Text = "";
            lblNumberOfPersonsValue.Text = "";
            lblArrivalDateValue.Text = "";
            lblArrivalDateValue.Text = "";
            lblDepartureDateValue.Text = "";
            lblNumberOfNightsValue.Text = "";
            dgvBookedRooms.Rows.Clear();
        }

        public void enableElements()
        {
            lblBookedRooms.Enabled = true;
            lblDiscountAndBill.Enabled = true;
            dgvBookedRooms.Enabled = true;
            grpDisocuntAndBill.Enabled = true;
            btnCheckOut.Visible = true;
            btnClear.Visible = true;
            btnCalculate.Visible = true;
        }

        public void disableElements()
        {
            lblBookedRooms.Enabled = false;
            lblDiscountAndBill.Enabled = false;
            dgvBookedRooms.Enabled = false;
            grpDisocuntAndBill.Enabled = false;
            btnCheckOut.Visible = false;
            btnClear.Visible = false;
            btnCalculate.Visible = false;
        }

        public void populateBookingNumberComboBox()
        {
            BookingManager bm = new BookingManager();
            List<int> bnList = bm.GetCheckedInBookingNo();

            foreach(int bn in bnList)
            {
                cmbBookingNumber.Items.Add(bn);
            }
        }

        public List<double> getTotalRoomPrice()
        {
            List<double> priceList = new List<double>();

            BookingManager bm = new BookingManager();
            List<ReservationView> rvList = bm.GetView(int.Parse(cmbBookingNumber.SelectedItem.ToString()));

            foreach(ReservationView rv in rvList)
            {
                priceList.Add(double.Parse(rv.price));
            }

            return priceList;
        }

        public void setElementsInForm()
        {
            dgvBookedRooms.Rows.Clear();
            enableElements();

            List<double> price = new List<double>();

            BookingManager bm = new BookingManager();

            List<ReservationView> rvList = bm.GetView(int.Parse(cmbBookingNumber.SelectedItem.ToString()));

            foreach (ReservationView rv in rvList)
            {
                lblGuestIDValue.Text = rv.guestId;
                lblNumberOfPersonsValue.Text = rv.noOfPersons;
                lblDepartureDateValue.Text = rv.departure;
                lblArrivalDateValue.Text = rv.arrival;
                lblNumberOfNightsValue.Text = rv.noOfNights;
                lblPersonName.Text = rv.personName;
                lblPersonSurname.Text = rv.personSurname;
                lblPersonDateOfBirth.Text = rv.personDob;
                lblPersonGender.Text = rv.personGender;
                lblPersonNationality.Text = rv.nationality;
                lblPersonEmail.Text = rv.email;
                lblPersonMobileNumber.Text = rv.mobile;
                dgvBookedRooms.Rows.Add(rv.roomNumber, rv.roomType, rv.bedType, rv.price);
                price.Add(int.Parse(rv.price));
            }

            lblTotalBillValue.Text = calculatingTotalBill(price, 0).ToString();
        }

        public double calculatingTotalBill(List<double> roomPrices, double discount)
        {
            double total = 0;

            foreach(double price in roomPrices)
            {
                total += price;
            }

            total = ((100 - discount) * total) / 100;

            return total;
        }

        //Returns all the details of the room validated.
        public bool validateInputs()
        {
            bool validated = true;            

            if (txtDiscount.Text == string.Empty)
            {
                erpDiscount.SetError(txtDiscount, "Please enter a room number");
                validated = false;
            }
            else
            {
                if (!double.TryParse(txtDiscount.Text, out double numValue))
                {
                    erpDiscount.SetError(txtDiscount, "Please enter a number");
                    validated = false;
                }
                else
                {
                    if(double.Parse(txtDiscount.Text) < 0)
                    {
                        erpDiscount.SetError(txtDiscount, "Discount must be greater than or equal to 0!");
                        validated = false;
                    }
                    else
                    {
                        erpDiscount.Clear();
                    }                    
                }
            }
            return validated;
        }

        private void cmbBookingNumber_DropDownClosed(object sender, EventArgs e)
        {
            setElementsInForm();
            txtDiscount.Enabled = true;
            if (cmbBookingNumber.SelectedItem != null)
            {
                setElementsInForm();
            }
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            if (validateInputs())
            {
                lblTotalBillValue.Text = calculatingTotalBill(getTotalRoomPrice(), double.Parse(txtDiscount.Text)).ToString();
            }
        }

        private void btnCheckOut_Click(object sender, EventArgs e)
        {
            BookingManager bm = new BookingManager();
            bm.UpdateStatusCheckOut(int.Parse(cmbBookingNumber.SelectedItem.ToString()));
            disableElements();
            clearData();
            cmbBookingNumber.Items.Remove(cmbBookingNumber.SelectedItem);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clearData();
            disableElements();
        }

        private void btnCalculate_MouseHover(object sender, EventArgs e)
        {
            btnCalculate.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnCalculate.ForeColor = Color.Black;
        }

        private void btnCalculate_MouseLeave(object sender, EventArgs e)
        {
            btnCalculate.BackColor = Color.Black;
            btnCalculate.ForeColor = Color.White;
        }

        private void btnCheckOut_MouseHover(object sender, EventArgs e)
        {
            btnCheckOut.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnCheckOut.ForeColor = Color.Black;
        }

        private void btnCheckOut_MouseLeave(object sender, EventArgs e)
        {
            btnCheckOut.BackColor = Color.Black;
            btnCheckOut.ForeColor = Color.White;
        }

        private void btnClear_MouseHover(object sender, EventArgs e)
        {
            btnClear.BackColor = ColorTranslator.FromHtml("#E0FFFF");
            btnClear.ForeColor = Color.Black;
        }

        private void btnClear_MouseLeave(object sender, EventArgs e)
        {
            btnClear.BackColor = Color.Black;
            btnClear.ForeColor = Color.White;
        }
    }
}
