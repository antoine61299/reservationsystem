﻿namespace HotelReservationSystem.CheckOutFolder
{
    partial class ViewCheckOut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewCheckOut));
            this.pnlRoomDetails = new System.Windows.Forms.Panel();
            this.dgvCheckedOutRooms = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RoomNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblCheckedOutRooms = new System.Windows.Forms.Label();
            this.lblViewCheckOut = new System.Windows.Forms.Label();
            this.pnlViewCheckOut = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlRoomDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCheckedOutRooms)).BeginInit();
            this.pnlViewCheckOut.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlRoomDetails
            // 
            this.pnlRoomDetails.BackColor = System.Drawing.Color.FloralWhite;
            this.pnlRoomDetails.Controls.Add(this.dgvCheckedOutRooms);
            this.pnlRoomDetails.Controls.Add(this.lblCheckedOutRooms);
            this.pnlRoomDetails.Location = new System.Drawing.Point(1, 123);
            this.pnlRoomDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlRoomDetails.Name = "pnlRoomDetails";
            this.pnlRoomDetails.Size = new System.Drawing.Size(1169, 702);
            this.pnlRoomDetails.TabIndex = 43;
            // 
            // dgvCheckedOutRooms
            // 
            this.dgvCheckedOutRooms.AllowUserToAddRows = false;
            this.dgvCheckedOutRooms.BackgroundColor = System.Drawing.SystemColors.WindowFrame;
            this.dgvCheckedOutRooms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCheckedOutRooms.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.RoomNumber});
            this.dgvCheckedOutRooms.Location = new System.Drawing.Point(49, 78);
            this.dgvCheckedOutRooms.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvCheckedOutRooms.Name = "dgvCheckedOutRooms";
            this.dgvCheckedOutRooms.ReadOnly = true;
            this.dgvCheckedOutRooms.RowHeadersWidth = 5;
            this.dgvCheckedOutRooms.Size = new System.Drawing.Size(1057, 585);
            this.dgvCheckedOutRooms.TabIndex = 59;
            this.dgvCheckedOutRooms.SelectionChanged += new System.EventHandler(this.dgvCheckedOutRooms_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Booking Number";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 110;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Guest ID";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 75;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Number Of Persons";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 125;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Number Of Nights";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 125;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Arrival Date";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 125;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DividerWidth = 2;
            this.dataGridViewTextBoxColumn7.HeaderText = "Departure Date";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 125;
            // 
            // RoomNumber
            // 
            this.RoomNumber.HeaderText = "Room Number";
            this.RoomNumber.MinimumWidth = 6;
            this.RoomNumber.Name = "RoomNumber";
            this.RoomNumber.ReadOnly = true;
            this.RoomNumber.Width = 125;
            // 
            // lblCheckedOutRooms
            // 
            this.lblCheckedOutRooms.AutoSize = true;
            this.lblCheckedOutRooms.BackColor = System.Drawing.Color.Transparent;
            this.lblCheckedOutRooms.Font = new System.Drawing.Font("Candara", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckedOutRooms.Location = new System.Drawing.Point(472, 21);
            this.lblCheckedOutRooms.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCheckedOutRooms.Name = "lblCheckedOutRooms";
            this.lblCheckedOutRooms.Size = new System.Drawing.Size(278, 37);
            this.lblCheckedOutRooms.TabIndex = 57;
            this.lblCheckedOutRooms.Text = "Checked Out Rooms";
            // 
            // lblViewCheckOut
            // 
            this.lblViewCheckOut.AutoSize = true;
            this.lblViewCheckOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblViewCheckOut.ForeColor = System.Drawing.Color.White;
            this.lblViewCheckOut.Location = new System.Drawing.Point(807, 41);
            this.lblViewCheckOut.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblViewCheckOut.Name = "lblViewCheckOut";
            this.lblViewCheckOut.Size = new System.Drawing.Size(300, 42);
            this.lblViewCheckOut.TabIndex = 3;
            this.lblViewCheckOut.Text = "View Check Out";
            // 
            // pnlViewCheckOut
            // 
            this.pnlViewCheckOut.BackColor = System.Drawing.Color.DarkTurquoise;
            this.pnlViewCheckOut.Controls.Add(this.pictureBox1);
            this.pnlViewCheckOut.Controls.Add(this.lblViewCheckOut);
            this.pnlViewCheckOut.Location = new System.Drawing.Point(0, 0);
            this.pnlViewCheckOut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlViewCheckOut.Name = "pnlViewCheckOut";
            this.pnlViewCheckOut.Size = new System.Drawing.Size(1175, 123);
            this.pnlViewCheckOut.TabIndex = 42;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(16, 15);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(295, 97);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 60;
            this.pictureBox1.TabStop = false;
            // 
            // ViewCheckOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1168, 821);
            this.Controls.Add(this.pnlRoomDetails);
            this.Controls.Add(this.pnlViewCheckOut);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ViewCheckOut";
            this.Text = "ViewCheck Out";
            this.pnlRoomDetails.ResumeLayout(false);
            this.pnlRoomDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCheckedOutRooms)).EndInit();
            this.pnlViewCheckOut.ResumeLayout(false);
            this.pnlViewCheckOut.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlRoomDetails;
        private System.Windows.Forms.Label lblCheckedOutRooms;
        private System.Windows.Forms.Label lblViewCheckOut;
        private System.Windows.Forms.Panel pnlViewCheckOut;
        private System.Windows.Forms.DataGridView dgvCheckedOutRooms;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoomNumber;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}