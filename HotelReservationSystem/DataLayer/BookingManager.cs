﻿using HotelReservationSystem.Other;
using HotelReservationSystem.ReservationFolder;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelReservationSystem.DataLayer
{
    class BookingManager : DataManager
    {
        public List<Booking> GetAll()
        {
            List<Booking> dbBooking = new List<Booking>();
            SqlConnection connection = GetDbConnection();
            string sql = "SELECT * FROM[dbo].[Booking]";
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                Booking b = new Booking(int.Parse(dr[0].ToString()), DateTime.Parse(dr[1].ToString()), DateTime.Parse(dr[2].ToString()), int.Parse(dr[3].ToString()), int.Parse(dr[4].ToString()), int.Parse(dr[5].ToString()), dr[6].ToString(), double.Parse(dr[7].ToString()), double.Parse(dr[8].ToString()), null);
                dbBooking.Add(b);
            }

            dr.Close();
            connection.Close();

            return dbBooking;
        }

        public List<int> GetAllGuestId()
        {
            List<int> dbGuestId = new List<int>();
            SqlConnection connection = GetDbConnection();
            string sql = "SELECT GuestID FROM[dbo].[Booking]";
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                dbGuestId.Add(int.Parse(dr[0].ToString()));
            }

            dr.Close();
            connection.Close();

            return dbGuestId;
        }

        public List<int> GetReservedBookingNo()
        {
            List<int> dbBookingNo = new List<int>();
            SqlConnection connection = GetDbConnection();
            SqlCommand cmd = new SqlCommand("SELECT BookingNo FROM [dbo].[Booking] WHERE Status = 'Reserved'", connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                dbBookingNo.Add(int.Parse(dr[0].ToString()));
            }

            dr.Close();
            connection.Close();
            return dbBookingNo;
        }

        public List<int> GetCheckedInBookingNo()
        {
            List<int> dbBookingNo = new List<int>();
            SqlConnection connection = GetDbConnection();
            SqlCommand cmd = new SqlCommand("SELECT BookingNo FROM [dbo].[Booking] WHERE Status = 'Checked In'", connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                dbBookingNo.Add(int.Parse(dr[0].ToString()));
            }

            dr.Close();
            connection.Close();
            return dbBookingNo;
        }

        public void Insert(Booking booking)
        {
            SqlConnection connection = GetDbConnection();
            string sql = "INSERT INTO [dbo].[Booking] VALUES ('" + booking.arrivalDate.ToString() + "','" + booking.departureDate.ToString() + "'," + booking.numberOfNights + "," + booking.numberOfPersons + "," + booking.guestID + ",'" + booking.status + "'," + booking.discount + "," + booking.bill + ")";
            string getPrimaryKey = "Select @@Identity";
            int bookingNo;
            SqlCommand cmdinsert = new SqlCommand(sql, connection);
            connection.Open();
            cmdinsert.CommandType = CommandType.Text;
            cmdinsert.ExecuteNonQuery();
            cmdinsert.CommandText = getPrimaryKey;
            bookingNo = Convert.ToInt32(cmdinsert.ExecuteScalar());
            connection.Close();
            connection.Open();
            foreach (int row in booking.rooms)
            {
                string retriveRoomId = "SELECT RoomId FROM Room WHERE RoomNo = " + row;
                string bookingRoomSql = "INSERT INTO [dbo].[BookingRoom] VALUES (" + bookingNo + ",(" + retriveRoomId + "))";
                SqlCommand cmdBookingRoomInsert = new SqlCommand(bookingRoomSql, connection);
                cmdBookingRoomInsert.CommandType = CommandType.Text;
                cmdBookingRoomInsert.ExecuteNonQuery();
            }
            connection.Close();
        }


        public List<ReservationView> GetView(int bookingNo)
        {
            List<ReservationView> rvList = new List<ReservationView>();
            SqlConnection connection = GetDbConnection();
            string sql = "SELECT * FROM Booking b" +
                              " INNER JOIN Guest g on b.GuestID = g.GuestID" +
                              " INNER JOIN BookingRoom br on b.BookingNo = br.BookingNo" +
                              " INNER JOIN Room r on br.RoomID = r.RoomID" +
                              " WHERE b.BookingNo = " + bookingNo;

            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                ReservationView rv = new ReservationView(bookingNo.ToString(), dr[5].ToString(), dr[4].ToString(), dr[3].ToString(), dr[1].ToString(), dr[2].ToString(), dr[10].ToString(), dr[11].ToString(), dr[12].ToString(), dr[13].ToString(), dr[14].ToString(), dr[15].ToString(), dr[16].ToString(), dr[24].ToString(), dr[21].ToString(), dr[22].ToString(), dr[23].ToString());
                rvList.Add(rv);
            }

            
            dr.Close();
            connection.Close();

            return rvList;
        }

        public void Update(int bookingNo, Booking booking)
        {
            SqlConnection connection = GetDbConnection();
            string sql = "UPDATE [dbo].[Booking] SET ArrivalDate = '" + booking.arrivalDate.ToString() + "', DepartureDate = '" + booking.departureDate.ToString() + "', NumberOfNights = " + booking.numberOfNights + ",NumberOfPersons = " + booking.numberOfPersons + ",Status = '" + booking.status + "',Discount = " + booking.discount + ", Bill = " + booking.bill + " WHERE BookingNo =" + bookingNo;

            SqlCommand cmdinsert = new SqlCommand(sql, connection);
            connection.Open();
            cmdinsert.CommandType = CommandType.Text;
            cmdinsert.ExecuteNonQuery();
            connection.Close();

            connection.Open();
            string deleteFromBookingRoom = "DELETE FROM BookingRoom WHERE BookingNo = " + bookingNo + ";";
            string resultingSql = deleteFromBookingRoom;
            foreach (int row in booking.rooms)
            {
                string retriveRoomId = "SELECT RoomId FROM Room WHERE RoomNo = " + row;
                string bookingRoomSql = "INSERT INTO [dbo].[BookingRoom] VALUES (" + bookingNo + ",(" + retriveRoomId + "));";
                resultingSql += bookingRoomSql;
            }
            SqlCommand cmdBookingRoomInsert = new SqlCommand(resultingSql, connection);
            cmdBookingRoomInsert.CommandType = CommandType.Text;
            cmdBookingRoomInsert.ExecuteNonQuery();
            connection.Close();
        }

        public void UpdateStatusCheckIn(int bookingNo)
        {
            SqlConnection connection = GetDbConnection();
            string sql = "UPDATE [dbo].[Booking] SET Status = 'Checked In' WHERE BookingNo = " + bookingNo;
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        public void UpdateStatusCheckOut(int bookingNo)
        {
            SqlConnection connection = GetDbConnection();
            string sql = "UPDATE [dbo].[Booking] SET Status = 'Checked Out' WHERE BookingNo = " + bookingNo;
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
            connection.Close();
        }
    }
}
