﻿using HotelReservationSystem.RoomFolder;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelReservationSystem.DataLayer
{
    class RoomManager : DataManager
    {
        public void Insert(Room room)
        {
            SqlConnection connection = GetDbConnection();
            string sql = "INSERT INTO [dbo].[Room] VALUES ('" + room.roomType + "','" + room.bedType + "'," + room.price + "," + room.roomNumber + ")";
            SqlCommand cmdinsert = new SqlCommand(sql, connection);
            connection.Open();
            cmdinsert.CommandType = CommandType.Text;
            cmdinsert.ExecuteNonQuery();
            connection.Close();
        }

        public bool FindRoom(int roomNumber)
        {
            bool roomExists = false;
            SqlConnection connection = GetDbConnection();
            SqlCommand cmd = new SqlCommand("SELECT RoomNo FROM [dbo].[Room]", connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                if (roomNumber == int.Parse(dr[0].ToString()))
                {
                    roomExists = true;
                }
            }
            dr.Close();
            connection.Close();
            return roomExists;
        }

        public List<Room> GetAll()
        {
            List<Room> dbRooms = new List<Room>();
            SqlConnection connection = GetDbConnection();
            SqlCommand cmd = new SqlCommand("SELECT * FROM [dbo].[Room]", connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                Room r = new Room(int.Parse(dr[0].ToString()), int.Parse(dr[4].ToString()), dr[1].ToString(), dr[2].ToString(), float.Parse(dr[3].ToString()));
                dbRooms.Add(r);
            }
            dr.Close();
            connection.Close();

            return dbRooms;
        }

        public List<int> GetAllRoomId()
        {
            List<int> dbRoomId = new List<int>();
            SqlConnection connection = GetDbConnection();
            SqlCommand cmd = new SqlCommand("SELECT RoomID FROM [dbo].[Room]", connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                dbRoomId.Add(int.Parse(dr[0].ToString()));
            }

            dr.Close();
            connection.Close();

            return dbRoomId;
        }

        public Room GetSingle(int roomId)
        {
            Room r = null;
            SqlConnection connection = GetDbConnection();
            SqlCommand cmd = new SqlCommand("SELECT * FROM [dbo].[Room] WHERE RoomID = " + roomId, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                r = new Room(int.Parse(dr[4].ToString()), dr[1].ToString(), dr[2].ToString(), float.Parse(dr[3].ToString()));
            }

            dr.Close();
            connection.Close();

            return r;
        }

        public void Update(int roomId, Room room)
        {
            SqlConnection connection = GetDbConnection();
            string sql = "UPDATE [dbo].[Room] SET Type = '" + room.roomType + "', Bed = '" + room.bedType + "', Price = " + room.price + ", RoomNo = " + room.roomNumber + " WHERE RoomID = " + roomId;
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        public void Delete(int roomId)
        {
            SqlConnection connection = GetDbConnection();
            string sql = "DELETE [dbo].[Room] WHERE RoomID = " + roomId;
            SqlCommand cmd2 = new SqlCommand(sql, connection);
            connection.Open();
            cmd2.CommandType = CommandType.Text;
            cmd2.ExecuteNonQuery();
            connection.Close();
        }
    }
}
