﻿using HotelReservationSystem.PersonFolder;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelReservationSystem.DataLayer
{
    class EmployeeManager : DataManager
    {
        public void InsertReceptionist(Receptionist receptionist)
        {
            SqlConnection connection = GetDbConnection();
            string sql = "INSERT INTO [dbo].[Employee] VALUES ('" + receptionist.name + "','" + receptionist.surname + "','" + receptionist.dateOfBirth + "','" + receptionist.gender + "','" + receptionist.nationality + "','" + receptionist.email + "'," + receptionist.mobileNumber + ",'" + receptionist.username + "','" + receptionist.password + "','Receptionist')";
            SqlCommand cmdinsert = new SqlCommand(sql, connection);
            connection.Open();
            cmdinsert.CommandType = CommandType.Text;
            cmdinsert.ExecuteNonQuery();
            connection.Close();
        }

        public List<Receptionist> GetAllReceptionist()
        {
            List<Receptionist> dbReceptionist = new List<Receptionist>();
            SqlConnection connection = GetDbConnection();
            string sql = "SELECT * FROM [dbo].[Employee] WHERE EmployeeType = 'Receptionist'";
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                Receptionist r = new Receptionist(int.Parse(dr[0].ToString()), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[5].ToString(), dr[4].ToString(), dr[6].ToString(), dr[7].ToString(), dr[8].ToString(), dr[9].ToString());
                dbReceptionist.Add(r);
            }

            dr.Close();
            connection.Close();
            return dbReceptionist;
        }

        public List<int> GetAllReceptionistId()
        {
            List<int> dbReceptionistId = new List<int>();
            SqlConnection connection = GetDbConnection();
            string sql = "SELECT EmployeeId FROM[dbo].[Employee] WHERE EmployeeType = 'Receptionist'";
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                dbReceptionistId.Add(int.Parse(dr[0].ToString()));
            }

            dr.Close();
            connection.Close();

            return dbReceptionistId;
        }

        public Receptionist GetSingleReceptionist(int personId)
        {
            Receptionist r = null;
            string sql = "SELECT * FROM[dbo].[Employee] WHERE EmployeeId = " + personId;
            SqlConnection connection = GetDbConnection();
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                r = new Receptionist(int.Parse(dr[0].ToString()), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[5].ToString(), dr[4].ToString(), dr[6].ToString(), dr[7].ToString(), dr[8].ToString(), dr[9].ToString());
            }
            dr.Close();
            connection.Close();
            return r;
        }

        public void UpdateReceptionist(int personId, Receptionist receptionist)
        {
            SqlConnection connection = GetDbConnection();
            string sql = sql = "UPDATE[dbo].[Employee] SET Name = '" + receptionist.name + "', Surname = '" + receptionist.surname + "', DateOfBirth = '" + receptionist.dateOfBirth + "' , Gender = '" + receptionist.nationality + "', Nationality = '" + receptionist.gender + "', Email = '" + receptionist.email + "', MobileNo = '" + receptionist.mobileNumber + "' , Username = '" + receptionist.username + "', Password = '" + receptionist.password + "' WHERE EmployeeID = " + personId;
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        public void Delete(int personId)
        {
            SqlConnection connection = GetDbConnection();
            string sql = "DELETE [dbo].[Employee] WHERE EmployeeId = " + personId;
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        public void InsertManager(Manager manager)
        {
            SqlConnection connection = GetDbConnection();
            string sql = "INSERT INTO [dbo].[Employee] VALUES ('" + manager.name + "','" + manager.surname + "','" + manager.dateOfBirth + "','" + manager.gender + "','" + manager.nationality + "','" + manager.email + "'," + manager.mobileNumber + ",'" + manager.username + "','" + manager.password + "','Manager')";
            SqlCommand cmdinsert = new SqlCommand(sql, connection);
            connection.Open();
            cmdinsert.CommandType = CommandType.Text;
            cmdinsert.ExecuteNonQuery();
            connection.Close();
        }

        public List<Manager> GetAllManager()
        {
            List<Manager> dbManager = new List<Manager>();
            SqlConnection connection = GetDbConnection();
            string sql = "SELECT * FROM [dbo].[Employee] WHERE EmployeeType = 'Manager'";
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                Manager m = new Manager(int.Parse(dr[0].ToString()), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[5].ToString(), dr[4].ToString(), dr[6].ToString(), dr[7].ToString(), dr[8].ToString(), dr[9].ToString());
                dbManager.Add(m);
            }

            dr.Close();
            connection.Close();
            return dbManager;
        }

        public List<int> GetAllManagerId()
        {
            List<int> dbManangerId = new List<int>();
            SqlConnection connection = GetDbConnection();
            string sql = "SELECT EmployeeId FROM[dbo].[Employee] WHERE EmployeeType = 'Manager'";
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                dbManangerId.Add(int.Parse(dr[0].ToString()));
            }

            dr.Close();
            connection.Close();

            return dbManangerId;
        }

        public Manager GetSingleManager(int personId)
        {
            Manager m = null;
            string sql = "SELECT * FROM[dbo].[Employee] WHERE EmployeeId = " + personId;
            SqlConnection connection = GetDbConnection();
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                m = new Manager(int.Parse(dr[0].ToString()), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[5].ToString(), dr[4].ToString(), dr[6].ToString(), dr[7].ToString(), dr[8].ToString(), dr[9].ToString());
            }
            dr.Close();
            connection.Close();
            return m;
        }

        public void UpdateManager(int personId, Manager manager)
        {
            SqlConnection connection = GetDbConnection();
            string sql = sql = "UPDATE[dbo].[Employee] SET Name = '" + manager.name + "', Surname = '" + manager.surname + "', DateOfBirth = '" + manager.dateOfBirth + "' , Gender = '" + manager.nationality + "', Nationality = '" + manager.gender + "', Email = '" + manager.email + "', MobileNo = '" + manager.mobileNumber + "' , Username = '" + manager.username + "', Password = '" + manager.password + "' WHERE EmployeeID = " + personId;
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        public string checkUsernamePassword(string username, string password)
        {
            SqlConnection connection = GetDbConnection();
            string personType = "";
            SqlCommand cmd = new SqlCommand("SELECT Username, Password, EmployeeType FROM [dbo].[Employee]", connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                if (username.Equals(dr[0].ToString()) && password.Equals(dr[1].ToString()))
                {
                    personType = dr[2].ToString();
                }
            }
            dr.Close();
            connection.Close();
            return personType;
        }
    }
}
