﻿using HotelReservationSystem.Other;
using HotelReservationSystem.ReservationFolder.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelReservationSystem.DataLayer
{
    public class BookingRoomManager : DataManager
    {
        public List<BookingRoom> GetAll()
        {
            SqlConnection connection = GetDbConnection();
            List<BookingRoom> dbBookingRooms = new List<BookingRoom>();
            SqlCommand cmd = new SqlCommand("SELECT * FROM [dbo].[BookingRoom]", connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                BookingRoom br = new BookingRoom(int.Parse(dr[1].ToString()), int.Parse(dr[2].ToString()));
                dbBookingRooms.Add(br);
            }
            dr.Close();
            connection.Close();

            return dbBookingRooms;
        }

        public List<int> GetAllRoomId()
        {
            SqlConnection connection = GetDbConnection();
            List<int> dbBookingRooms = new List<int>();
            SqlCommand cmd = new SqlCommand("SELECT RoomId FROM [dbo].[BookingRoom]", connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            { 
                dbBookingRooms.Add(int.Parse(dr[0].ToString()));
            }
            dr.Close();
            connection.Close();

            return dbBookingRooms;
        }

        public List<ReservationView> GetView()
        {
            List<ReservationView> rvList = new List<ReservationView>();

            SqlConnection connection = GetDbConnection();
            string sql = "SELECT b.BookingNo, b.GuestID, b.NumberOfPersons, b.NumberOfNights, b.ArrivalDate, b.DepartureDate, STUFF((SELECT ',' + CAST(r.RoomNo as varchar(10)) FROM BookingRoom br INNER JOIN Room r ON br.RoomID = r.RoomID WHERE br.BookingNo = b.BookingNo FOR XML PATH('')), 1, 1, '') " +
                         "FROM Booking b " +
                         "WHERE Status = 'Reserved'";
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                ReservationView rv = new ReservationView(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString(), dr[5].ToString(), dr[6].ToString());
                rvList.Add(rv);
            }               

            dr.Close();
            connection.Close();

            return rvList;
        }

        public List<ReservationView> GetViewCheckedIn()
        {
            List<ReservationView> rvList = new List<ReservationView>();

            SqlConnection connection = GetDbConnection();
            string sql = "SELECT b.BookingNo, b.GuestID, b.NumberOfPersons, b.NumberOfNights, b.ArrivalDate, b.DepartureDate, STUFF((SELECT ',' + CAST(r.RoomNo as varchar(10)) FROM BookingRoom br INNER JOIN Room r ON br.RoomID = r.RoomID WHERE br.BookingNo = b.BookingNo FOR XML PATH('')), 1, 1, '') " +
                         "FROM Booking b " +
                         "WHERE Status = 'Checked In'";
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                ReservationView rv = new ReservationView(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString(), dr[5].ToString(), dr[6].ToString());
                rvList.Add(rv);
            }

            dr.Close();
            connection.Close();

            return rvList;
        }

        public List<ReservationView> GetViewCheckedOut()
        {
            List<ReservationView> rvList = new List<ReservationView>();

            SqlConnection connection = GetDbConnection();
            string sql = "SELECT b.BookingNo, b.GuestID, b.NumberOfPersons, b.NumberOfNights, b.ArrivalDate, b.DepartureDate, STUFF((SELECT ',' + CAST(r.RoomNo as varchar(10)) FROM BookingRoom br INNER JOIN Room r ON br.RoomID = r.RoomID WHERE br.BookingNo = b.BookingNo FOR XML PATH('')), 1, 1, '') " +
                         "FROM Booking b " +
                         "WHERE Status = 'Checked Out'";
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                ReservationView rv = new ReservationView(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString(), dr[5].ToString(), dr[6].ToString());
                rvList.Add(rv);
            }

            dr.Close();
            connection.Close();

            return rvList;
        }

        public void Delete(int bookingNo)
        {
            SqlConnection connection = GetDbConnection();
            string deleteFromBookingRoom = "DELETE FROM BookingRoom WHERE BookingNo = " + bookingNo + ";";
            string sql = "DELETE [dbo].[Booking] WHERE BookingNo = " + bookingNo + ";";
            string resultingSql = deleteFromBookingRoom + sql;
            SqlCommand cmd = new SqlCommand(resultingSql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
            connection.Close();
        }
    }
}
