﻿using HotelReservationSystem.PersonFolder;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelReservationSystem.DataLayer
{
    public class GuestManager : DataManager
    {
        public void Insert(Guest guest)
        {
            SqlConnection connection = GetDbConnection();
            string sql = "INSERT INTO [dbo].[Guest] VALUES ('" + guest.name + "','" + guest.surname + "','" + guest.dateOfBirth + "','" + guest.gender + "','" + guest.nationality + "','" + guest.email + "'," + guest.mobileNumber + ")";
            SqlCommand cmdinsert = new SqlCommand(sql, connection);
            connection.Open();
            cmdinsert.CommandType = CommandType.Text;
            cmdinsert.ExecuteNonQuery();
            connection.Close();
        }

        public List<Guest> GetAll()
        {
            List<Guest> dbGuest = new List<Guest>();
            SqlConnection connection = GetDbConnection();
            string sql = "SELECT * FROM [dbo].[Guest]";
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                Guest g = new Guest(int.Parse(dr[0].ToString()), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[5].ToString(), dr[4].ToString(), dr[6].ToString(), dr[7].ToString());
                dbGuest.Add(g);
            }

            dr.Close();
            connection.Close();
            return dbGuest;
        }

        public List<int> GetAllGuestId()
        {
            List<int> dbGuestId = new List<int>();
            SqlConnection connection = GetDbConnection();
            string sql = "SELECT GuestID FROM[dbo].[Guest]";
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                dbGuestId.Add(int.Parse(dr[0].ToString()));
            }

            dr.Close();
            connection.Close();

            return dbGuestId;
        }

        public Guest GetSingle(int personId)
        {
            Guest g = null;
            string sql = "SELECT * FROM[dbo].[Guest] WHERE GuestID = " + personId;
            SqlConnection connection = GetDbConnection();
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                g = new Guest(int.Parse(dr[0].ToString()), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[5].ToString(), dr[4].ToString(), dr[6].ToString(), dr[7].ToString());
            }
            dr.Close();
            connection.Close();
            return g;
        }

        public void Update(int personId, Guest guest)
        {
            SqlConnection connection = GetDbConnection();
            string sql = "UPDATE[dbo].[Guest] SET Name = '" + guest.name + "', Surname = '" + guest.surname + "', DateOfBirth = '" + guest.dateOfBirth + "' , Gender = '" + guest.nationality + "', Nationality = '" + guest.gender + "', Email = '" + guest.email + "', MobileNo = '" + guest.mobileNumber + "' WHERE GuestID = " + personId;
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        public void Delete(int personId)
        {
            SqlConnection connection = GetDbConnection();
            string sql = "DELETE [dbo].[Guest] WHERE GuestID = " + personId;
            SqlCommand cmd = new SqlCommand(sql, connection);
            connection.Open();
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
            connection.Close();
        }
    }
}
